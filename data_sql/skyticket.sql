-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2017 at 02:23 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+07:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skyticket`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `baocaodoanhthunam` (IN `nam` INTEGER)  BEGIN
DECLARE thang integer;
DECLARE countcb integer;
DECLARE doanhthu decimal;
SET @thang = 1;
TRUNCATE TABLE baocao;
while @thang <= 12
do
SELECT COUNT( * ), SUM(result.tonggiave) into @countcb, @doanhthu
FROM (
SELECT cb.MACHUYENBAY, SUM( ctdv.GIAVE ) AS tonggiave
FROM chuyenbay cb, dondatve ddv, chitietdatve ctdv, ve v
WHERE YEAR( cb.THOIGIANBAY ) = nam
AND MONTH( cb.THOIGIANBAY ) = @thang
AND ctdv.MAVE = v.MAVE
AND v.MACHUYENBAY = cb.MACHUYENBAY
AND ctdv.MADONDATVE = ddv.MADONDATVE
AND ddv.DATHANHTOAN =1
GROUP BY cb.MACHUYENBAY
) AS result;
insert into baocao values (@thang, @countcb, @doanhthu);
set @thang = @thang + 1;
end while;
select t.THANG as 'Tháng', t.SOCHUYENBAY as 'Số chuyến bay', t.DOANHTHU as 'Doanh thu' from baocao t;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `baocaodoanhthuthangnam` (IN `thang` INT, IN `nam` INT)  BEGIN
SELECT cb.MACHUYENBAY AS 'macb', COUNT(ctdv.MAVE) as 'sove', SUM(ctdv.GIAVE) as 'tong'
FROM chuyenbay cb, dondatve ddv, chitietdatve ctdv, ve v
WHERE MONTH( cb.THOIGIANBAY ) = thang
AND YEAR(cb.THOIGIANBAY) = nam
AND ctdv.MAVE = v.MAVE
AND v.MACHUYENBAY = cb.MACHUYENBAY
AND ctdv.MADONDATVE = ddv.MADONDATVE
AND ddv.DATHANHTOAN =1
GROUP BY cb.MACHUYENBAY;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `danhsachchuyenbay` ()  BEGIN
SELECT cb.MACHUYENBAY AS 'Mã chuyến bay', cb.MAMAYBAY AS 'Mã máy bay', sb1.TENSANBAY AS 'Sân Bay Đi', sb2.TENSANBAY AS 'Sân Bay Đến', cb.THOIGIANBAY AS 'Thời gian bay', cb.THOIGIANDEN AS 'Thời gian đến', sb3.TENSANBAY AS 'Tên sân bay trạm dừng', (

SELECT COUNT( v1.MAVE )
FROM ve AS v1
WHERE cb.MACHUYENBAY = v1.MACHUYENBAY
AND v1.TRANGTHAI =1
) AS 'Số vé đã bán', (

SELECT COUNT( v2.MAVE )
FROM ve AS v2
WHERE cb.MACHUYENBAY = v2.MACHUYENBAY
AND (
v2.TRANGTHAI =0
OR v2.TRANGTHAI =2
)
) AS 'Số vé chưa bán', (

SELECT COUNT( v3.MAVE )
FROM ve AS v3
WHERE v3.MACHUYENBAY = cb.MACHUYENBAY
) AS 'Tổng số vé'
FROM chuyenbay AS cb
INNER JOIN sanbay AS sb1 ON cb.SANBAYDI = sb1.MASANBAY
INNER JOIN sanbay AS sb2 ON cb.SANBAYDEN = sb2.MASANBAY
LEFT JOIN sanbay AS sb3 ON cb.TRANSITION = sb3.MASANBAY
ORDER BY cb.DABAY, cb.THOIGIANBAY DESC;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `layloaivecu` (IN `mahdcu` INTEGER, OUT `soluongdamua` INTEGER)  BEGIN
SELECT ve.LOAIVE FROM ve, chitietdatve, dondatve WHERE ve.MAVE = chitietdatve.MAVE AND dondatve.MADONDATVE = chitietdatve.MADONDATVE and dondatve.MADONDATVE = mahdcu;
SELECT COUNT(*) INTO soluongdamua FROM ve, chitietdatve, dondatve WHERE ve.MAVE = chitietdatve.MAVE AND dondatve.MADONDATVE = chitietdatve.MADONDATVE and dondatve.MADONDATVE = mahdcu;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `layvechuamua` (IN `macb` VARCHAR(5), IN `tongsove` INTEGER)  BEGIN
SELECT v.MAVE FROM ve v WHERE v.MACHUYENBAY = macb and v.TRANGTHAI = 0 LIMIT tongsove;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `searchChuyenBay` (IN `diemdi` VARCHAR(3), IN `diemden` VARCHAR(3), IN `thoigiandi` DATE)  BEGIN

select cb.MACHUYENBAY as 'ma_chuyen_bay', sb1.TENSANBAY as 'san_bay_di', sb2.TENSANBAY as 'san_bay_den', cb.THOIGIANBAY as 'thoi_gian_bay', 
cb.THOIGIANDEN as 'thoi_gian_den', cb.GIAVEGOC as 'gia_ve_goc', timediff(cb.THOIGIANDEN, cb.THOIGIANBAY) as 'duration',
sb3.TENSANBAY as 'transition'
from chuyenbay as cb
inner join sanbay as sb1 on cb.SANBAYDI = sb1.MASANBAY
inner join sanbay as sb2 on cb.SANBAYDEN = sb2.MASANBAY
left join sanbay as sb3 on cb.TRANSITION = sb3.MASANBAY
WHERE cb.SANBAYDI = diemdi and cb.SANBAYDEN = diemden and DATE(cb.THOIGIANBAY) = thoigiandi and cb.DABAY = 0;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `sualoaive` (IN `mave` INT, IN `loaive` INT)  BEGIN
UPDATE ve SET ve.LOAIVE = loaive where ve.MAVE = MAVE;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `xoaddvvactdv` (IN `madondatve` INT)  BEGIN
DELETE FROM dondatve WHERE dondatve.MADONDATVE = madondatve;
END$$

CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` PROCEDURE `xoakhachhang` (IN `makhachhang` INT)  BEGIN

DELETE FROM dondatve WHERE dondatve.MAKHACHHANG = makhachhang;

DELETE FROM khachhang WHERE khachhang.MAKHACHHANG = makhachhang;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `baocao`
--

CREATE TABLE `baocao` (
  `THANG` int(11) DEFAULT NULL,
  `SOCHUYENBAY` int(11) DEFAULT NULL,
  `DOANHTHU` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `baocao`
--

INSERT INTO `baocao` (`THANG`, `SOCHUYENBAY`, `DOANHTHU`) VALUES
(1, 0, NULL),
(2, 0, NULL),
(3, 0, NULL),
(4, 0, NULL),
(5, 0, NULL),
(6, 0, NULL),
(7, 0, NULL),
(8, 0, NULL),
(9, 0, NULL),
(10, 0, NULL),
(11, 0, NULL),
(12, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chitietdatve`
--

CREATE TABLE `chitietdatve` (
  `MADONDATVE` int(11) NOT NULL,
  `MAVE` int(11) NOT NULL,
  `HOTENHANHKHACH` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'họ tên người đi chung với ng` đặt vé',
  `GIAVE` decimal(10,0) DEFAULT NULL,
  `DELETEFLAG` bit(1) NOT NULL DEFAULT b'0',
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chitietdatve`
--

INSERT INTO `chitietdatve` (`MADONDATVE`, `MAVE`, `HOTENHANHKHACH`, `GIAVE`, `DELETEFLAG`, `DELETETIME`) VALUES
(5, 1, 'Quyền', '1200000', b'0', NULL),
(8, 946, 'A', '1450000', b'0', NULL),
(8, 947, 'B', '1450000', b'0', NULL),
(10, 948, 'Châu Minh Phúc', '1450000', b'0', NULL),
(10, 949, 'Nguyễn Quốc Khánh', '1450000', b'0', NULL),
(10, 950, 'Tất Vỹ Tâm', '1450000', b'0', NULL),
(11, 1459, 'Cô Tú', '2000000', b'0', NULL);

--
-- Triggers `chitietdatve`
--
DELIMITER $$
CREATE TRIGGER `VE_capnhat_TRANGTHAI_BAN` AFTER INSERT ON `chitietdatve` FOR EACH ROW BEGIN
UPDATE ve
SET ve.TRANGTHAI = 1
WHERE MAVE = NEW.MAVE;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `chuyenbay`
--

CREATE TABLE `chuyenbay` (
  `MACHUYENBAY` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `MAMAYBAY` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `SANBAYDI` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `SANBAYDEN` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `GIAVEGOC` int(11) NOT NULL,
  `THOIGIANBAY` datetime NOT NULL,
  `THOIGIANDEN` datetime NOT NULL,
  `TRANSITION` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'sân bay trạm dừng',
  `DABAY` bit(1) DEFAULT b'0' COMMENT '0: chưa bay, 1: bay rồi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chuyenbay`
--

INSERT INTO `chuyenbay` (`MACHUYENBAY`, `MAMAYBAY`, `SANBAYDI`, `SANBAYDEN`, `GIAVEGOC`, `THOIGIANBAY`, `THOIGIANDEN`, `TRANSITION`, `DABAY`) VALUES
('A0001', 'A3810 ', 'SGN', 'DLI', 1200000, '2017-06-01 20:00:00', '2017-06-01 21:00:00', NULL, b'0'),
('A0002', 'A3810 ', 'SGN', 'DLI', 1450000, '2017-06-01 19:00:00', '2017-06-01 22:00:00', 'BMV', b'0'),
('A0003', 'A3810 ', 'SGN', 'DLI', 1200000, '2017-05-20 19:00:00', '2017-05-20 20:00:00', NULL, b'1'),
('A0004', 'A3810 ', 'BMV', 'BMV', 0, '2017-05-20 00:00:00', '2017-05-20 23:59:00', NULL, b'1'),
('B0001', 'A3810 ', 'SGN', 'DLI', 1450000, '2017-05-24 18:00:00', '2017-05-24 19:00:00', NULL, b'1'),
('B0002', 'A3810 ', 'SGN', 'DLI', 2800000, '2017-05-24 22:00:00', '2017-05-24 23:00:00', 'BMV', b'1'),
('B0003', 'BO737 ', 'SGN', 'DLI', 1200000, '2017-05-25 05:00:00', '2017-05-25 07:00:00', NULL, b'1'),
('B0005', 'A3810 ', 'VCA', 'HAN', 1500000, '2017-05-24 15:55:00', '2017-05-24 17:00:00', NULL, b'1'),
('MN555', 'A3810 ', 'SGN', 'DLI', 2000000, '2017-05-24 18:00:00', '2017-05-24 19:00:00', NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `diadiem`
--

CREATE TABLE `diadiem` (
  `MADIADIEM` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `TENDIADIEM` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diadiem`
--

INSERT INTO `diadiem` (`MADIADIEM`, `TENDIADIEM`) VALUES
('BMT', 'Buôn Mê Thuột'),
('CTT', 'Cần Thơ'),
('DLA', 'Đà Lạt'),
('DNN', 'Đà Nẵng'),
('HCM', 'Hồ Chí Minh'),
('HNN', 'Hà Nội'),
('HPP', 'Hải Phòng'),
('HUE', 'Huế'),
('KGG', 'Kiên Giang'),
('NAA', 'Nghệ An'),
('NTT', 'Nha Trang'),
('THH', 'Thanh Hóa');

-- --------------------------------------------------------

--
-- Table structure for table `dondatve`
--

CREATE TABLE `dondatve` (
  `MADONDATVE` int(11) NOT NULL,
  `MAKHACHHANG` int(11) NOT NULL,
  `SOLUONGDAT` int(11) NOT NULL DEFAULT '1',
  `NGAYMUAVE` datetime NOT NULL,
  `THANHTIEN` decimal(10,0) NOT NULL,
  `DATHANHTOAN` bit(1) NOT NULL DEFAULT b'0' COMMENT '0: chưa, 1: ok',
  `MAKHUYENMAI` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELETEFLAG` bit(1) NOT NULL DEFAULT b'0',
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dondatve`
--

INSERT INTO `dondatve` (`MADONDATVE`, `MAKHACHHANG`, `SOLUONGDAT`, `NGAYMUAVE`, `THANHTIEN`, `DATHANHTOAN`, `MAKHUYENMAI`, `DELETEFLAG`, `DELETETIME`) VALUES
(5, 7, 1, '2017-05-18 05:38:58', '1200000', b'1', NULL, b'0', NULL),
(8, 7, 2, '2017-05-24 08:49:08', '2900000', b'1', NULL, b'0', NULL),
(10, 9, 3, '2017-05-24 10:28:52', '4350000', b'1', NULL, b'0', NULL),
(11, 9, 1, '2017-05-24 10:55:53', '2000000', b'1', NULL, b'0', NULL);

--
-- Triggers `dondatve`
--
DELIMITER $$
CREATE TRIGGER `DELETE_INSERT_BACKUP_DONDATVE_CHITIETDATVE` BEFORE UPDATE ON `dondatve` FOR EACH ROW BEGIN

INSERT INTO historydondatve(`MADONDATVE`, `MAKHACHHANG`, `SOLUONGDAT`, `NGAYMUAVE`, `THANHTIEN`, `DATHANHTOAN`, `MAKHUYENMAI`, `DELETEFLAG`, `DELETETIME`) SELECT ddv.MADONDATVE, ddv.MAKHACHHANG, ddv.SOLUONGDAT, ddv.NGAYMUAVE, ddv.THANHTIEN, ddv.DATHANHTOAN, ddv.MAKHUYENMAI, ddv.DELETEFLAG, NOW()
FROM dondatve ddv
WHERE NEW.DELETEFLAG = 1 AND ddv.MADONDATVE = NEW.MADONDATVE;

INSERT INTO historychitietdatve(`MADONDATVE`, `MAVE`, `HOTENHANHKHACH`, `GIAVE`, `DELETEFLAG`, `DELETETIME`) 
SELECT ctdv.MADONDATVE, ctdv.MAVE, ctdv.HOTENHANHKHACH, ctdv.GIAVE, 1, NOW()
FROM chitietdatve ctdv
WHERE ctdv.MADONDATVE = NEW.MADONDATVE;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `historychitietdatve`
--

CREATE TABLE `historychitietdatve` (
  `MADONDATVE` int(11) NOT NULL,
  `MAVE` int(11) NOT NULL,
  `HOTENHANHKHACH` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'họ tên người đi chung với ng` đặt vé',
  `GIAVE` decimal(10,0) DEFAULT NULL,
  `DELETEFLAG` bit(1) NOT NULL DEFAULT b'0',
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `historychitietdatve`
--

INSERT INTO `historychitietdatve` (`MADONDATVE`, `MAVE`, `HOTENHANHKHACH`, `GIAVE`, `DELETEFLAG`, `DELETETIME`) VALUES
(5, 1, 'Quyền', '1200000', b'1', '2017-05-19'),
(8, 946, 'A', '1450000', b'1', '2017-05-24'),
(8, 947, 'B', '1450000', b'1', '2017-05-24'),
(10, 948, 'Châu Minh Phúc', '1450000', b'1', '2017-05-24'),
(10, 949, 'Nguyễn Quốc Khánh', '1450000', b'1', '2017-05-24'),
(10, 950, 'Tất Vỹ Tâm', '1450000', b'1', '2017-05-24'),
(11, 1459, 'Cô Tú', '2000000', b'1', '2017-05-24');

-- --------------------------------------------------------

--
-- Table structure for table `historydondatve`
--

CREATE TABLE `historydondatve` (
  `MADONDATVE` int(11) NOT NULL,
  `MAKHACHHANG` int(11) NOT NULL,
  `SOLUONGDAT` int(11) NOT NULL DEFAULT '1',
  `NGAYMUAVE` datetime NOT NULL,
  `THANHTIEN` decimal(10,0) NOT NULL,
  `DATHANHTOAN` bit(1) NOT NULL DEFAULT b'0' COMMENT '0: chưa, 1: ok',
  `MAKHUYENMAI` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELETEFLAG` bit(1) NOT NULL DEFAULT b'0',
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `historykhachhang`
--

CREATE TABLE `historykhachhang` (
  `MAKHACHHANG` int(11) NOT NULL,
  `TENKHACHHANG` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DIACHI` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PHONE` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `DELETEFLAG` bit(1) NOT NULL DEFAULT b'0',
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `historyve`
--

CREATE TABLE `historyve` (
  `MAVE` int(11) NOT NULL,
  `MACHUYENBAY` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `LOAIVE` int(11) NOT NULL DEFAULT '0' COMMENT '0: mặc định, khi mua thì xét vé ng` dùng chọn (1: ng` lớn, 2: trẻ em, 3: em bé)',
  `TRANGTHAI` int(11) NOT NULL DEFAULT '0' COMMENT '0: chưa bán, 1: bán rồi, 2: qua ngày nhưng chưa bán dc',
  `DELETEFLAG` bit(1) NOT NULL,
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `MAKHACHHANG` int(11) NOT NULL,
  `TENKHACHHANG` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DIACHI` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PHONE` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `DELETEFLAG` bit(1) NOT NULL DEFAULT b'0',
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MAKHACHHANG`, `TENKHACHHANG`, `DIACHI`, `PHONE`, `EMAIL`, `PASSWORD`, `DELETEFLAG`, `DELETETIME`) VALUES
(5, 'Tất Vĩ Quyền', 'Hà Nội', '01222292364', 'quyenjino96@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', b'0', NULL),
(7, 'Quyen Jino', '12345', '01222292364', 'quyen@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', b'0', NULL),
(9, 'Châu Minh Phúc', 'HCM', '01207594594', 'phucgaoxam@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', b'0', NULL);

--
-- Triggers `khachhang`
--
DELIMITER $$
CREATE TRIGGER `DELETE_INSERT_BACKUP_KHACHHANG_DONDATVE_CTDV` BEFORE UPDATE ON `khachhang` FOR EACH ROW BEGIN

INSERT INTO historykhachhang(`MAKHACHHANG`, `TENKHACHHANG`, `DIACHI`, `PHONE`, `EMAIL`, `PASSWORD`, `DELETEFLAG`, `DELETETIME`)
SELECT kh.MAKHACHHANG, kh.TENKHACHHANG, kh.DIACHI, kh.PHONE, kh.EMAIL, kh.PASSWORD, 1, NOW() 
FROM khachhang kh
WHERE kh.MAKHACHHANG = NEW.MAKHACHHANG and NEW.DELETEFLAG = 1;

UPDATE dondatve SET dondatve.DELETEFLAG = 1 WHERE dondatve.MAKHACHHANG = NEW.MAKHACHHANG;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `khuyenmai`
--

CREATE TABLE `khuyenmai` (
  `MAKHUYENMAI` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `KHUYENMAI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khuyenmai`
--

INSERT INTO `khuyenmai` (`MAKHUYENMAI`, `KHUYENMAI`) VALUES
('OPEN1', 200000),
('QUYEN', 500000),
('TAIKM', 300000);

-- --------------------------------------------------------

--
-- Table structure for table `loaive`
--

CREATE TABLE `loaive` (
  `MALOAIVE` int(11) NOT NULL COMMENT '1: vé ng` lớn, 2: vé cho trẻ em, 3: vé cho em bé',
  `TENLOAIVE` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loaive`
--

INSERT INTO `loaive` (`MALOAIVE`, `TENLOAIVE`) VALUES
(1, 'Người Lớn'),
(2, 'Trẻ Em'),
(3, 'Em Bé');

-- --------------------------------------------------------

--
-- Table structure for table `maybay`
--

CREATE TABLE `maybay` (
  `MAMAYBAY` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `TENMAYBAY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SOLUONGGHE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `maybay`
--

INSERT INTO `maybay` (`MAMAYBAY`, `TENMAYBAY`, `SOLUONGGHE`) VALUES
('A3810', 'Airbus A.318', 117),
('BO737', 'Boeing 737', 162),
('BO767', 'Boeing 767', 198),
('BO777', 'Boeing 777', 222);

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `MANHANVIEN` int(11) NOT NULL,
  `TENNHANVIEN` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DIACHI` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PHONE` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `NGAYSINH` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MANHANVIEN`, `TENNHANVIEN`, `DIACHI`, `EMAIL`, `PHONE`, `NGAYSINH`) VALUES
(1001, 'Admin', 'PM1401', 'admin@skyticket.com', '1080', '2017-04-01'),
(1002, 'NhanSu1', 'Hà Nội', 'nhansu1@skyticket.vn', '0123456789', '1999-02-16'),
(1003, 'KeToan', 'Đà Nẵng', 'ketoan@skyticket.vn', '0123456789', '2000-01-05'),
(1005, 'Quản lý chuyến bay', 'Phú Quốc', 'qlcb@gmail.com', '0123456789', '2017-05-02');

-- --------------------------------------------------------

--
-- Table structure for table `quyen`
--

CREATE TABLE `quyen` (
  `MAQUYEN` varchar(2) NOT NULL,
  `TENQUYEN` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quyen`
--

INSERT INTO `quyen` (`MAQUYEN`, `TENQUYEN`) VALUES
('AD', 'Admin'),
('KT', 'Kế toán'),
('NS', 'Nhân sự'),
('QL', 'Quản lý chuyến bay');

-- --------------------------------------------------------

--
-- Table structure for table `sanbay`
--

CREATE TABLE `sanbay` (
  `MASANBAY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `TENSANBAY` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MADIADIEM` varchar(3) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sanbay`
--

INSERT INTO `sanbay` (`MASANBAY`, `TENSANBAY`, `MADIADIEM`) VALUES
('BMV', 'Buôn Mê Thuột', 'BMT'),
('CRX', 'Cam Ranh', 'NTT'),
('DAD', 'Đà Nẵng', 'DNN'),
('DLI', 'Liên Khương', 'DLA'),
('HAN', 'Nội Bài', 'HNN'),
('HPH', 'Cát Bi', 'HPP'),
('HUI', 'Phú Bài', 'HUE'),
('PQC', 'Phú Quốc', 'KGG'),
('SGN', 'Tân Sơn Nhất', 'HCM'),
('VCA', 'Cần Thơ', 'CTT'),
('VII', 'Vinh', 'NAA');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE `taikhoan` (
  `MAQUYEN` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `MANHANVIEN` int(11) NOT NULL,
  `TENTAIKHOAN` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MATKHAU` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`MAQUYEN`, `MANHANVIEN`, `TENTAIKHOAN`, `MATKHAU`) VALUES
('AD', 1001, 'admin', '12345'),
('KT', 1003, 'ketoan', '12345'),
('NS', 1002, 'nhansu', '12345'),
('QL', 1005, 'qlcb', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `thamso`
--

CREATE TABLE `thamso` (
  `MATHAMSO` int(11) NOT NULL,
  `THAMSO` int(11) NOT NULL,
  `GHICHU` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ve`
--

CREATE TABLE `ve` (
  `MAVE` bigint(20) NOT NULL,
  `MACHUYENBAY` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `LOAIVE` int(11) NOT NULL DEFAULT '0' COMMENT '0: mặc định, khi mua thì xét vé ng` dùng chọn (1: ng` lớn, 2: trẻ em, 3: em bé)',
  `TRANGTHAI` int(11) NOT NULL DEFAULT '0' COMMENT '0: chưa bán, 1: bán rồi, 2: qua ngày nhưng chưa bán dc',
  `DELETEFLAG` bit(1) NOT NULL,
  `DELETETIME` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ve`
--

INSERT INTO `ve` (`MAVE`, `MACHUYENBAY`, `LOAIVE`, `TRANGTHAI`, `DELETEFLAG`, `DELETETIME`) VALUES
(1, 'A0001', 1, 1, b'0', NULL),
(2, 'A0001', 0, 0, b'0', NULL),
(3, 'A0001', 0, 0, b'0', NULL),
(4, 'A0001', 0, 0, b'0', NULL),
(5, 'A0001', 0, 0, b'0', NULL),
(6, 'A0001', 0, 0, b'0', NULL),
(7, 'A0001', 0, 0, b'0', NULL),
(8, 'A0001', 0, 0, b'0', NULL),
(9, 'A0001', 0, 0, b'0', NULL),
(10, 'A0001', 0, 0, b'0', NULL),
(11, 'A0001', 0, 0, b'0', NULL),
(12, 'A0001', 0, 0, b'0', NULL),
(13, 'A0001', 0, 0, b'0', NULL),
(14, 'A0001', 0, 0, b'0', NULL),
(15, 'A0001', 0, 0, b'0', NULL),
(16, 'A0001', 0, 0, b'0', NULL),
(17, 'A0001', 0, 0, b'0', NULL),
(18, 'A0001', 0, 0, b'0', NULL),
(19, 'A0001', 0, 0, b'0', NULL),
(20, 'A0001', 0, 0, b'0', NULL),
(21, 'A0001', 0, 0, b'0', NULL),
(22, 'A0001', 0, 0, b'0', NULL),
(23, 'A0001', 0, 0, b'0', NULL),
(24, 'A0001', 0, 0, b'0', NULL),
(25, 'A0001', 0, 0, b'0', NULL),
(26, 'A0001', 0, 0, b'0', NULL),
(27, 'A0001', 0, 0, b'0', NULL),
(28, 'A0001', 0, 0, b'0', NULL),
(29, 'A0001', 0, 0, b'0', NULL),
(30, 'A0001', 0, 0, b'0', NULL),
(31, 'A0001', 0, 0, b'0', NULL),
(32, 'A0001', 0, 0, b'0', NULL),
(33, 'A0001', 0, 0, b'0', NULL),
(34, 'A0001', 0, 0, b'0', NULL),
(35, 'A0001', 0, 0, b'0', NULL),
(36, 'A0001', 0, 0, b'0', NULL),
(37, 'A0001', 0, 0, b'0', NULL),
(38, 'A0001', 0, 0, b'0', NULL),
(39, 'A0001', 0, 0, b'0', NULL),
(40, 'A0001', 0, 0, b'0', NULL),
(41, 'A0001', 0, 0, b'0', NULL),
(42, 'A0001', 0, 0, b'0', NULL),
(43, 'A0001', 0, 0, b'0', NULL),
(44, 'A0001', 0, 0, b'0', NULL),
(45, 'A0001', 0, 0, b'0', NULL),
(46, 'A0001', 0, 0, b'0', NULL),
(47, 'A0001', 0, 0, b'0', NULL),
(48, 'A0001', 0, 0, b'0', NULL),
(49, 'A0001', 0, 0, b'0', NULL),
(50, 'A0001', 0, 0, b'0', NULL),
(51, 'A0001', 0, 0, b'0', NULL),
(52, 'A0001', 0, 0, b'0', NULL),
(53, 'A0001', 0, 0, b'0', NULL),
(54, 'A0001', 0, 0, b'0', NULL),
(55, 'A0001', 0, 0, b'0', NULL),
(56, 'A0001', 0, 0, b'0', NULL),
(57, 'A0001', 0, 0, b'0', NULL),
(58, 'A0001', 0, 0, b'0', NULL),
(59, 'A0001', 0, 0, b'0', NULL),
(60, 'A0001', 0, 0, b'0', NULL),
(61, 'A0001', 0, 0, b'0', NULL),
(62, 'A0001', 0, 0, b'0', NULL),
(63, 'A0001', 0, 0, b'0', NULL),
(64, 'A0001', 0, 0, b'0', NULL),
(65, 'A0001', 0, 0, b'0', NULL),
(66, 'A0001', 0, 0, b'0', NULL),
(67, 'A0001', 0, 0, b'0', NULL),
(68, 'A0001', 0, 0, b'0', NULL),
(69, 'A0001', 0, 0, b'0', NULL),
(70, 'A0001', 0, 0, b'0', NULL),
(71, 'A0001', 0, 0, b'0', NULL),
(72, 'A0001', 0, 0, b'0', NULL),
(73, 'A0001', 0, 0, b'0', NULL),
(74, 'A0001', 0, 0, b'0', NULL),
(75, 'A0001', 0, 0, b'0', NULL),
(76, 'A0001', 0, 0, b'0', NULL),
(77, 'A0001', 0, 0, b'0', NULL),
(78, 'A0001', 0, 0, b'0', NULL),
(79, 'A0001', 0, 0, b'0', NULL),
(80, 'A0001', 0, 0, b'0', NULL),
(81, 'A0001', 0, 0, b'0', NULL),
(82, 'A0001', 0, 0, b'0', NULL),
(83, 'A0001', 0, 0, b'0', NULL),
(84, 'A0001', 0, 0, b'0', NULL),
(85, 'A0001', 0, 0, b'0', NULL),
(86, 'A0001', 0, 0, b'0', NULL),
(87, 'A0001', 0, 0, b'0', NULL),
(88, 'A0001', 0, 0, b'0', NULL),
(89, 'A0001', 0, 0, b'0', NULL),
(90, 'A0001', 0, 0, b'0', NULL),
(91, 'A0001', 0, 0, b'0', NULL),
(92, 'A0001', 0, 0, b'0', NULL),
(93, 'A0001', 0, 0, b'0', NULL),
(94, 'A0001', 0, 0, b'0', NULL),
(95, 'A0001', 0, 0, b'0', NULL),
(96, 'A0001', 0, 0, b'0', NULL),
(97, 'A0001', 0, 0, b'0', NULL),
(98, 'A0001', 0, 0, b'0', NULL),
(99, 'A0001', 0, 0, b'0', NULL),
(100, 'A0001', 0, 0, b'0', NULL),
(101, 'A0001', 0, 0, b'0', NULL),
(102, 'A0001', 0, 0, b'0', NULL),
(103, 'A0001', 0, 0, b'0', NULL),
(104, 'A0001', 0, 0, b'0', NULL),
(105, 'A0001', 0, 0, b'0', NULL),
(106, 'A0001', 0, 0, b'0', NULL),
(107, 'A0001', 0, 0, b'0', NULL),
(108, 'A0001', 0, 0, b'0', NULL),
(109, 'A0001', 0, 0, b'0', NULL),
(110, 'A0001', 0, 0, b'0', NULL),
(111, 'A0001', 0, 0, b'0', NULL),
(112, 'A0001', 0, 0, b'0', NULL),
(113, 'A0001', 0, 0, b'0', NULL),
(114, 'A0001', 0, 0, b'0', NULL),
(115, 'A0001', 0, 0, b'0', NULL),
(116, 'A0001', 0, 0, b'0', NULL),
(117, 'A0001', 0, 0, b'0', NULL),
(397, 'A0002', 1, 1, b'0', NULL),
(398, 'A0002', 2, 1, b'0', NULL),
(399, 'A0002', 0, 0, b'0', NULL),
(400, 'A0002', 0, 0, b'0', NULL),
(401, 'A0002', 0, 0, b'0', NULL),
(402, 'A0002', 0, 0, b'0', NULL),
(403, 'A0002', 0, 0, b'0', NULL),
(404, 'A0002', 0, 0, b'0', NULL),
(405, 'A0002', 0, 0, b'0', NULL),
(406, 'A0002', 0, 0, b'0', NULL),
(407, 'A0002', 0, 0, b'0', NULL),
(408, 'A0002', 0, 0, b'0', NULL),
(409, 'A0002', 0, 0, b'0', NULL),
(410, 'A0002', 0, 0, b'0', NULL),
(411, 'A0002', 0, 0, b'0', NULL),
(412, 'A0002', 0, 0, b'0', NULL),
(413, 'A0002', 0, 0, b'0', NULL),
(414, 'A0002', 0, 0, b'0', NULL),
(415, 'A0002', 0, 0, b'0', NULL),
(416, 'A0002', 0, 0, b'0', NULL),
(417, 'A0002', 0, 0, b'0', NULL),
(418, 'A0002', 0, 0, b'0', NULL),
(419, 'A0002', 0, 0, b'0', NULL),
(420, 'A0002', 0, 0, b'0', NULL),
(421, 'A0002', 0, 0, b'0', NULL),
(422, 'A0002', 0, 0, b'0', NULL),
(423, 'A0002', 0, 0, b'0', NULL),
(424, 'A0002', 0, 0, b'0', NULL),
(425, 'A0002', 0, 0, b'0', NULL),
(426, 'A0002', 0, 0, b'0', NULL),
(427, 'A0002', 0, 0, b'0', NULL),
(428, 'A0002', 0, 0, b'0', NULL),
(429, 'A0002', 0, 0, b'0', NULL),
(430, 'A0002', 0, 0, b'0', NULL),
(431, 'A0002', 0, 0, b'0', NULL),
(432, 'A0002', 0, 0, b'0', NULL),
(433, 'A0002', 0, 0, b'0', NULL),
(434, 'A0002', 0, 0, b'0', NULL),
(435, 'A0002', 0, 0, b'0', NULL),
(436, 'A0002', 0, 0, b'0', NULL),
(437, 'A0002', 0, 0, b'0', NULL),
(438, 'A0002', 0, 0, b'0', NULL),
(439, 'A0002', 0, 0, b'0', NULL),
(440, 'A0002', 0, 0, b'0', NULL),
(441, 'A0002', 0, 0, b'0', NULL),
(442, 'A0002', 0, 0, b'0', NULL),
(443, 'A0002', 0, 0, b'0', NULL),
(444, 'A0002', 0, 0, b'0', NULL),
(445, 'A0002', 0, 0, b'0', NULL),
(446, 'A0002', 0, 0, b'0', NULL),
(447, 'A0002', 0, 0, b'0', NULL),
(448, 'A0002', 0, 0, b'0', NULL),
(449, 'A0002', 0, 0, b'0', NULL),
(450, 'A0002', 0, 0, b'0', NULL),
(451, 'A0002', 0, 0, b'0', NULL),
(452, 'A0002', 0, 0, b'0', NULL),
(453, 'A0002', 0, 0, b'0', NULL),
(454, 'A0002', 0, 0, b'0', NULL),
(455, 'A0002', 0, 0, b'0', NULL),
(456, 'A0002', 0, 0, b'0', NULL),
(457, 'A0002', 0, 0, b'0', NULL),
(458, 'A0002', 0, 0, b'0', NULL),
(459, 'A0002', 0, 0, b'0', NULL),
(460, 'A0002', 0, 0, b'0', NULL),
(461, 'A0002', 0, 0, b'0', NULL),
(462, 'A0002', 0, 0, b'0', NULL),
(463, 'A0002', 0, 0, b'0', NULL),
(464, 'A0002', 0, 0, b'0', NULL),
(465, 'A0002', 0, 0, b'0', NULL),
(466, 'A0002', 0, 0, b'0', NULL),
(467, 'A0002', 0, 0, b'0', NULL),
(468, 'A0002', 0, 0, b'0', NULL),
(469, 'A0002', 0, 0, b'0', NULL),
(470, 'A0002', 0, 0, b'0', NULL),
(471, 'A0002', 0, 0, b'0', NULL),
(472, 'A0002', 0, 0, b'0', NULL),
(473, 'A0002', 0, 0, b'0', NULL),
(474, 'A0002', 0, 0, b'0', NULL),
(475, 'A0002', 0, 0, b'0', NULL),
(476, 'A0002', 0, 0, b'0', NULL),
(477, 'A0002', 0, 0, b'0', NULL),
(478, 'A0002', 0, 0, b'0', NULL),
(479, 'A0002', 0, 0, b'0', NULL),
(480, 'A0002', 0, 0, b'0', NULL),
(481, 'A0002', 0, 0, b'0', NULL),
(482, 'A0002', 0, 0, b'0', NULL),
(483, 'A0002', 0, 0, b'0', NULL),
(484, 'A0002', 0, 0, b'0', NULL),
(485, 'A0002', 0, 0, b'0', NULL),
(486, 'A0002', 0, 0, b'0', NULL),
(487, 'A0002', 0, 0, b'0', NULL),
(488, 'A0002', 0, 0, b'0', NULL),
(489, 'A0002', 0, 0, b'0', NULL),
(490, 'A0002', 0, 0, b'0', NULL),
(491, 'A0002', 0, 0, b'0', NULL),
(492, 'A0002', 0, 0, b'0', NULL),
(493, 'A0002', 0, 0, b'0', NULL),
(494, 'A0002', 0, 0, b'0', NULL),
(495, 'A0002', 0, 0, b'0', NULL),
(496, 'A0002', 0, 0, b'0', NULL),
(497, 'A0002', 0, 0, b'0', NULL),
(498, 'A0002', 0, 0, b'0', NULL),
(499, 'A0002', 0, 0, b'0', NULL),
(500, 'A0002', 0, 0, b'0', NULL),
(501, 'A0002', 0, 0, b'0', NULL),
(502, 'A0002', 0, 0, b'0', NULL),
(503, 'A0002', 0, 0, b'0', NULL),
(504, 'A0002', 0, 0, b'0', NULL),
(505, 'A0002', 0, 0, b'0', NULL),
(506, 'A0002', 0, 0, b'0', NULL),
(507, 'A0002', 0, 0, b'0', NULL),
(508, 'A0002', 0, 0, b'0', NULL),
(509, 'A0002', 0, 0, b'0', NULL),
(510, 'A0002', 0, 0, b'0', NULL),
(511, 'A0002', 0, 0, b'0', NULL),
(512, 'A0002', 0, 0, b'0', NULL),
(513, 'A0002', 0, 0, b'0', NULL),
(514, 'A0003', 1, 1, b'0', NULL),
(515, 'A0003', 1, 1, b'0', NULL),
(516, 'A0003', 0, 2, b'0', NULL),
(517, 'A0003', 0, 2, b'0', NULL),
(518, 'A0003', 0, 2, b'0', NULL),
(519, 'A0003', 0, 2, b'0', NULL),
(520, 'A0003', 0, 2, b'0', NULL),
(521, 'A0003', 0, 2, b'0', NULL),
(522, 'A0003', 0, 2, b'0', NULL),
(523, 'A0003', 0, 2, b'0', NULL),
(524, 'A0003', 0, 2, b'0', NULL),
(525, 'A0003', 0, 2, b'0', NULL),
(526, 'A0003', 0, 2, b'0', NULL),
(527, 'A0003', 0, 2, b'0', NULL),
(528, 'A0003', 0, 2, b'0', NULL),
(529, 'A0003', 0, 2, b'0', NULL),
(530, 'A0003', 0, 2, b'0', NULL),
(531, 'A0003', 0, 2, b'0', NULL),
(532, 'A0003', 0, 2, b'0', NULL),
(533, 'A0003', 0, 2, b'0', NULL),
(534, 'A0003', 0, 2, b'0', NULL),
(535, 'A0003', 0, 2, b'0', NULL),
(536, 'A0003', 0, 2, b'0', NULL),
(537, 'A0003', 0, 2, b'0', NULL),
(538, 'A0003', 0, 2, b'0', NULL),
(539, 'A0003', 0, 2, b'0', NULL),
(540, 'A0003', 0, 2, b'0', NULL),
(541, 'A0003', 0, 2, b'0', NULL),
(542, 'A0003', 0, 2, b'0', NULL),
(543, 'A0003', 0, 2, b'0', NULL),
(544, 'A0003', 0, 2, b'0', NULL),
(545, 'A0003', 0, 2, b'0', NULL),
(546, 'A0003', 0, 2, b'0', NULL),
(547, 'A0003', 0, 2, b'0', NULL),
(548, 'A0003', 0, 2, b'0', NULL),
(549, 'A0003', 0, 2, b'0', NULL),
(550, 'A0003', 0, 2, b'0', NULL),
(551, 'A0003', 0, 2, b'0', NULL),
(552, 'A0003', 0, 2, b'0', NULL),
(553, 'A0003', 0, 2, b'0', NULL),
(554, 'A0003', 0, 2, b'0', NULL),
(555, 'A0003', 0, 2, b'0', NULL),
(556, 'A0003', 0, 2, b'0', NULL),
(557, 'A0003', 0, 2, b'0', NULL),
(558, 'A0003', 0, 2, b'0', NULL),
(559, 'A0003', 0, 2, b'0', NULL),
(560, 'A0003', 0, 2, b'0', NULL),
(561, 'A0003', 0, 2, b'0', NULL),
(562, 'A0003', 0, 2, b'0', NULL),
(563, 'A0003', 0, 2, b'0', NULL),
(564, 'A0003', 0, 2, b'0', NULL),
(565, 'A0003', 0, 2, b'0', NULL),
(566, 'A0003', 0, 2, b'0', NULL),
(567, 'A0003', 0, 2, b'0', NULL),
(568, 'A0003', 0, 2, b'0', NULL),
(569, 'A0003', 0, 2, b'0', NULL),
(570, 'A0003', 0, 2, b'0', NULL),
(571, 'A0003', 0, 2, b'0', NULL),
(572, 'A0003', 0, 2, b'0', NULL),
(573, 'A0003', 0, 2, b'0', NULL),
(574, 'A0003', 0, 2, b'0', NULL),
(575, 'A0003', 0, 2, b'0', NULL),
(576, 'A0003', 0, 2, b'0', NULL),
(577, 'A0003', 0, 2, b'0', NULL),
(578, 'A0003', 0, 2, b'0', NULL),
(579, 'A0003', 0, 2, b'0', NULL),
(580, 'A0003', 0, 2, b'0', NULL),
(581, 'A0003', 0, 2, b'0', NULL),
(582, 'A0003', 0, 2, b'0', NULL),
(583, 'A0003', 0, 2, b'0', NULL),
(584, 'A0003', 0, 2, b'0', NULL),
(585, 'A0003', 0, 2, b'0', NULL),
(586, 'A0003', 0, 2, b'0', NULL),
(587, 'A0003', 0, 2, b'0', NULL),
(588, 'A0003', 0, 2, b'0', NULL),
(589, 'A0003', 0, 2, b'0', NULL),
(590, 'A0003', 0, 2, b'0', NULL),
(591, 'A0003', 0, 2, b'0', NULL),
(592, 'A0003', 0, 2, b'0', NULL),
(593, 'A0003', 0, 2, b'0', NULL),
(594, 'A0003', 0, 2, b'0', NULL),
(595, 'A0003', 0, 2, b'0', NULL),
(596, 'A0003', 0, 2, b'0', NULL),
(597, 'A0003', 0, 2, b'0', NULL),
(598, 'A0003', 0, 2, b'0', NULL),
(599, 'A0003', 0, 2, b'0', NULL),
(600, 'A0003', 0, 2, b'0', NULL),
(601, 'A0003', 0, 2, b'0', NULL),
(602, 'A0003', 0, 2, b'0', NULL),
(603, 'A0003', 0, 2, b'0', NULL),
(604, 'A0003', 0, 2, b'0', NULL),
(605, 'A0003', 0, 2, b'0', NULL),
(606, 'A0003', 0, 2, b'0', NULL),
(607, 'A0003', 0, 2, b'0', NULL),
(608, 'A0003', 0, 2, b'0', NULL),
(609, 'A0003', 0, 2, b'0', NULL),
(610, 'A0003', 0, 2, b'0', NULL),
(611, 'A0003', 0, 2, b'0', NULL),
(612, 'A0003', 0, 2, b'0', NULL),
(613, 'A0003', 0, 2, b'0', NULL),
(614, 'A0003', 0, 2, b'0', NULL),
(615, 'A0003', 0, 2, b'0', NULL),
(616, 'A0003', 0, 2, b'0', NULL),
(617, 'A0003', 0, 2, b'0', NULL),
(618, 'A0003', 0, 2, b'0', NULL),
(619, 'A0003', 0, 2, b'0', NULL),
(620, 'A0003', 0, 2, b'0', NULL),
(621, 'A0003', 0, 2, b'0', NULL),
(622, 'A0003', 0, 2, b'0', NULL),
(623, 'A0003', 0, 2, b'0', NULL),
(624, 'A0003', 0, 2, b'0', NULL),
(625, 'A0003', 0, 2, b'0', NULL),
(626, 'A0003', 0, 2, b'0', NULL),
(627, 'A0003', 0, 2, b'0', NULL),
(628, 'A0003', 0, 2, b'0', NULL),
(629, 'A0003', 0, 2, b'0', NULL),
(630, 'A0003', 0, 2, b'0', NULL),
(631, 'A0003', 0, 2, b'0', NULL),
(632, 'A0003', 0, 2, b'0', NULL),
(633, 'A0003', 0, 2, b'0', NULL),
(634, 'A0003', 0, 2, b'0', NULL),
(635, 'A0003', 0, 2, b'0', NULL),
(636, 'A0003', 0, 2, b'0', NULL),
(637, 'A0003', 0, 2, b'0', NULL),
(638, 'A0003', 0, 2, b'0', NULL),
(639, 'A0003', 0, 2, b'0', NULL),
(640, 'A0003', 0, 2, b'0', NULL),
(641, 'A0003', 0, 2, b'0', NULL),
(642, 'A0003', 0, 2, b'0', NULL),
(643, 'A0003', 0, 2, b'0', NULL),
(644, 'A0003', 0, 2, b'0', NULL),
(645, 'A0003', 0, 2, b'0', NULL),
(646, 'A0003', 0, 2, b'0', NULL),
(647, 'A0003', 0, 2, b'0', NULL),
(648, 'A0003', 0, 2, b'0', NULL),
(649, 'A0003', 0, 2, b'0', NULL),
(650, 'A0003', 0, 2, b'0', NULL),
(651, 'A0003', 0, 2, b'0', NULL),
(652, 'A0003', 0, 2, b'0', NULL),
(653, 'A0003', 0, 2, b'0', NULL),
(654, 'A0003', 0, 2, b'0', NULL),
(655, 'A0003', 0, 2, b'0', NULL),
(656, 'A0003', 0, 2, b'0', NULL),
(657, 'A0003', 0, 2, b'0', NULL),
(658, 'A0003', 0, 2, b'0', NULL),
(659, 'A0003', 0, 2, b'0', NULL),
(660, 'A0003', 0, 2, b'0', NULL),
(661, 'A0003', 0, 2, b'0', NULL),
(662, 'A0003', 0, 2, b'0', NULL),
(663, 'A0003', 0, 2, b'0', NULL),
(664, 'A0003', 0, 2, b'0', NULL),
(665, 'A0003', 0, 2, b'0', NULL),
(666, 'A0003', 0, 2, b'0', NULL),
(667, 'A0003', 0, 2, b'0', NULL),
(668, 'A0003', 0, 2, b'0', NULL),
(669, 'A0003', 0, 2, b'0', NULL),
(670, 'A0003', 0, 2, b'0', NULL),
(671, 'A0003', 0, 2, b'0', NULL),
(672, 'A0003', 0, 2, b'0', NULL),
(673, 'A0003', 0, 2, b'0', NULL),
(674, 'A0003', 0, 2, b'0', NULL),
(675, 'A0003', 0, 2, b'0', NULL),
(676, 'A0003', 0, 2, b'0', NULL),
(677, 'A0003', 0, 2, b'0', NULL),
(678, 'A0003', 0, 2, b'0', NULL),
(679, 'A0003', 0, 2, b'0', NULL),
(680, 'A0003', 0, 2, b'0', NULL),
(681, 'A0003', 0, 2, b'0', NULL),
(682, 'A0003', 0, 2, b'0', NULL),
(683, 'A0003', 0, 2, b'0', NULL),
(684, 'A0003', 0, 2, b'0', NULL),
(685, 'A0003', 0, 2, b'0', NULL),
(686, 'A0003', 0, 2, b'0', NULL),
(687, 'A0003', 0, 2, b'0', NULL),
(688, 'A0003', 0, 2, b'0', NULL),
(689, 'A0003', 0, 2, b'0', NULL),
(690, 'A0003', 0, 2, b'0', NULL),
(691, 'A0003', 0, 2, b'0', NULL),
(692, 'A0003', 0, 2, b'0', NULL),
(693, 'A0003', 0, 2, b'0', NULL),
(694, 'A0003', 0, 2, b'0', NULL),
(695, 'A0003', 0, 2, b'0', NULL),
(696, 'A0003', 0, 2, b'0', NULL),
(697, 'A0003', 0, 2, b'0', NULL),
(698, 'A0003', 0, 2, b'0', NULL),
(699, 'A0003', 0, 2, b'0', NULL),
(700, 'A0003', 0, 2, b'0', NULL),
(701, 'A0003', 0, 2, b'0', NULL),
(702, 'A0003', 0, 2, b'0', NULL),
(703, 'A0003', 0, 2, b'0', NULL),
(704, 'A0003', 0, 2, b'0', NULL),
(705, 'A0003', 0, 2, b'0', NULL),
(706, 'A0003', 0, 2, b'0', NULL),
(707, 'A0003', 0, 2, b'0', NULL),
(708, 'A0003', 0, 2, b'0', NULL),
(709, 'A0003', 0, 2, b'0', NULL),
(710, 'A0003', 0, 2, b'0', NULL),
(711, 'A0003', 0, 2, b'0', NULL),
(712, 'A0003', 0, 2, b'0', NULL),
(713, 'A0003', 0, 2, b'0', NULL),
(714, 'A0003', 0, 2, b'0', NULL),
(715, 'A0003', 0, 2, b'0', NULL),
(716, 'A0003', 0, 2, b'0', NULL),
(717, 'A0003', 0, 2, b'0', NULL),
(718, 'A0003', 0, 2, b'0', NULL),
(719, 'A0003', 0, 2, b'0', NULL),
(720, 'A0003', 0, 2, b'0', NULL),
(721, 'A0003', 0, 2, b'0', NULL),
(722, 'A0003', 0, 2, b'0', NULL),
(723, 'A0003', 0, 2, b'0', NULL),
(724, 'A0003', 0, 2, b'0', NULL),
(725, 'A0003', 0, 2, b'0', NULL),
(726, 'A0003', 0, 2, b'0', NULL),
(727, 'A0003', 0, 2, b'0', NULL),
(728, 'A0003', 0, 2, b'0', NULL),
(729, 'A0003', 0, 2, b'0', NULL),
(730, 'A0003', 0, 2, b'0', NULL),
(731, 'A0003', 0, 2, b'0', NULL),
(732, 'A0003', 0, 2, b'0', NULL),
(733, 'A0003', 0, 2, b'0', NULL),
(734, 'A0003', 0, 2, b'0', NULL),
(735, 'A0003', 0, 2, b'0', NULL),
(736, 'A0003', 0, 2, b'0', NULL),
(737, 'A0003', 0, 2, b'0', NULL),
(738, 'A0003', 0, 2, b'0', NULL),
(739, 'A0003', 0, 2, b'0', NULL),
(740, 'A0003', 0, 2, b'0', NULL),
(741, 'A0003', 0, 2, b'0', NULL),
(742, 'A0003', 0, 2, b'0', NULL),
(743, 'A0003', 0, 2, b'0', NULL),
(744, 'A0003', 0, 2, b'0', NULL),
(745, 'A0003', 0, 2, b'0', NULL),
(746, 'A0003', 0, 2, b'0', NULL),
(747, 'A0003', 0, 2, b'0', NULL),
(748, 'A0003', 0, 2, b'0', NULL),
(749, 'A0003', 0, 2, b'0', NULL),
(750, 'A0003', 0, 2, b'0', NULL),
(751, 'A0003', 0, 2, b'0', NULL),
(752, 'A0003', 0, 2, b'0', NULL),
(753, 'A0003', 0, 2, b'0', NULL),
(754, 'A0003', 0, 2, b'0', NULL),
(755, 'A0003', 0, 2, b'0', NULL),
(756, 'A0003', 0, 2, b'0', NULL),
(757, 'A0003', 0, 2, b'0', NULL),
(758, 'A0003', 0, 2, b'0', NULL),
(759, 'A0003', 0, 2, b'0', NULL),
(760, 'A0003', 0, 2, b'0', NULL),
(761, 'A0003', 0, 2, b'0', NULL),
(762, 'A0003', 0, 2, b'0', NULL),
(763, 'A0003', 0, 2, b'0', NULL),
(764, 'A0003', 0, 2, b'0', NULL),
(765, 'A0003', 0, 2, b'0', NULL),
(766, 'A0003', 0, 2, b'0', NULL),
(767, 'A0003', 0, 2, b'0', NULL),
(768, 'A0003', 0, 2, b'0', NULL),
(769, 'A0003', 0, 2, b'0', NULL),
(770, 'A0003', 0, 2, b'0', NULL),
(771, 'A0003', 0, 2, b'0', NULL),
(772, 'A0003', 0, 2, b'0', NULL),
(773, 'A0003', 0, 2, b'0', NULL),
(774, 'A0003', 0, 2, b'0', NULL),
(775, 'A0003', 0, 2, b'0', NULL),
(776, 'A0003', 0, 2, b'0', NULL),
(777, 'A0003', 0, 2, b'0', NULL),
(778, 'A0003', 0, 2, b'0', NULL),
(779, 'A0003', 0, 2, b'0', NULL),
(780, 'A0003', 0, 2, b'0', NULL),
(781, 'A0003', 0, 2, b'0', NULL),
(782, 'A0003', 0, 2, b'0', NULL),
(783, 'A0003', 0, 2, b'0', NULL),
(784, 'A0003', 0, 2, b'0', NULL),
(785, 'A0003', 0, 2, b'0', NULL),
(786, 'A0003', 0, 2, b'0', NULL),
(787, 'A0003', 0, 2, b'0', NULL),
(788, 'A0003', 0, 2, b'0', NULL),
(789, 'A0003', 0, 2, b'0', NULL),
(790, 'A0003', 0, 2, b'0', NULL),
(791, 'A0003', 0, 2, b'0', NULL),
(792, 'A0003', 0, 2, b'0', NULL),
(793, 'A0003', 0, 2, b'0', NULL),
(794, 'A0003', 0, 2, b'0', NULL),
(795, 'A0003', 0, 2, b'0', NULL),
(796, 'A0003', 0, 2, b'0', NULL),
(797, 'A0003', 0, 2, b'0', NULL),
(798, 'A0003', 0, 2, b'0', NULL),
(799, 'A0003', 0, 2, b'0', NULL),
(800, 'A0003', 0, 2, b'0', NULL),
(801, 'A0003', 0, 2, b'0', NULL),
(802, 'A0003', 0, 2, b'0', NULL),
(803, 'A0003', 0, 2, b'0', NULL),
(804, 'A0003', 0, 2, b'0', NULL),
(805, 'A0003', 0, 2, b'0', NULL),
(806, 'A0003', 0, 2, b'0', NULL),
(807, 'A0003', 0, 2, b'0', NULL),
(808, 'A0003', 0, 2, b'0', NULL),
(809, 'A0003', 0, 2, b'0', NULL),
(810, 'A0003', 0, 2, b'0', NULL),
(811, 'A0003', 0, 2, b'0', NULL),
(812, 'A0003', 0, 2, b'0', NULL),
(813, 'A0003', 0, 2, b'0', NULL),
(814, 'A0003', 0, 2, b'0', NULL),
(815, 'A0003', 0, 2, b'0', NULL),
(816, 'A0003', 0, 2, b'0', NULL),
(817, 'A0003', 0, 2, b'0', NULL),
(818, 'A0003', 0, 2, b'0', NULL),
(819, 'A0003', 0, 2, b'0', NULL),
(820, 'A0003', 0, 2, b'0', NULL),
(821, 'A0003', 0, 2, b'0', NULL),
(822, 'A0003', 0, 2, b'0', NULL),
(823, 'A0003', 0, 2, b'0', NULL),
(824, 'A0003', 0, 2, b'0', NULL),
(825, 'A0003', 0, 2, b'0', NULL),
(826, 'A0003', 0, 2, b'0', NULL),
(827, 'A0003', 0, 2, b'0', NULL),
(828, 'A0003', 0, 2, b'0', NULL),
(829, 'A0004', 0, 2, b'0', NULL),
(830, 'A0004', 0, 2, b'0', NULL),
(831, 'A0004', 0, 2, b'0', NULL),
(832, 'A0004', 0, 2, b'0', NULL),
(833, 'A0004', 0, 2, b'0', NULL),
(834, 'A0004', 0, 2, b'0', NULL),
(835, 'A0004', 0, 2, b'0', NULL),
(836, 'A0004', 0, 2, b'0', NULL),
(837, 'A0004', 0, 2, b'0', NULL),
(838, 'A0004', 0, 2, b'0', NULL),
(839, 'A0004', 0, 2, b'0', NULL),
(840, 'A0004', 0, 2, b'0', NULL),
(841, 'A0004', 0, 2, b'0', NULL),
(842, 'A0004', 0, 2, b'0', NULL),
(843, 'A0004', 0, 2, b'0', NULL),
(844, 'A0004', 0, 2, b'0', NULL),
(845, 'A0004', 0, 2, b'0', NULL),
(846, 'A0004', 0, 2, b'0', NULL),
(847, 'A0004', 0, 2, b'0', NULL),
(848, 'A0004', 0, 2, b'0', NULL),
(849, 'A0004', 0, 2, b'0', NULL),
(850, 'A0004', 0, 2, b'0', NULL),
(851, 'A0004', 0, 2, b'0', NULL),
(852, 'A0004', 0, 2, b'0', NULL),
(853, 'A0004', 0, 2, b'0', NULL),
(854, 'A0004', 0, 2, b'0', NULL),
(855, 'A0004', 0, 2, b'0', NULL),
(856, 'A0004', 0, 2, b'0', NULL),
(857, 'A0004', 0, 2, b'0', NULL),
(858, 'A0004', 0, 2, b'0', NULL),
(859, 'A0004', 0, 2, b'0', NULL),
(860, 'A0004', 0, 2, b'0', NULL),
(861, 'A0004', 0, 2, b'0', NULL),
(862, 'A0004', 0, 2, b'0', NULL),
(863, 'A0004', 0, 2, b'0', NULL),
(864, 'A0004', 0, 2, b'0', NULL),
(865, 'A0004', 0, 2, b'0', NULL),
(866, 'A0004', 0, 2, b'0', NULL),
(867, 'A0004', 0, 2, b'0', NULL),
(868, 'A0004', 0, 2, b'0', NULL),
(869, 'A0004', 0, 2, b'0', NULL),
(870, 'A0004', 0, 2, b'0', NULL),
(871, 'A0004', 0, 2, b'0', NULL),
(872, 'A0004', 0, 2, b'0', NULL),
(873, 'A0004', 0, 2, b'0', NULL),
(874, 'A0004', 0, 2, b'0', NULL),
(875, 'A0004', 0, 2, b'0', NULL),
(876, 'A0004', 0, 2, b'0', NULL),
(877, 'A0004', 0, 2, b'0', NULL),
(878, 'A0004', 0, 2, b'0', NULL),
(879, 'A0004', 0, 2, b'0', NULL),
(880, 'A0004', 0, 2, b'0', NULL),
(881, 'A0004', 0, 2, b'0', NULL),
(882, 'A0004', 0, 2, b'0', NULL),
(883, 'A0004', 0, 2, b'0', NULL),
(884, 'A0004', 0, 2, b'0', NULL),
(885, 'A0004', 0, 2, b'0', NULL),
(886, 'A0004', 0, 2, b'0', NULL),
(887, 'A0004', 0, 2, b'0', NULL),
(888, 'A0004', 0, 2, b'0', NULL),
(889, 'A0004', 0, 2, b'0', NULL),
(890, 'A0004', 0, 2, b'0', NULL),
(891, 'A0004', 0, 2, b'0', NULL),
(892, 'A0004', 0, 2, b'0', NULL),
(893, 'A0004', 0, 2, b'0', NULL),
(894, 'A0004', 0, 2, b'0', NULL),
(895, 'A0004', 0, 2, b'0', NULL),
(896, 'A0004', 0, 2, b'0', NULL),
(897, 'A0004', 0, 2, b'0', NULL),
(898, 'A0004', 0, 2, b'0', NULL),
(899, 'A0004', 0, 2, b'0', NULL),
(900, 'A0004', 0, 2, b'0', NULL),
(901, 'A0004', 0, 2, b'0', NULL),
(902, 'A0004', 0, 2, b'0', NULL),
(903, 'A0004', 0, 2, b'0', NULL),
(904, 'A0004', 0, 2, b'0', NULL),
(905, 'A0004', 0, 2, b'0', NULL),
(906, 'A0004', 0, 2, b'0', NULL),
(907, 'A0004', 0, 2, b'0', NULL),
(908, 'A0004', 0, 2, b'0', NULL),
(909, 'A0004', 0, 2, b'0', NULL),
(910, 'A0004', 0, 2, b'0', NULL),
(911, 'A0004', 0, 2, b'0', NULL),
(912, 'A0004', 0, 2, b'0', NULL),
(913, 'A0004', 0, 2, b'0', NULL),
(914, 'A0004', 0, 2, b'0', NULL),
(915, 'A0004', 0, 2, b'0', NULL),
(916, 'A0004', 0, 2, b'0', NULL),
(917, 'A0004', 0, 2, b'0', NULL),
(918, 'A0004', 0, 2, b'0', NULL),
(919, 'A0004', 0, 2, b'0', NULL),
(920, 'A0004', 0, 2, b'0', NULL),
(921, 'A0004', 0, 2, b'0', NULL),
(922, 'A0004', 0, 2, b'0', NULL),
(923, 'A0004', 0, 2, b'0', NULL),
(924, 'A0004', 0, 2, b'0', NULL),
(925, 'A0004', 0, 2, b'0', NULL),
(926, 'A0004', 0, 2, b'0', NULL),
(927, 'A0004', 0, 2, b'0', NULL),
(928, 'A0004', 0, 2, b'0', NULL),
(929, 'A0004', 0, 2, b'0', NULL),
(930, 'A0004', 0, 2, b'0', NULL),
(931, 'A0004', 0, 2, b'0', NULL),
(932, 'A0004', 0, 2, b'0', NULL),
(933, 'A0004', 0, 2, b'0', NULL),
(934, 'A0004', 0, 2, b'0', NULL),
(935, 'A0004', 0, 2, b'0', NULL),
(936, 'A0004', 0, 2, b'0', NULL),
(937, 'A0004', 0, 2, b'0', NULL),
(938, 'A0004', 0, 2, b'0', NULL),
(939, 'A0004', 0, 2, b'0', NULL),
(940, 'A0004', 0, 2, b'0', NULL),
(941, 'A0004', 0, 2, b'0', NULL),
(942, 'A0004', 0, 2, b'0', NULL),
(943, 'A0004', 0, 2, b'0', NULL),
(944, 'A0004', 0, 2, b'0', NULL),
(945, 'A0004', 0, 2, b'0', NULL),
(946, 'B0001', 1, 1, b'0', NULL),
(947, 'B0001', 1, 1, b'0', NULL),
(948, 'B0001', 1, 1, b'0', NULL),
(949, 'B0001', 1, 1, b'0', NULL),
(950, 'B0001', 1, 1, b'0', NULL),
(951, 'B0001', 0, 2, b'0', NULL),
(952, 'B0001', 0, 2, b'0', NULL),
(953, 'B0001', 0, 2, b'0', NULL),
(954, 'B0001', 0, 2, b'0', NULL),
(955, 'B0001', 0, 2, b'0', NULL),
(956, 'B0001', 0, 2, b'0', NULL),
(957, 'B0001', 0, 2, b'0', NULL),
(958, 'B0001', 0, 2, b'0', NULL),
(959, 'B0001', 0, 2, b'0', NULL),
(960, 'B0001', 0, 2, b'0', NULL),
(961, 'B0001', 0, 2, b'0', NULL),
(962, 'B0001', 0, 2, b'0', NULL),
(963, 'B0001', 0, 2, b'0', NULL),
(964, 'B0001', 0, 2, b'0', NULL),
(965, 'B0001', 0, 2, b'0', NULL),
(966, 'B0001', 0, 2, b'0', NULL),
(967, 'B0001', 0, 2, b'0', NULL),
(968, 'B0001', 0, 2, b'0', NULL),
(969, 'B0001', 0, 2, b'0', NULL),
(970, 'B0001', 0, 2, b'0', NULL),
(971, 'B0001', 0, 2, b'0', NULL),
(972, 'B0001', 0, 2, b'0', NULL),
(973, 'B0001', 0, 2, b'0', NULL),
(974, 'B0001', 0, 2, b'0', NULL),
(975, 'B0001', 0, 2, b'0', NULL),
(976, 'B0001', 0, 2, b'0', NULL),
(977, 'B0001', 0, 2, b'0', NULL),
(978, 'B0001', 0, 2, b'0', NULL),
(979, 'B0001', 0, 2, b'0', NULL),
(980, 'B0001', 0, 2, b'0', NULL),
(981, 'B0001', 0, 2, b'0', NULL),
(982, 'B0001', 0, 2, b'0', NULL),
(983, 'B0001', 0, 2, b'0', NULL),
(984, 'B0001', 0, 2, b'0', NULL),
(985, 'B0001', 0, 2, b'0', NULL),
(986, 'B0001', 0, 2, b'0', NULL),
(987, 'B0001', 0, 2, b'0', NULL),
(988, 'B0001', 0, 2, b'0', NULL),
(989, 'B0001', 0, 2, b'0', NULL),
(990, 'B0001', 0, 2, b'0', NULL),
(991, 'B0001', 0, 2, b'0', NULL),
(992, 'B0001', 0, 2, b'0', NULL),
(993, 'B0001', 0, 2, b'0', NULL),
(994, 'B0001', 0, 2, b'0', NULL),
(995, 'B0001', 0, 2, b'0', NULL),
(996, 'B0001', 0, 2, b'0', NULL),
(997, 'B0001', 0, 2, b'0', NULL),
(998, 'B0001', 0, 2, b'0', NULL),
(999, 'B0001', 0, 2, b'0', NULL),
(1000, 'B0001', 0, 2, b'0', NULL),
(1001, 'B0001', 0, 2, b'0', NULL),
(1002, 'B0001', 0, 2, b'0', NULL),
(1003, 'B0001', 0, 2, b'0', NULL),
(1004, 'B0001', 0, 2, b'0', NULL),
(1005, 'B0001', 0, 2, b'0', NULL),
(1006, 'B0001', 0, 2, b'0', NULL),
(1007, 'B0001', 0, 2, b'0', NULL),
(1008, 'B0001', 0, 2, b'0', NULL),
(1009, 'B0001', 0, 2, b'0', NULL),
(1010, 'B0001', 0, 2, b'0', NULL),
(1011, 'B0001', 0, 2, b'0', NULL),
(1012, 'B0001', 0, 2, b'0', NULL),
(1013, 'B0001', 0, 2, b'0', NULL),
(1014, 'B0001', 0, 2, b'0', NULL),
(1015, 'B0001', 0, 2, b'0', NULL),
(1016, 'B0001', 0, 2, b'0', NULL),
(1017, 'B0001', 0, 2, b'0', NULL),
(1018, 'B0001', 0, 2, b'0', NULL),
(1019, 'B0001', 0, 2, b'0', NULL),
(1020, 'B0001', 0, 2, b'0', NULL),
(1021, 'B0001', 0, 2, b'0', NULL),
(1022, 'B0001', 0, 2, b'0', NULL),
(1023, 'B0001', 0, 2, b'0', NULL),
(1024, 'B0001', 0, 2, b'0', NULL),
(1025, 'B0001', 0, 2, b'0', NULL),
(1026, 'B0001', 0, 2, b'0', NULL),
(1027, 'B0001', 0, 2, b'0', NULL),
(1028, 'B0001', 0, 2, b'0', NULL),
(1029, 'B0001', 0, 2, b'0', NULL),
(1030, 'B0001', 0, 2, b'0', NULL),
(1031, 'B0001', 0, 2, b'0', NULL),
(1032, 'B0001', 0, 2, b'0', NULL),
(1033, 'B0001', 0, 2, b'0', NULL),
(1034, 'B0001', 0, 2, b'0', NULL),
(1035, 'B0001', 0, 2, b'0', NULL),
(1036, 'B0001', 0, 2, b'0', NULL),
(1037, 'B0001', 0, 2, b'0', NULL),
(1038, 'B0001', 0, 2, b'0', NULL),
(1039, 'B0001', 0, 2, b'0', NULL),
(1040, 'B0001', 0, 2, b'0', NULL),
(1041, 'B0001', 0, 2, b'0', NULL),
(1042, 'B0001', 0, 2, b'0', NULL),
(1043, 'B0001', 0, 2, b'0', NULL),
(1044, 'B0001', 0, 2, b'0', NULL),
(1045, 'B0001', 0, 2, b'0', NULL),
(1046, 'B0001', 0, 2, b'0', NULL),
(1047, 'B0001', 0, 2, b'0', NULL),
(1048, 'B0001', 0, 2, b'0', NULL),
(1049, 'B0001', 0, 2, b'0', NULL),
(1050, 'B0001', 0, 2, b'0', NULL),
(1051, 'B0001', 0, 2, b'0', NULL),
(1052, 'B0001', 0, 2, b'0', NULL),
(1053, 'B0001', 0, 2, b'0', NULL),
(1054, 'B0001', 0, 2, b'0', NULL),
(1055, 'B0001', 0, 2, b'0', NULL),
(1056, 'B0001', 0, 2, b'0', NULL),
(1057, 'B0001', 0, 2, b'0', NULL),
(1058, 'B0001', 0, 2, b'0', NULL),
(1059, 'B0001', 0, 2, b'0', NULL),
(1060, 'B0001', 0, 2, b'0', NULL),
(1061, 'B0001', 0, 2, b'0', NULL),
(1062, 'B0001', 0, 2, b'0', NULL),
(1063, 'B0002', 1, 1, b'0', NULL),
(1064, 'B0002', 0, 2, b'0', NULL),
(1065, 'B0002', 0, 2, b'0', NULL),
(1066, 'B0002', 0, 2, b'0', NULL),
(1067, 'B0002', 0, 2, b'0', NULL),
(1068, 'B0002', 0, 2, b'0', NULL),
(1069, 'B0002', 0, 2, b'0', NULL),
(1070, 'B0002', 0, 2, b'0', NULL),
(1071, 'B0002', 0, 2, b'0', NULL),
(1072, 'B0002', 0, 2, b'0', NULL),
(1073, 'B0002', 0, 2, b'0', NULL),
(1074, 'B0002', 0, 2, b'0', NULL),
(1075, 'B0002', 0, 2, b'0', NULL),
(1076, 'B0002', 0, 2, b'0', NULL),
(1077, 'B0002', 0, 2, b'0', NULL),
(1078, 'B0002', 0, 2, b'0', NULL),
(1079, 'B0002', 0, 2, b'0', NULL),
(1080, 'B0002', 0, 2, b'0', NULL),
(1081, 'B0002', 0, 2, b'0', NULL),
(1082, 'B0002', 0, 2, b'0', NULL),
(1083, 'B0002', 0, 2, b'0', NULL),
(1084, 'B0002', 0, 2, b'0', NULL),
(1085, 'B0002', 0, 2, b'0', NULL),
(1086, 'B0002', 0, 2, b'0', NULL),
(1087, 'B0002', 0, 2, b'0', NULL),
(1088, 'B0002', 0, 2, b'0', NULL),
(1089, 'B0002', 0, 2, b'0', NULL),
(1090, 'B0002', 0, 2, b'0', NULL),
(1091, 'B0002', 0, 2, b'0', NULL),
(1092, 'B0002', 0, 2, b'0', NULL),
(1093, 'B0002', 0, 2, b'0', NULL),
(1094, 'B0002', 0, 2, b'0', NULL),
(1095, 'B0002', 0, 2, b'0', NULL),
(1096, 'B0002', 0, 2, b'0', NULL),
(1097, 'B0002', 0, 2, b'0', NULL),
(1098, 'B0002', 0, 2, b'0', NULL),
(1099, 'B0002', 0, 2, b'0', NULL),
(1100, 'B0002', 0, 2, b'0', NULL),
(1101, 'B0002', 0, 2, b'0', NULL),
(1102, 'B0002', 0, 2, b'0', NULL),
(1103, 'B0002', 0, 2, b'0', NULL),
(1104, 'B0002', 0, 2, b'0', NULL),
(1105, 'B0002', 0, 2, b'0', NULL),
(1106, 'B0002', 0, 2, b'0', NULL),
(1107, 'B0002', 0, 2, b'0', NULL),
(1108, 'B0002', 0, 2, b'0', NULL),
(1109, 'B0002', 0, 2, b'0', NULL),
(1110, 'B0002', 0, 2, b'0', NULL),
(1111, 'B0002', 0, 2, b'0', NULL),
(1112, 'B0002', 0, 2, b'0', NULL),
(1113, 'B0002', 0, 2, b'0', NULL),
(1114, 'B0002', 0, 2, b'0', NULL),
(1115, 'B0002', 0, 2, b'0', NULL),
(1116, 'B0002', 0, 2, b'0', NULL),
(1117, 'B0002', 0, 2, b'0', NULL),
(1118, 'B0002', 0, 2, b'0', NULL),
(1119, 'B0002', 0, 2, b'0', NULL),
(1120, 'B0002', 0, 2, b'0', NULL),
(1121, 'B0002', 0, 2, b'0', NULL),
(1122, 'B0002', 0, 2, b'0', NULL),
(1123, 'B0002', 0, 2, b'0', NULL),
(1124, 'B0002', 0, 2, b'0', NULL),
(1125, 'B0002', 0, 2, b'0', NULL),
(1126, 'B0002', 0, 2, b'0', NULL),
(1127, 'B0002', 0, 2, b'0', NULL),
(1128, 'B0002', 0, 2, b'0', NULL),
(1129, 'B0002', 0, 2, b'0', NULL),
(1130, 'B0002', 0, 2, b'0', NULL),
(1131, 'B0002', 0, 2, b'0', NULL),
(1132, 'B0002', 0, 2, b'0', NULL),
(1133, 'B0002', 0, 2, b'0', NULL),
(1134, 'B0002', 0, 2, b'0', NULL),
(1135, 'B0002', 0, 2, b'0', NULL),
(1136, 'B0002', 0, 2, b'0', NULL),
(1137, 'B0002', 0, 2, b'0', NULL),
(1138, 'B0002', 0, 2, b'0', NULL),
(1139, 'B0002', 0, 2, b'0', NULL),
(1140, 'B0002', 0, 2, b'0', NULL),
(1141, 'B0002', 0, 2, b'0', NULL),
(1142, 'B0002', 0, 2, b'0', NULL),
(1143, 'B0002', 0, 2, b'0', NULL),
(1144, 'B0002', 0, 2, b'0', NULL),
(1145, 'B0002', 0, 2, b'0', NULL),
(1146, 'B0002', 0, 2, b'0', NULL),
(1147, 'B0002', 0, 2, b'0', NULL),
(1148, 'B0002', 0, 2, b'0', NULL),
(1149, 'B0002', 0, 2, b'0', NULL),
(1150, 'B0002', 0, 2, b'0', NULL),
(1151, 'B0002', 0, 2, b'0', NULL),
(1152, 'B0002', 0, 2, b'0', NULL),
(1153, 'B0002', 0, 2, b'0', NULL),
(1154, 'B0002', 0, 2, b'0', NULL),
(1155, 'B0002', 0, 2, b'0', NULL),
(1156, 'B0002', 0, 2, b'0', NULL),
(1157, 'B0002', 0, 2, b'0', NULL),
(1158, 'B0002', 0, 2, b'0', NULL),
(1159, 'B0002', 0, 2, b'0', NULL),
(1160, 'B0002', 0, 2, b'0', NULL),
(1161, 'B0002', 0, 2, b'0', NULL),
(1162, 'B0002', 0, 2, b'0', NULL),
(1163, 'B0002', 0, 2, b'0', NULL),
(1164, 'B0002', 0, 2, b'0', NULL),
(1165, 'B0002', 0, 2, b'0', NULL),
(1166, 'B0002', 0, 2, b'0', NULL),
(1167, 'B0002', 0, 2, b'0', NULL),
(1168, 'B0002', 0, 2, b'0', NULL),
(1169, 'B0002', 0, 2, b'0', NULL),
(1170, 'B0002', 0, 2, b'0', NULL),
(1171, 'B0002', 0, 2, b'0', NULL),
(1172, 'B0002', 0, 2, b'0', NULL),
(1173, 'B0002', 0, 2, b'0', NULL),
(1174, 'B0002', 0, 2, b'0', NULL),
(1175, 'B0002', 0, 2, b'0', NULL),
(1176, 'B0002', 0, 2, b'0', NULL),
(1177, 'B0002', 0, 2, b'0', NULL),
(1178, 'B0002', 0, 2, b'0', NULL),
(1179, 'B0002', 0, 2, b'0', NULL),
(1180, 'B0003', 0, 2, b'0', NULL),
(1181, 'B0003', 0, 2, b'0', NULL),
(1182, 'B0003', 0, 2, b'0', NULL),
(1183, 'B0003', 0, 2, b'0', NULL),
(1184, 'B0003', 0, 2, b'0', NULL),
(1185, 'B0003', 0, 2, b'0', NULL),
(1186, 'B0003', 0, 2, b'0', NULL),
(1187, 'B0003', 0, 2, b'0', NULL),
(1188, 'B0003', 0, 2, b'0', NULL),
(1189, 'B0003', 0, 2, b'0', NULL),
(1190, 'B0003', 0, 2, b'0', NULL),
(1191, 'B0003', 0, 2, b'0', NULL),
(1192, 'B0003', 0, 2, b'0', NULL),
(1193, 'B0003', 0, 2, b'0', NULL),
(1194, 'B0003', 0, 2, b'0', NULL),
(1195, 'B0003', 0, 2, b'0', NULL),
(1196, 'B0003', 0, 2, b'0', NULL),
(1197, 'B0003', 0, 2, b'0', NULL),
(1198, 'B0003', 0, 2, b'0', NULL),
(1199, 'B0003', 0, 2, b'0', NULL),
(1200, 'B0003', 0, 2, b'0', NULL),
(1201, 'B0003', 0, 2, b'0', NULL),
(1202, 'B0003', 0, 2, b'0', NULL),
(1203, 'B0003', 0, 2, b'0', NULL),
(1204, 'B0003', 0, 2, b'0', NULL),
(1205, 'B0003', 0, 2, b'0', NULL),
(1206, 'B0003', 0, 2, b'0', NULL),
(1207, 'B0003', 0, 2, b'0', NULL),
(1208, 'B0003', 0, 2, b'0', NULL),
(1209, 'B0003', 0, 2, b'0', NULL),
(1210, 'B0003', 0, 2, b'0', NULL),
(1211, 'B0003', 0, 2, b'0', NULL),
(1212, 'B0003', 0, 2, b'0', NULL),
(1213, 'B0003', 0, 2, b'0', NULL),
(1214, 'B0003', 0, 2, b'0', NULL),
(1215, 'B0003', 0, 2, b'0', NULL),
(1216, 'B0003', 0, 2, b'0', NULL),
(1217, 'B0003', 0, 2, b'0', NULL),
(1218, 'B0003', 0, 2, b'0', NULL),
(1219, 'B0003', 0, 2, b'0', NULL),
(1220, 'B0003', 0, 2, b'0', NULL),
(1221, 'B0003', 0, 2, b'0', NULL),
(1222, 'B0003', 0, 2, b'0', NULL),
(1223, 'B0003', 0, 2, b'0', NULL),
(1224, 'B0003', 0, 2, b'0', NULL),
(1225, 'B0003', 0, 2, b'0', NULL),
(1226, 'B0003', 0, 2, b'0', NULL),
(1227, 'B0003', 0, 2, b'0', NULL),
(1228, 'B0003', 0, 2, b'0', NULL),
(1229, 'B0003', 0, 2, b'0', NULL),
(1230, 'B0003', 0, 2, b'0', NULL),
(1231, 'B0003', 0, 2, b'0', NULL),
(1232, 'B0003', 0, 2, b'0', NULL),
(1233, 'B0003', 0, 2, b'0', NULL),
(1234, 'B0003', 0, 2, b'0', NULL),
(1235, 'B0003', 0, 2, b'0', NULL),
(1236, 'B0003', 0, 2, b'0', NULL),
(1237, 'B0003', 0, 2, b'0', NULL),
(1238, 'B0003', 0, 2, b'0', NULL),
(1239, 'B0003', 0, 2, b'0', NULL),
(1240, 'B0003', 0, 2, b'0', NULL),
(1241, 'B0003', 0, 2, b'0', NULL),
(1242, 'B0003', 0, 2, b'0', NULL),
(1243, 'B0003', 0, 2, b'0', NULL),
(1244, 'B0003', 0, 2, b'0', NULL),
(1245, 'B0003', 0, 2, b'0', NULL),
(1246, 'B0003', 0, 2, b'0', NULL),
(1247, 'B0003', 0, 2, b'0', NULL),
(1248, 'B0003', 0, 2, b'0', NULL),
(1249, 'B0003', 0, 2, b'0', NULL),
(1250, 'B0003', 0, 2, b'0', NULL),
(1251, 'B0003', 0, 2, b'0', NULL),
(1252, 'B0003', 0, 2, b'0', NULL),
(1253, 'B0003', 0, 2, b'0', NULL),
(1254, 'B0003', 0, 2, b'0', NULL),
(1255, 'B0003', 0, 2, b'0', NULL),
(1256, 'B0003', 0, 2, b'0', NULL),
(1257, 'B0003', 0, 2, b'0', NULL),
(1258, 'B0003', 0, 2, b'0', NULL),
(1259, 'B0003', 0, 2, b'0', NULL),
(1260, 'B0003', 0, 2, b'0', NULL),
(1261, 'B0003', 0, 2, b'0', NULL),
(1262, 'B0003', 0, 2, b'0', NULL),
(1263, 'B0003', 0, 2, b'0', NULL),
(1264, 'B0003', 0, 2, b'0', NULL),
(1265, 'B0003', 0, 2, b'0', NULL),
(1266, 'B0003', 0, 2, b'0', NULL),
(1267, 'B0003', 0, 2, b'0', NULL),
(1268, 'B0003', 0, 2, b'0', NULL),
(1269, 'B0003', 0, 2, b'0', NULL),
(1270, 'B0003', 0, 2, b'0', NULL),
(1271, 'B0003', 0, 2, b'0', NULL),
(1272, 'B0003', 0, 2, b'0', NULL),
(1273, 'B0003', 0, 2, b'0', NULL),
(1274, 'B0003', 0, 2, b'0', NULL),
(1275, 'B0003', 0, 2, b'0', NULL),
(1276, 'B0003', 0, 2, b'0', NULL),
(1277, 'B0003', 0, 2, b'0', NULL),
(1278, 'B0003', 0, 2, b'0', NULL),
(1279, 'B0003', 0, 2, b'0', NULL),
(1280, 'B0003', 0, 2, b'0', NULL),
(1281, 'B0003', 0, 2, b'0', NULL),
(1282, 'B0003', 0, 2, b'0', NULL),
(1283, 'B0003', 0, 2, b'0', NULL),
(1284, 'B0003', 0, 2, b'0', NULL),
(1285, 'B0003', 0, 2, b'0', NULL),
(1286, 'B0003', 0, 2, b'0', NULL),
(1287, 'B0003', 0, 2, b'0', NULL),
(1288, 'B0003', 0, 2, b'0', NULL),
(1289, 'B0003', 0, 2, b'0', NULL),
(1290, 'B0003', 0, 2, b'0', NULL),
(1291, 'B0003', 0, 2, b'0', NULL),
(1292, 'B0003', 0, 2, b'0', NULL),
(1293, 'B0003', 0, 2, b'0', NULL),
(1294, 'B0003', 0, 2, b'0', NULL),
(1295, 'B0003', 0, 2, b'0', NULL),
(1296, 'B0003', 0, 2, b'0', NULL),
(1297, 'B0003', 0, 2, b'0', NULL),
(1298, 'B0003', 0, 2, b'0', NULL),
(1299, 'B0003', 0, 2, b'0', NULL),
(1300, 'B0003', 0, 2, b'0', NULL),
(1301, 'B0003', 0, 2, b'0', NULL),
(1302, 'B0003', 0, 2, b'0', NULL),
(1303, 'B0003', 0, 2, b'0', NULL),
(1304, 'B0003', 0, 2, b'0', NULL),
(1305, 'B0003', 0, 2, b'0', NULL),
(1306, 'B0003', 0, 2, b'0', NULL),
(1307, 'B0003', 0, 2, b'0', NULL),
(1308, 'B0003', 0, 2, b'0', NULL),
(1309, 'B0003', 0, 2, b'0', NULL),
(1310, 'B0003', 0, 2, b'0', NULL),
(1311, 'B0003', 0, 2, b'0', NULL),
(1312, 'B0003', 0, 2, b'0', NULL),
(1313, 'B0003', 0, 2, b'0', NULL),
(1314, 'B0003', 0, 2, b'0', NULL),
(1315, 'B0003', 0, 2, b'0', NULL),
(1316, 'B0003', 0, 2, b'0', NULL),
(1317, 'B0003', 0, 2, b'0', NULL),
(1318, 'B0003', 0, 2, b'0', NULL),
(1319, 'B0003', 0, 2, b'0', NULL),
(1320, 'B0003', 0, 2, b'0', NULL),
(1321, 'B0003', 0, 2, b'0', NULL),
(1322, 'B0003', 0, 2, b'0', NULL),
(1323, 'B0003', 0, 2, b'0', NULL),
(1324, 'B0003', 0, 2, b'0', NULL),
(1325, 'B0003', 0, 2, b'0', NULL),
(1326, 'B0003', 0, 2, b'0', NULL),
(1327, 'B0003', 0, 2, b'0', NULL),
(1328, 'B0003', 0, 2, b'0', NULL),
(1329, 'B0003', 0, 2, b'0', NULL),
(1330, 'B0003', 0, 2, b'0', NULL),
(1331, 'B0003', 0, 2, b'0', NULL),
(1332, 'B0003', 0, 2, b'0', NULL),
(1333, 'B0003', 0, 2, b'0', NULL),
(1334, 'B0003', 0, 2, b'0', NULL),
(1335, 'B0003', 0, 2, b'0', NULL),
(1336, 'B0003', 0, 2, b'0', NULL),
(1337, 'B0003', 0, 2, b'0', NULL),
(1338, 'B0003', 0, 2, b'0', NULL),
(1339, 'B0003', 0, 2, b'0', NULL),
(1340, 'B0003', 0, 2, b'0', NULL),
(1341, 'B0003', 0, 2, b'0', NULL),
(1342, 'B0005', 0, 2, b'0', NULL),
(1343, 'B0005', 0, 2, b'0', NULL),
(1344, 'B0005', 0, 2, b'0', NULL),
(1345, 'B0005', 0, 2, b'0', NULL),
(1346, 'B0005', 0, 2, b'0', NULL),
(1347, 'B0005', 0, 2, b'0', NULL),
(1348, 'B0005', 0, 2, b'0', NULL),
(1349, 'B0005', 0, 2, b'0', NULL),
(1350, 'B0005', 0, 2, b'0', NULL),
(1351, 'B0005', 0, 2, b'0', NULL),
(1352, 'B0005', 0, 2, b'0', NULL),
(1353, 'B0005', 0, 2, b'0', NULL),
(1354, 'B0005', 0, 2, b'0', NULL),
(1355, 'B0005', 0, 2, b'0', NULL),
(1356, 'B0005', 0, 2, b'0', NULL),
(1357, 'B0005', 0, 2, b'0', NULL),
(1358, 'B0005', 0, 2, b'0', NULL),
(1359, 'B0005', 0, 2, b'0', NULL),
(1360, 'B0005', 0, 2, b'0', NULL),
(1361, 'B0005', 0, 2, b'0', NULL),
(1362, 'B0005', 0, 2, b'0', NULL),
(1363, 'B0005', 0, 2, b'0', NULL),
(1364, 'B0005', 0, 2, b'0', NULL),
(1365, 'B0005', 0, 2, b'0', NULL),
(1366, 'B0005', 0, 2, b'0', NULL),
(1367, 'B0005', 0, 2, b'0', NULL),
(1368, 'B0005', 0, 2, b'0', NULL),
(1369, 'B0005', 0, 2, b'0', NULL),
(1370, 'B0005', 0, 2, b'0', NULL),
(1371, 'B0005', 0, 2, b'0', NULL),
(1372, 'B0005', 0, 2, b'0', NULL),
(1373, 'B0005', 0, 2, b'0', NULL),
(1374, 'B0005', 0, 2, b'0', NULL),
(1375, 'B0005', 0, 2, b'0', NULL),
(1376, 'B0005', 0, 2, b'0', NULL),
(1377, 'B0005', 0, 2, b'0', NULL),
(1378, 'B0005', 0, 2, b'0', NULL),
(1379, 'B0005', 0, 2, b'0', NULL),
(1380, 'B0005', 0, 2, b'0', NULL),
(1381, 'B0005', 0, 2, b'0', NULL),
(1382, 'B0005', 0, 2, b'0', NULL),
(1383, 'B0005', 0, 2, b'0', NULL),
(1384, 'B0005', 0, 2, b'0', NULL),
(1385, 'B0005', 0, 2, b'0', NULL),
(1386, 'B0005', 0, 2, b'0', NULL),
(1387, 'B0005', 0, 2, b'0', NULL),
(1388, 'B0005', 0, 2, b'0', NULL),
(1389, 'B0005', 0, 2, b'0', NULL),
(1390, 'B0005', 0, 2, b'0', NULL),
(1391, 'B0005', 0, 2, b'0', NULL),
(1392, 'B0005', 0, 2, b'0', NULL),
(1393, 'B0005', 0, 2, b'0', NULL),
(1394, 'B0005', 0, 2, b'0', NULL),
(1395, 'B0005', 0, 2, b'0', NULL),
(1396, 'B0005', 0, 2, b'0', NULL),
(1397, 'B0005', 0, 2, b'0', NULL),
(1398, 'B0005', 0, 2, b'0', NULL),
(1399, 'B0005', 0, 2, b'0', NULL),
(1400, 'B0005', 0, 2, b'0', NULL),
(1401, 'B0005', 0, 2, b'0', NULL),
(1402, 'B0005', 0, 2, b'0', NULL),
(1403, 'B0005', 0, 2, b'0', NULL),
(1404, 'B0005', 0, 2, b'0', NULL),
(1405, 'B0005', 0, 2, b'0', NULL),
(1406, 'B0005', 0, 2, b'0', NULL),
(1407, 'B0005', 0, 2, b'0', NULL),
(1408, 'B0005', 0, 2, b'0', NULL),
(1409, 'B0005', 0, 2, b'0', NULL),
(1410, 'B0005', 0, 2, b'0', NULL),
(1411, 'B0005', 0, 2, b'0', NULL),
(1412, 'B0005', 0, 2, b'0', NULL),
(1413, 'B0005', 0, 2, b'0', NULL),
(1414, 'B0005', 0, 2, b'0', NULL),
(1415, 'B0005', 0, 2, b'0', NULL),
(1416, 'B0005', 0, 2, b'0', NULL),
(1417, 'B0005', 0, 2, b'0', NULL),
(1418, 'B0005', 0, 2, b'0', NULL),
(1419, 'B0005', 0, 2, b'0', NULL),
(1420, 'B0005', 0, 2, b'0', NULL),
(1421, 'B0005', 0, 2, b'0', NULL),
(1422, 'B0005', 0, 2, b'0', NULL),
(1423, 'B0005', 0, 2, b'0', NULL),
(1424, 'B0005', 0, 2, b'0', NULL),
(1425, 'B0005', 0, 2, b'0', NULL),
(1426, 'B0005', 0, 2, b'0', NULL),
(1427, 'B0005', 0, 2, b'0', NULL),
(1428, 'B0005', 0, 2, b'0', NULL),
(1429, 'B0005', 0, 2, b'0', NULL),
(1430, 'B0005', 0, 2, b'0', NULL),
(1431, 'B0005', 0, 2, b'0', NULL),
(1432, 'B0005', 0, 2, b'0', NULL),
(1433, 'B0005', 0, 2, b'0', NULL),
(1434, 'B0005', 0, 2, b'0', NULL),
(1435, 'B0005', 0, 2, b'0', NULL),
(1436, 'B0005', 0, 2, b'0', NULL),
(1437, 'B0005', 0, 2, b'0', NULL),
(1438, 'B0005', 0, 2, b'0', NULL),
(1439, 'B0005', 0, 2, b'0', NULL),
(1440, 'B0005', 0, 2, b'0', NULL),
(1441, 'B0005', 0, 2, b'0', NULL),
(1442, 'B0005', 0, 2, b'0', NULL),
(1443, 'B0005', 0, 2, b'0', NULL),
(1444, 'B0005', 0, 2, b'0', NULL),
(1445, 'B0005', 0, 2, b'0', NULL),
(1446, 'B0005', 0, 2, b'0', NULL),
(1447, 'B0005', 0, 2, b'0', NULL),
(1448, 'B0005', 0, 2, b'0', NULL),
(1449, 'B0005', 0, 2, b'0', NULL),
(1450, 'B0005', 0, 2, b'0', NULL),
(1451, 'B0005', 0, 2, b'0', NULL),
(1452, 'B0005', 0, 2, b'0', NULL),
(1453, 'B0005', 0, 2, b'0', NULL),
(1454, 'B0005', 0, 2, b'0', NULL),
(1455, 'B0005', 0, 2, b'0', NULL),
(1456, 'B0005', 0, 2, b'0', NULL),
(1457, 'B0005', 0, 2, b'0', NULL),
(1458, 'B0005', 0, 2, b'0', NULL),
(1459, 'MN555', 1, 1, b'0', NULL),
(1460, 'MN555', 0, 2, b'0', NULL),
(1461, 'MN555', 0, 2, b'0', NULL),
(1462, 'MN555', 0, 2, b'0', NULL),
(1463, 'MN555', 0, 2, b'0', NULL),
(1464, 'MN555', 0, 2, b'0', NULL),
(1465, 'MN555', 0, 2, b'0', NULL),
(1466, 'MN555', 0, 2, b'0', NULL),
(1467, 'MN555', 0, 2, b'0', NULL),
(1468, 'MN555', 0, 2, b'0', NULL),
(1469, 'MN555', 0, 2, b'0', NULL),
(1470, 'MN555', 0, 2, b'0', NULL),
(1471, 'MN555', 0, 2, b'0', NULL),
(1472, 'MN555', 0, 2, b'0', NULL),
(1473, 'MN555', 0, 2, b'0', NULL),
(1474, 'MN555', 0, 2, b'0', NULL),
(1475, 'MN555', 0, 2, b'0', NULL),
(1476, 'MN555', 0, 2, b'0', NULL),
(1477, 'MN555', 0, 2, b'0', NULL),
(1478, 'MN555', 0, 2, b'0', NULL),
(1479, 'MN555', 0, 2, b'0', NULL),
(1480, 'MN555', 0, 2, b'0', NULL),
(1481, 'MN555', 0, 2, b'0', NULL),
(1482, 'MN555', 0, 2, b'0', NULL),
(1483, 'MN555', 0, 2, b'0', NULL),
(1484, 'MN555', 0, 2, b'0', NULL),
(1485, 'MN555', 0, 2, b'0', NULL),
(1486, 'MN555', 0, 2, b'0', NULL),
(1487, 'MN555', 0, 2, b'0', NULL),
(1488, 'MN555', 0, 2, b'0', NULL),
(1489, 'MN555', 0, 2, b'0', NULL),
(1490, 'MN555', 0, 2, b'0', NULL),
(1491, 'MN555', 0, 2, b'0', NULL),
(1492, 'MN555', 0, 2, b'0', NULL),
(1493, 'MN555', 0, 2, b'0', NULL),
(1494, 'MN555', 0, 2, b'0', NULL),
(1495, 'MN555', 0, 2, b'0', NULL),
(1496, 'MN555', 0, 2, b'0', NULL),
(1497, 'MN555', 0, 2, b'0', NULL),
(1498, 'MN555', 0, 2, b'0', NULL),
(1499, 'MN555', 0, 2, b'0', NULL),
(1500, 'MN555', 0, 2, b'0', NULL),
(1501, 'MN555', 0, 2, b'0', NULL),
(1502, 'MN555', 0, 2, b'0', NULL),
(1503, 'MN555', 0, 2, b'0', NULL),
(1504, 'MN555', 0, 2, b'0', NULL),
(1505, 'MN555', 0, 2, b'0', NULL),
(1506, 'MN555', 0, 2, b'0', NULL),
(1507, 'MN555', 0, 2, b'0', NULL),
(1508, 'MN555', 0, 2, b'0', NULL),
(1509, 'MN555', 0, 2, b'0', NULL),
(1510, 'MN555', 0, 2, b'0', NULL),
(1511, 'MN555', 0, 2, b'0', NULL),
(1512, 'MN555', 0, 2, b'0', NULL),
(1513, 'MN555', 0, 2, b'0', NULL),
(1514, 'MN555', 0, 2, b'0', NULL),
(1515, 'MN555', 0, 2, b'0', NULL),
(1516, 'MN555', 0, 2, b'0', NULL),
(1517, 'MN555', 0, 2, b'0', NULL),
(1518, 'MN555', 0, 2, b'0', NULL),
(1519, 'MN555', 0, 2, b'0', NULL),
(1520, 'MN555', 0, 2, b'0', NULL),
(1521, 'MN555', 0, 2, b'0', NULL),
(1522, 'MN555', 0, 2, b'0', NULL),
(1523, 'MN555', 0, 2, b'0', NULL),
(1524, 'MN555', 0, 2, b'0', NULL),
(1525, 'MN555', 0, 2, b'0', NULL),
(1526, 'MN555', 0, 2, b'0', NULL),
(1527, 'MN555', 0, 2, b'0', NULL),
(1528, 'MN555', 0, 2, b'0', NULL),
(1529, 'MN555', 0, 2, b'0', NULL),
(1530, 'MN555', 0, 2, b'0', NULL),
(1531, 'MN555', 0, 2, b'0', NULL),
(1532, 'MN555', 0, 2, b'0', NULL),
(1533, 'MN555', 0, 2, b'0', NULL),
(1534, 'MN555', 0, 2, b'0', NULL),
(1535, 'MN555', 0, 2, b'0', NULL),
(1536, 'MN555', 0, 2, b'0', NULL),
(1537, 'MN555', 0, 2, b'0', NULL),
(1538, 'MN555', 0, 2, b'0', NULL),
(1539, 'MN555', 0, 2, b'0', NULL),
(1540, 'MN555', 0, 2, b'0', NULL),
(1541, 'MN555', 0, 2, b'0', NULL),
(1542, 'MN555', 0, 2, b'0', NULL),
(1543, 'MN555', 0, 2, b'0', NULL),
(1544, 'MN555', 0, 2, b'0', NULL),
(1545, 'MN555', 0, 2, b'0', NULL),
(1546, 'MN555', 0, 2, b'0', NULL),
(1547, 'MN555', 0, 2, b'0', NULL),
(1548, 'MN555', 0, 2, b'0', NULL),
(1549, 'MN555', 0, 2, b'0', NULL),
(1550, 'MN555', 0, 2, b'0', NULL),
(1551, 'MN555', 0, 2, b'0', NULL),
(1552, 'MN555', 0, 2, b'0', NULL),
(1553, 'MN555', 0, 2, b'0', NULL),
(1554, 'MN555', 0, 2, b'0', NULL),
(1555, 'MN555', 0, 2, b'0', NULL),
(1556, 'MN555', 0, 2, b'0', NULL),
(1557, 'MN555', 0, 2, b'0', NULL),
(1558, 'MN555', 0, 2, b'0', NULL),
(1559, 'MN555', 0, 2, b'0', NULL),
(1560, 'MN555', 0, 2, b'0', NULL),
(1561, 'MN555', 0, 2, b'0', NULL),
(1562, 'MN555', 0, 2, b'0', NULL),
(1563, 'MN555', 0, 2, b'0', NULL),
(1564, 'MN555', 0, 2, b'0', NULL),
(1565, 'MN555', 0, 2, b'0', NULL),
(1566, 'MN555', 0, 2, b'0', NULL),
(1567, 'MN555', 0, 2, b'0', NULL),
(1568, 'MN555', 0, 2, b'0', NULL),
(1569, 'MN555', 0, 2, b'0', NULL),
(1570, 'MN555', 0, 2, b'0', NULL),
(1571, 'MN555', 0, 2, b'0', NULL),
(1572, 'MN555', 0, 2, b'0', NULL),
(1573, 'MN555', 0, 2, b'0', NULL),
(1574, 'MN555', 0, 2, b'0', NULL),
(1575, 'MN555', 0, 2, b'0', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chitietdatve`
--
ALTER TABLE `chitietdatve`
  ADD PRIMARY KEY (`MADONDATVE`,`MAVE`);

--
-- Indexes for table `chuyenbay`
--
ALTER TABLE `chuyenbay`
  ADD PRIMARY KEY (`MACHUYENBAY`),
  ADD KEY `SANBAYDI` (`SANBAYDI`),
  ADD KEY `SANBAYDEN` (`SANBAYDEN`),
  ADD KEY `TRANSITION` (`TRANSITION`);

--
-- Indexes for table `diadiem`
--
ALTER TABLE `diadiem`
  ADD PRIMARY KEY (`MADIADIEM`);

--
-- Indexes for table `dondatve`
--
ALTER TABLE `dondatve`
  ADD PRIMARY KEY (`MADONDATVE`),
  ADD KEY `MADONDATVE` (`MADONDATVE`);

--
-- Indexes for table `historychitietdatve`
--
ALTER TABLE `historychitietdatve`
  ADD PRIMARY KEY (`MADONDATVE`,`MAVE`);

--
-- Indexes for table `historydondatve`
--
ALTER TABLE `historydondatve`
  ADD PRIMARY KEY (`MADONDATVE`),
  ADD KEY `MADONDATVE` (`MADONDATVE`);

--
-- Indexes for table `historykhachhang`
--
ALTER TABLE `historykhachhang`
  ADD PRIMARY KEY (`MAKHACHHANG`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`);

--
-- Indexes for table `historyve`
--
ALTER TABLE `historyve`
  ADD PRIMARY KEY (`MAVE`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`MAKHACHHANG`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`);

--
-- Indexes for table `khuyenmai`
--
ALTER TABLE `khuyenmai`
  ADD PRIMARY KEY (`MAKHUYENMAI`);

--
-- Indexes for table `loaive`
--
ALTER TABLE `loaive`
  ADD PRIMARY KEY (`MALOAIVE`);

--
-- Indexes for table `maybay`
--
ALTER TABLE `maybay`
  ADD PRIMARY KEY (`MAMAYBAY`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`MANHANVIEN`);

--
-- Indexes for table `quyen`
--
ALTER TABLE `quyen`
  ADD PRIMARY KEY (`MAQUYEN`);

--
-- Indexes for table `sanbay`
--
ALTER TABLE `sanbay`
  ADD PRIMARY KEY (`MASANBAY`);

--
-- Indexes for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`MAQUYEN`,`MANHANVIEN`);

--
-- Indexes for table `thamso`
--
ALTER TABLE `thamso`
  ADD PRIMARY KEY (`MATHAMSO`);

--
-- Indexes for table `ve`
--
ALTER TABLE `ve`
  ADD PRIMARY KEY (`MAVE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dondatve`
--
ALTER TABLE `dondatve`
  MODIFY `MADONDATVE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `historydondatve`
--
ALTER TABLE `historydondatve`
  MODIFY `MADONDATVE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `historykhachhang`
--
ALTER TABLE `historykhachhang`
  MODIFY `MAKHACHHANG` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `historyve`
--
ALTER TABLE `historyve`
  MODIFY `MAVE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `khachhang`
--
ALTER TABLE `khachhang`
  MODIFY `MAKHACHHANG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `loaive`
--
ALTER TABLE `loaive`
  MODIFY `MALOAIVE` int(11) NOT NULL AUTO_INCREMENT COMMENT '1: vé ng` lớn, 2: vé cho trẻ em, 3: vé cho em bé', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
  MODIFY `MANHANVIEN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1006;
--
-- AUTO_INCREMENT for table `thamso`
--
ALTER TABLE `thamso`
  MODIFY `MATHAMSO` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ve`
--
ALTER TABLE `ve`
  MODIFY `MAVE` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1576;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `chitietdatve`
--
ALTER TABLE `chitietdatve`
  ADD CONSTRAINT `FK_ctdv_ddv` FOREIGN KEY (`MADONDATVE`) REFERENCES `dondatve` (`MADONDATVE`) ON DELETE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`adminuJrSWIj`@`127.13.81.2` EVENT `updateChuyenBayVe` ON SCHEDULE EVERY 60 SECOND STARTS '2017-05-04 07:36:32' ON COMPLETION PRESERVE ENABLE DO BEGIN
UPDATE skyticket.chuyenbay s
SET s.DABAY = 1
WHERE NOW() > s.THOIGIANBAY;

UPDATE skyticket.ve v
SET v.TRANGTHAI = 2
WHERE v.TRANGTHAI = 0 AND NOW() > (SELECT chuyenbay.THOIGIANBAY FROM chuyenbay WHERE v.MACHUYENBAY = chuyenbay.MACHUYENBAY);

DELETE ddv.* FROM skyticket.dondatve ddv
WHERE ddv.NGAYMUAVE < NOW() - INTERVAL 24 HOUR AND ddv.DATHANHTOAN = 0;
END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
