$(function(){
	//Chuyến qua trang success
	$('#payment-ok').click(function(){
		window.location.href = 'success.php';
	})

	//Check vé đó còn ghế hay ko ? và ko dưới 24h
	$('.dat_ve_cb_nay').off('click').on('click', function(){
		var id = $(this).attr('data-id');
		$.ajax({
			type: 'POST',
			url: 'xl_check_ve_con_ghe.php',
			data: {id: id},
			success: function(dataBack){
				if(dataBack == 2) //ok
				{
					window.location.href = 'information.php';
				}
				else if (dataBack == 0)
				{
					alert("Đã hết ghế cho chuyến bay này, mời chọn chuyến bay khác!");
				}
				else
				{
					alert("Đã hết thời gian đặt vé cho chuyến bay này!");
				}
			}
		})
	}); //end .dat_ve_cb_nay

	
})