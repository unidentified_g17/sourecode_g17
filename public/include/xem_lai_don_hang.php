<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Xem lại đơn hàng</h3>
  </div>
  <div class="panel-body">
   <form action="xem_don_hang.php" method="get">
    <div class="input-group">
      <span class="input-group-addon">
        <i class="material-icons">bubble_chart</i>
      </span>
      <input type="text" class="form-control" placeholder="Mã đơn hàng" name="madonhang" required="true">
    </div>

    <div class="input-group">
      <span class="input-group-addon">
        <i class="material-icons">wifi_tethering</i>
      </span>
      <input type="email" class="form-control" placeholder="Email" name="email" required="true">
    </div>

    <input type="submit" class="btn btn-danger" value="Xem lại đơn hàng">
  </form>
</div>
</div>