<div class="wrapper">
  <div class="header header-filter" style="background-image: url('public/layout/assets/img/bg2.jpeg');">
    <div class="container"> 
      <form method="post">
        <!--TAB ĐẶT VÉ-->
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="main dat-ve">
              <!--Title đặt vé-->
              <div class="row">
                <div class="title-dat-ve"> 
                  <i class="material-icons">keyboard_arrow_right</i>
                  <span class="title-dvmb">ĐẶT VÉ MÁY BAY NGAY KẺO LỠ</span>
                  <i class="material-icons">keyboard_arrow_left</i>
                </div>
                <p class="center-block text-center">Vé máy bay, đặt mua vé máy bay tại đại lý vé máy bay SkyTicket cam kết giá rẻ nhất</</p>
                <hr class="bottom-line"/>
              </div>

              <!--Loại vé-->
              <div class="row row-dat-ve">
                <label><strong>Loại vé:</strong></label>
                <div class="radio radio-loai-ve">
                  <label><input type="radio" name="loai_cb" value="mot-chieu" checked="true">Một chiều</label>
                </div>
                <div class="radio radio-loai-ve">
                  <label><input type="radio" name="loai_cb" value="khu-hoi" disabled="disabled">Khứ hồi</label><i class="material-icons icon-khu-hoi" rel="tooltip" data-width="500px" data-placement="bottom" data-html="true" data-original-title="<b>Vé máy bay khứ hồi</b> là loại vé thuờng được các khách du lịch hay người công tác ngắn ngày sử dụng. <hr/><b>Chức năng này đang hoàn thiện...</b>">info_outline</i>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12"> 
                  <!--ĐI-->
                  <div class="col-md-6"> 
                    <!--Điểm đi-->
                    <div class="flex-center-item">
                      <label>Điểm đi:</label>
                      <div class="collapse navbar-collapse js-navbar-collapse inline-block">
                        <ul class="nav navbar-nav">

                          <li class="dropdown dropdown-large"> <a href="#" class="dropdown-toggle primary-color" data-toggle="dropdown"><i class="material-icons">flight_takeoff</i> <span id="text_san_bay_di"><strong>Hồ Chí Minh</strong> (SGN)</span><b class="caret"></b></a>
                            <input type="hidden" id="san_bay_di" name="san_bay_di" value="Hồ Chí Minh (SGN)"/>

                            <ul class="dropdown-menu dropdown-menu-large dd-diadiem-margin">
                              <li class="col-md-4">
                                <ul>
                                  <li class="dropdown-header"><span class="label label-primary">MIỀN BẮC</span></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Hà Nội</strong> (HAN)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Hải Phòng</strong> (HPH)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Thanh Hóa</strong> (THD)</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-header"><span class="label label-primary">MIỀN TRUNG</span></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Vinh</strong> (VII)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Huế</strong> (HUI)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Đà Nẵng</strong> (DAD)</a></li>
                                </ul>
                              </li>
                              <li class="col-md-4">
                                <ul>
                                  <li class="dropdown-header"><span class="label label-primary">MIỀN NAM</span></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Hồ Chí Minh</strong> (SGN)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Nha Trang</strong> (CXR)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Đà Lạt</strong> (DLI)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Buôn Mê Thuột</strong> (BMV)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Cần Thơ</strong> (VCA)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Phú Quốc</strong> (PQC)</a></li>                  
                                </ul>
                              </li>
                              <li class="col-md-4">
                                <ul>
                                  <li class="dropdown-header"><span class="label label-primary">TÌM KIẾM</span> <span style="font-size:12px; color:#bbb">Nhập tên <br/>thành phố hoặc mã sân bay</span></li>
                                  <li>
                                    <div class="input-group"> <span class="input-group-addon"> <i class="material-icons">location_searching</i> </span>
                                      <input type="text" class="form-control" placeholder="...">
                                    </div>
                                  </li>
                                  <li class="chon_sb_di"><a href="#"><strong>Cần Thơ</strong> (VCA)</a></li>
                                  <li class="chon_sb_di"><a href="#"><strong>Phú Quốc</strong> (PQC)</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>

                    <!--Ngày đi-->
                    <label>Ngày đi:</label>
                    <div class="ui calendar" id="rangestart">
                      <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="Ngày đi..." name="ngay_di" required="required" />
                      </div>
                    </div>
                  </div>
                  <!--end ĐI--> 

                  <!--ĐẾN-->
                  <!--Điểm đến-->
                  <div class="flex-center-item">
                    <label>Điểm đến:</label>
                    <div class="collapse navbar-collapse js-navbar-collapse inline-block">
                      <ul class="nav navbar-nav">

                        <li class="dropdown dropdown-large"> <a href="#" class="dropdown-toggle primary-color" data-toggle="dropdown"><i class="material-icons">flight_land</i> <span id="text_san_bay_den"><strong>Đà Lạt</strong> (DLI)</span><b class="caret"></b></a>
                          <input type="hidden" id="san_bay_den" name="san_bay_den" value="Đà Lạt (DLI)"/>

                          <ul class="dropdown-menu dropdown-menu-large dd-diadiem-margin">
                            <li class="col-md-4">
                              <ul>
                                <li class="dropdown-header"><span class="label label-primary">MIỀN BẮC</span></li>
                                <li class="chon_sb_den"><a href="#"><strong>Hà Nội</strong> (HAN)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Hải Phòng</strong> (HPH)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Thanh Hóa</strong> (THD)</a></li>
                                <li class="denvider"></li>
                                <li class="dropdown-header"><span class="label label-primary">MIỀN TRUNG</span></li>
                                <li class="chon_sb_den"><a href="#"><strong>Vinh</strong> (VII)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Huế</strong> (HUI)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Đà Nẵng</strong> (DAD)</a></li>
                              </ul>
                            </li>
                            <li class="col-md-4">
                              <ul>
                                <li class="dropdown-header"><span class="label label-primary">MIỀN NAM</span></li>
                                <li class="chon_sb_den"><a href="#"><strong>Hồ Chí Minh</strong> (SGN)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Nha Trang</strong> (CXR)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Đà Lạt</strong> (DLI)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Buôn Mê Thuột</strong> (BMV)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Cần Thơ</strong> (VCA)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Phú Quốc</strong> (PQC)</a></li>                  
                              </ul>
                            </li>
                            <li class="col-md-4">
                              <ul>
                                <li class="dropdown-header"><span class="label label-primary">TÌM KIẾM</span> <span style="font-size:12px; color:#bbb">Nhập tên <br/>thành phố hoặc mã sân bay</span></li>
                                <li>
                                  <div class="input-group"> <span class="input-group-addon"> <i class="material-icons">location_searching</i> </span>
                                    <input type="text" class="form-control" placeholder="...">
                                  </div>
                                </li>
                                <li class="chon_sb_den"><a href="#"><strong>Cần Thơ</strong> (VCA)</a></li>
                                <li class="chon_sb_den"><a href="#"><strong>Phú Quốc</strong> (PQC)</a></li>
                              </ul>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <!--Ngày đến-->
                  <label style="color:#ccc">Ngày về:</label>
                  <div class="ui calendar" id="rangeend">
                    <div class="ui input left icon">
                      <i class="calendar icon"></i>
                      <input type="text" placeholder="Ngày về..." disabled="disabled">
                    </div>
                  </div>
                </div>
              </div>
              <!--end ĐẾN--> 

              <!-- Số lượng-->
              <div class="padding10">

              </div>
              <div class="row">
                <div class="col-md-4 flex-soluong">
                  <label><strong>Người lớn</strong><br/>(trên 12 tuổi)</label>
                  <div class="sl-setting">
                    <input class="form-control" type="number" name="sl_nl" min="1" value="1" max="10" />
                  </div>
                </div>

                <div class="col-md-4 flex-soluong">
                  <label><strong>Trẻ em</strong><br/>(2 - 11 tuổi)</label>
                  <div class="sl-setting">
                    <input class="form-control" type="number" name="sl_te" min="0" value="0" max="10" />
                  </div>
                </div>

                <div class="col-md-4 flex-soluong">
                  <label><strong>Em bé</strong><br/>(dưới 23 tháng)</label>
                  <div class="sl-setting">
                    <input class="form-control" type="number" name="sl_eb" min="0" value="0" max="10" />
                  </div>
                </div>

              </div>

              <!-- Nút tìm chuyến bay-->
              <div class="row">
                <div class="col-md-4 col-md-offset-4">
                  <input type="submit" name="tim_chuyen_bay" value="TÌM CHUYẾN BAY" class="btn btn-primary btn-round btn-timchuyenbay"/>
                  <div class="center-block text-center">
                    <i class="material-icons blue200">arrow_drop_down_circle</i></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--END TAB ĐẶT VÉ--> 
        </form>
      </div>
    </div>
  </div>