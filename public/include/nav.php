<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <a href=".">
      <div class="logo-container">
        <img src="images/logo/skyticket.png" width="200px" alt="SkyTicket" rel="tooltip" title="<b>SkyTicket</b> là hệ thống bán vé máy bay trực tuyến <b>nhanh nhất</b> phường 13 quận 10" data-placement="bottom" data-html="true">
      </div>
    </a> 
  </div>
  <div class="collapse navbar-collapse" id="navigation-index">
    <ul class="nav navbar-nav navbar-right">
      <?php if(isset($_SESSION["ten_kh"])) 
      {
        echo
        '<li><a href="dang-nhap.php" type="button" class="btn btn-sm btn-success disabled">' . 'Xin chào, <strong>' .$_SESSION["ten_kh"] . '</strong></a></li>
        <li><a href="logout.php" class="btn btn-sm btn-warning">Đăng xuất</a></li>';
      }
      else
      {
        echo
        '<li><a href="dang-nhap.php" type="button" class="btn btn-sm btn-success">Đăng nhập</a></li>
        <li><a href="dang-ky.php" class="btn btn-sm btn-primary">Đăng ký</a></li>';
      }
      ?>
    </ul>
  </div>
</div>