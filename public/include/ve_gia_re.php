<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Vé máy bay với giá rẻ nhất</h3>
  </div>
  <div class="panel-body">
    <div class="container-fluid">
      <div class="row">

        <!--đi for-->
        <?php for($i=0; $i<7; $i++){ ?>
        <div class="col-md-4 item-gia-re">
          <div class="image-city" style="background-image:url('images/location/da-nang.jpg')">
            <a class="mask" href="#">
              <div class="info-gia-re">
                <div class="info-gr-location">Hồ Chí Minh <i class="material-icons">trending_flat</i> Đà Nẵng</div>
                <div class="info-gr-date">Ngày 10/4/2017</div>
                <div class="info-gr-price">190.000đ</div>
              </div>
            </a>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
  </div>
</div>