<!doctype html>
<html lang="en">
<!--head-->
<?php $title="SkyTicket - Đặt vé máy bay trực tuyến nhanh nhất" ?>
<?php include("head.php") ?>

<!--trang chủ-->
<body class="index-page">
	<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
		<!--navbar-->
		<?php include("nav.php") ?>
	</nav>
	<!-- End Navbar -->

	<!--TABLE ĐẶT VÉ-->
	<?php include ("dat_ve_home.php"); ?>
	<!--MAIN CONTENT-->
	<div class="main main-raised">
		<div class="section section-basic">
			<div class="container">
				<!--Các bước-->
				<div class="padding10"></div>
				<?php include("fixed_cac_buoc.html") ?>

				<!--Nội dung giá vé rẻ, tìm hóa đơn (2 cột)-->
				<div class="row">
					<div class="col-md-8">
						<!--Vé giá rẻ-->
						<?php include("ve_gia_re.php") ?>

						<!--Facebook comments-->
						<?php include("fb_comments.php") ?>

					</div><!--end cột trái-->

					<div class="col-md-4">
						<!--Xem lại Đơn hàng-->
						<?php include("xem_lai_don_hang.php") ?>

						<!--Hỗ trợ-->
						<?php include("fixed_ho_tro.html") ?>
					</div><!--end cột phải-->
				</div><!--end row-->
			</div><!--end containter-->
		</div><!--end section-->

		<!--Footer-->
		<?php include("footer.php") ?>
	</div><!--end main-->
</body>

<!--   Core JS Files   -->
<?php include("script.php") ?>

<!--calendar-->
<link rel="stylesheet" type="text/css" href="public/layout/assets/css/calendar.css">
<script src="public/layout/assets/js/semantic.js"></script>
<script src="public/layout/assets/js/calendar.js"></script>
<script>
	$(function()
	{
		//xử lý ngày tháng
		var today = new Date();
		$('#rangestart').calendar({
			type: 'date',
			format: 'dd/mm/yyyy',
			minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
		});
		/*
		$('#rangeend').calendar({
			type: 'date',
			startCalendar: $('#rangestart')
		});
		*/

		//xử lý chọn sân bay
		//sân bay đi
		$('li.chon_sb_di').click(function(){
			//set format
			var sb1 = $(this).html();
			$('span#text_san_bay_di').html(sb1);

			//get dữ liệu
			var sb_di = $(this).text();
			$('#san_bay_di').val(sb_di);
		});
		//sân bay đến
		$('li.chon_sb_den').click(function(){
			//set format
			var sb2 = $(this).html();
			$('span#text_san_bay_den').html(sb2);

			//get dữ liệu
			var sb_den = $(this).text();
			$('#san_bay_den').val(sb_den);
		});
	})
</script>
</html>
