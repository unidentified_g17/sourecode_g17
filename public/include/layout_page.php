<!doctype html>
<html lang="en">
    <!--head-->
    <?php include("head.php") ?>

    <!--Trang chủ-->
    <body>
        <div class="header header-filter header-height-2nd" style="background-image: url('public/layout/assets/img/bg2.jpeg');">
            <nav class="navbar navbar-transparent navbar-static-top">
                <!--navbar-->
                <?php include("nav.php") ?>
            </nav>
            <!-- End Navbar -->
        </div><!--end bg-img-->

        <div class="main main-raised">
            <?php include("process.php") ?>
            <div class="well well-lg main-top-margin">
                <div class="row">
                    <div class="col-md-3 process-content">
                        <i class="material-icons md-60 <?php echo $a1 ?>">location_on</i>
                        <h3 class="process <?php echo $a2 ?>">Lựa chọn chuyến bay</h3>
                        <i class="material-icons md-60 grey">keyboard_arrow_right</i>
                    </div>

                    <div class="col-md-3 process-content">
                        <i class="material-icons md-60 <?php echo $b1 ?>">fingerprint</i>
                        <h3 class="process <?php echo $b2 ?>">Thông tin khách hàng</h3>
                        <i class="material-icons md-60 grey">keyboard_arrow_right</i>
                    </div>

                    <div class="col-md-3 process-content">
                        <i class="material-icons md-60 <?php echo $c1 ?>">payment</i>
                        <h3 class="process <?php echo $c2 ?>">Chọn cách thanh toán</h3>
                        <i class="material-icons md-60 grey">keyboard_arrow_right</i>
                    </div>

                    <div class="col-md-3 process-content">
                        <i class="material-icons md-60 <?php echo $d1 ?>">verified_user</i>
                        <h3 class="process <?php echo $d2 ?>">Xác nhận thành công</h3>
                        <i class="material-icons md-60 <?php echo $d1 ?>">done_all</i>
                    </div>
                </div>  
            </div> <!--end process-->

            <!--Gọi content (@RenderBody())-->
            <?php include("content.php") ?>

            <!--Footer-->
            <?php include("footer.php") ?>
        </div><!--end main-->
    </body>

    <!--   Core JS Files   -->
    <?php include("script.php") ?>
</html>
