<?php 
@session_start();
include("app/models/m_login.php");
class C_login
{
	public function Dang_nhap()
	{
		//Model
		if(isset($_POST["btn_dang_nhap"]))
		{
			$email = $_POST["email"];
			$mat_khau = md5($_POST["mat_khau"]);
			$m_login = new M_login();
			$kq = $m_login->Doc_tai_khoan_theo_email($email, $mat_khau);
			if($kq)
			{
				$_SESSION["ma_kh"] = $kq->MAKHACHHANG;
				$_SESSION["ten_kh"] = $kq->TENKHACHHANG;
				header('location: index.php');
			}
			else
			{
				$err = "Bạn đã nhập sai email hoặc mật khẩu! Nếu chưa có tài khoản, hãy đăng ký mới.";
			}
		}
		//View
		$view = "app/views/login/v_login.php";
		$title = "Đăng nhập";
		include("public/include/layout_page.php");
	}

	public function Dang_ky()
	{
		//Model
		$m_login = new M_login();
		$lastID = $m_login->getRowCount();
		if(isset($_POST["btn_dang_ky"]))
		{
			$ho_ten = $_POST["ho_ten"];
			$email = $_POST["email"];
			$mat_khau = md5($_POST["mat_khau"]);
			$dia_chi = $_POST["dia_chi"];
			$dien_thoai = $_POST["dien_thoai"];
			$m_login->Tao_khach_hang($ho_ten,$dia_chi,$dien_thoai,$email,$mat_khau);
			$newID = $m_login->getRowCount();
			if($lastID != $newID)
			{
				$ok = "Đăng kí thành công, đang chuyển sang trang đăng nhập...";
				header('Refresh:3, url=dang-nhap.php');
			}
			else
			{
				echo '<script>alert("Email đã tồn tại!")</script>';
			}
		}
		
		//View
		$view = "app/views/login/v_register.php";
		$title = "Đăng ký";
		include("public/include/layout_page.php");
	}
}


 ?>