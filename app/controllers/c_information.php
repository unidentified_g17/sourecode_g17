<?php 
@session_start();
include_once("app/models/m_thong_tin_ve.php");
class C_information
{
	public function Hien_thi_information()
	{
		//Model
		$m_thong_tin_ve = new M_thong_tin_ve();
		$ss_ve = $_SESSION['ve'];
		$ma_cbay = $ss_ve["ma_cbay"];
		$tong_sl_ve_dat = $ss_ve["tong_sl_ve_dat"];
		$sl_nl = $ss_ve["sl_nl"];
		$sl_te = $ss_ve["sl_te"];
		$sl_eb = $ss_ve["sl_eb"];

		include_once("app/models/m_login.php");
		$m_login = new M_login();
		$kh = $m_login->Doc_thong_tin_theo_ma_kh($_SESSION["ma_kh"]);

		if(isset($_POST["xac-nhan"]))
		{
			$thanhtien = $_POST["thanhtien"];
			$gia_ve_goc = $_POST["gia_ve_goc"];
			
			$tenhk = $_POST["tenhk"];
			$email = $_POST["email"];

			//--
			//Tạo hóa đơn
			$ma_hd = $m_thong_tin_ve->Tao_hoa_don($_SESSION["ma_kh"], $tong_sl_ve_dat, date('Y-m-d H:i:s', strtotime('+7 hours')), $thanhtien);
			
			//Lấy vé chưa mua
			$m_thong_tin_ve = new M_thong_tin_ve();
			$ve_chua_muas = $m_thong_tin_ve->Lay_ve_chua_mua($ma_cbay, $tong_sl_ve_dat);
			//print_r($ve_chua_muas);

			//Sửa loại vé
			//$m_thong_tin_ve->Sua_loai_ve($value->MAVE, 3);
			$countnl = 0;
			$countte = 0;
			foreach ($ve_chua_muas as $key => $value) {
				if($sl_nl == $countnl)
				{
					if($sl_te == $countte)
					{ 
						//insert em be
						$m_thong_tin_ve->Sua_loai_ve($value->MAVE, 3);
						$gia_ve = $gia_ve_goc * 0.8;
						$m_thong_tin_ve->Them_chi_tiet_dat_ve($ma_hd, $value->MAVE, $tenhk[$key], $gia_ve);
					}
					else //insert tre em
					{
						$m_thong_tin_ve->Sua_loai_ve($value->MAVE, 2);
						$gia_ve = $gia_ve_goc * 0.9;
						$m_thong_tin_ve->Them_chi_tiet_dat_ve($ma_hd, $value->MAVE, $tenhk[$key], $gia_ve);
						$countte++;
					}
				}
				else //insert ng lon
				{
					$m_thong_tin_ve->Sua_loai_ve($value->MAVE, 1);
					$gia_ve = $gia_ve_goc;
					$m_thong_tin_ve->Them_chi_tiet_dat_ve($ma_hd, $value->MAVE, $tenhk[$key], $gia_ve);
					$countnl++;
				}
			}

			$_SESSION['don_hang'] = array("email" => $email, "ma_hd" => $ma_hd);

			//Thành công (default)
			header('location: payment.php');
		}
		//View
		$title = "Thông tin người đặt vé :: SkyTicket";
		$view = "app/views/b2_nhapthongtin/v_information.php";
		include("public/include/layout_page.php");
	}
}
?>