<?php 
@session_start();
include_once("app/models/m_tom_tat_ve.php");
class C_tom_tat_ve
{
	public function Hien_thi_tom_tat_ve()
	{
		//Model
		$ss_ve = $_SESSION['ve'];
		$san_bay_di = $ss_ve["san_bay_di"];
		$san_bay_den = $ss_ve["san_bay_den"];
		$ngay_di = $ss_ve["ngay_di"];
		$tong_sl_ve_dat = $ss_ve["tong_sl_ve_dat"];
		$sl_nl = $ss_ve["sl_nl"];
		$sl_te = $ss_ve["sl_te"];
		$sl_eb = $ss_ve["sl_eb"];
		$ma_cbay = $ss_ve["ma_cbay"];

		//-----------
		$m_tom_tat_ve = new M_tom_tat_ve();
		$dia_diem_di = $m_tom_tat_ve->Tim_dia_diem($san_bay_di);
		$dia_diem_den = $m_tom_tat_ve->Tim_dia_diem($san_bay_den);
		$san_bay_di = $m_tom_tat_ve->Tim_san_bay($san_bay_di);
		$san_bay_den = $m_tom_tat_ve->Tim_san_bay($san_bay_den);
		$thoi_gian = $m_tom_tat_ve->Tim_thoi_gian_bay($ma_cbay);

		$gia_ve_goc = $m_tom_tat_ve->Tim_thoi_gian_bay($ma_cbay)->giavegoc;
		//CT tính giá vé tổng cộng: NL * 1 + TE * 0.9 + EB * 0.8
		$gia_ve_total =  ($gia_ve_goc)*($sl_nl + $sl_te * 0.9 + $sl_eb * 0.8);
		//Views
		include("app/views/tom_tat_ve/v_tom_tat_ve.php");
	}

}
?>
