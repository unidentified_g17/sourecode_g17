<?php 
include_once("configs/database.php");
class M_login extends database
{
	public function Doc_tai_khoan_theo_email($email, $pass)
	{
		$sql = "select * from khachhang where EMAIL = ? && PASSWORD = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($email, $pass));
	}

	//MAKHACHHANG, TENKHACHHANG, DIACHI, PHONE, EMAIL, PASSWORD, DELETEFLAG, DELETETIME
	public function Tao_khach_hang($TENKHACHHANG, $DIACHI, $PHONE, $EMAIL, $PASSWORD)
	{
		$sql = "insert into khachhang values(?,?,?,?,?,?,b?,?)";
		$this->setQuery($sql);
		$this->execute(array(NULL, $TENKHACHHANG, $DIACHI, $PHONE, $EMAIL, $PASSWORD, 0, NULL));
		return $this->getLastId();
	}

	//kiểm tra thêm thành công hay ko
	public function getRowCount() 
	{
		$sql = "select count(*) from khachhang";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Doc_thong_tin_theo_ma_kh($ma_kh)
	{
		$sql = "select * from khachhang where MAKHACHHANG = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_kh));
	}
}
 ?>

