<?php 
include_once("configs/database.php");
class M_thong_tin_ve extends database
{
	//MADONDATVE, MAKHACHHANG, SOLUONGDAT, NGAYMUAVE, THANHTIEN, DATHANHTOAN, MAKHUYENMAI, DELETEFLAG, DELETETIME
	public function Tao_hoa_don($MAKHACHHANG, $SOLUONGDAT, $NGAYMUAVE, $THANHTIEN)
	{
		$sql = "insert into dondatve values(?, ?, ?, ?, ?, b?, ?, b?, ?)";
		$this->setQuery($sql);
		$this->execute(array(NULL, $MAKHACHHANG, $SOLUONGDAT, $NGAYMUAVE, $THANHTIEN, 0, NULL, 0, NULL));
		return $this->getLastId();
	}

	public function Lay_ve_chua_mua($ma_chuyen_bay, $sl_ve)
	{
		$sql = "CALL layvechuamua(?, ?)";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_chuyen_bay, $sl_ve));
	}

	//loại vé: NL:1, TE:2, EB:3
	public function Sua_loai_ve($ma_ve, $loai_ve)
	{
		$sql = "CALL sualoaive(?, ?)";
		$this->setQuery($sql);
		return $this->execute(array($ma_ve, $loai_ve));
	}

	//MADONDATVE, MAVE, HOTENHANHKHACH, GIAVE, DELETEFLAG, DELETETIME
	public function Them_chi_tiet_dat_ve($MADONDATVE, $MAVE, $HOTENHANHKHACH, $GIAVE)
	{
		//insert into chitietdatve(MADONDATVE, MAVE, GIAVE, HOTENHANHKHACH, DELETEFLAG, DELETETIME) VALUES(mahoadon, mave, @giave - (@giave * phantram / 100), hotenhanhkhach, 0, null);
		$sql = "insert into chitietdatve values(?,?,?,?,b?,?)";
		$this->setQuery($sql);
		return $this->execute(array($MADONDATVE, $MAVE, $HOTENHANHKHACH, $GIAVE, 0, NULL));
	}
}