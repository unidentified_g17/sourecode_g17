<?php 
include_once("configs/database.php");
class M_tom_tat_ve extends database
{
	public function Tim_dia_diem($ma_san_bay)
	{
		$sql = "select tendiadiem from diadiem, sanbay where sanbay.MASANBAY = ? and sanbay.MADIADIEM = diadiem.MADIADIEM";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_san_bay));
	}

	public function Tim_san_bay($ma_san_bay)
	{
		$sql = "select tensanbay from sanbay where sanbay.MASANBAY = ?";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_san_bay));
	}

	//Tìm luôn giá vé để tính
	public function Tim_thoi_gian_bay($ma_chuyen_bay)
	{
		$sql = "select thoigianbay, thoigianden, giavegoc from chuyenbay where machuyenbay = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_chuyen_bay));
	}
}

 ?>