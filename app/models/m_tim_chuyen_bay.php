<?php 
include_once("configs/database.php");
class M_tim_chuyen_bay extends database
{
	public function Hien_thi_cb($san_bay_di, $san_bay_den, $ngay_di)
	{
		//$sql = "SET @p0=?; SET @p1=?; SET @p2=?; CALL `searchChuyenBay`(@p0, @p1, @p2);";
		$sql = "CALL searchChuyenBay(?, ?, ?)";
		$this->setQuery($sql);
		return $this->loadAllRows(array($san_bay_di, $san_bay_den, $ngay_di));
	}

	//select count(*) from ve where MACHUYENBAY = 'A0001' and TRANGTHAI = 0
	public function Kiem_tra_ghe($ma_chuyen_bay)
	{
		$sql = "select count(*) from ve where MACHUYENBAY = ? and TRANGTHAI = 0";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_chuyen_bay));
	}

	public function Kiem_tra_24h($ma_chuyen_bay)
	{
		$sql = "call checkChuyenBayDeDatVe(?)";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_chuyen_bay));
	}
}

 ?>