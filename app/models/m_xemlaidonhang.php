<?php 
include_once 'configs/database.php';
class M_xemlaidonhang extends database
{
	public function Data_cbay($ma_don_dat_ve)
	{
		$sql = "select kh.TENKHACHHANG, kh.EMAIL, kh.PHONE, cb.MACHUYENBAY, cb.SANBAYDI, cb.THOIGIANBAY, cb.SANBAYDEN, cb.THOIGIANDEN, ddv.DATHANHTOAN from dondatve ddv, chitietdatve ct, chuyenbay cb, khachhang kh, ve where ddv.MADONDATVE = ct.MADONDATVE and ct.MAVE = ve.MAVE and ve.MACHUYENBAY = cb.MACHUYENBAY and kh.MAKHACHHANG = ddv.MAKHACHHANG and ct.MADONDATVE = ? limit 0,1";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_don_dat_ve));
	}

	public function Data_chitietve($ma_don_dat_ve)
	{
		$sql = "select * from chitietdatve as ct, ve where ct.MAVE = ve.MAVE and MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_don_dat_ve));
	}

	public function Kiemtra($email, $ma_don_dat_ve)
	{
		$sql = "select kh.MAKHACHHANG from khachhang kh, dondatve ddv where ddv.MAKHACHHANG = kh.MAKHACHHANG and kh.EMAIL = ? and ddv.MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($email, $ma_don_dat_ve));
	}
}
?>