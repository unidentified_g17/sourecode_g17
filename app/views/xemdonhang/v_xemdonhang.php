<?php 
if(count($kt) == 0)
	{ ?>
<h3 class="text-center">Mã hóa đơn hoặc email không có dữ liệu, vui lòng kiểm tra lại. Xin cảm ơn!</h3>
<?php } else { ?>
<div class="container">
<?php if($tt->DATHANHTOAN == 0) 
{ ?>
<div class="alert alert-danger">
	<strong>Tình trạng thanh toán: </strong>Chưa thanh toán
</div>
<?php }
else {?>
<div class="alert alert-success">
	<strong>Tình trạng thanh toán: </strong>Đã thanh toán
</div>
<?php } ?>

	<div class="row">

		<div class="col-md-6">
			<strong>Tên khách hàng: </strong><?php echo $tt->TENKHACHHANG ?>
			<hr/>
			<strong>Sân bay đi: </strong><?php echo $tt->SANBAYDI ?><br/>
			<strong>Thời gian đi: </strong><?php echo $tt->THOIGIANBAY ?>
		</div>

		<div class="col-md-6">
			<strong>Số đt: </strong><?php echo $tt->PHONE ?>
			<hr/>
			<strong>Sân bay đến: </strong><?php echo $tt->SANBAYDEN ?><br/>
			<strong>Thời gian đến: </strong><?php echo $tt->THOIGIANDEN ?>
		</div>
	</div>
	<br/>
	<table id="table-donhang" class="table table-bordered" style="padding:20px">
		<thead>
			<tr>
				<th>Họ tên hành khách</th>
				<th>Loại vé</th>
				<th>Mã vé</th>
				<th>Giá vé</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($cthds as $dh)
			{ ?>
			<tr>
				<td><?php echo $dh->HOTENHANHKHACH ?></td>
				<td>
					<?php
					if($dh->LOAIVE == 1)
						echo 'Người lớn';
					elseif($dh->LOAIVE == 2)
						echo 'Trẻ em';
					else echo 'Em bé';
					?>
				</td>
				<td><?php echo $dh->MAVE ?></td>
				<td><?php echo number_format($dh->GIAVE) ?> VNĐ</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php } ?>