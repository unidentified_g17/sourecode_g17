<div class="main-2nd">
  <div class="row">
    <!--trái-->
    <div class="col-md-9">
      <div class="panel panel-blue800">
        <div class="panel-heading">
          <h2 class="panel-flex"><i class="material-icons">account_circle</i> CHỌN PHƯƠNG THỨC THANH TOÁN</h2>
          <p>Lựa chọn phương thức thanh toán phù hợp nhất với bạn</p>
        </div>

        <div class="panel-body">
          <div class="well">
            <div class="row">
              <div class="col-md-1">
                <div class="radio">
                  <label>
                    <input type="radio" name="optionsRadios" checked="checked">
                  </label>
                </div>
              </div>
              <div class="col-md-1 icon-payment">
                <i class="material-icons md-tt">tab</i>
              </div>
              <div class="col-md-10">
                <h2>THANH TOÁN TRỰC TIẾP</h2>
                <p>Chúng tôi sẽ liên hệ với bạn để xác nhận đơn hàng. Sau khi thanh toán chúng tôi sẽ gửi vé qua email.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-success">
        <div class="panel-heading">
          <h2 class="panel-flex">
            <i class="material-icons">favorite</i> &nbsp; NHẬP MÃ KHUYẾN MÃI (nếu có)
            <p style="font-size:12px"> -- [TÍNH NĂNG ĐANG HOÀN THIỆN...]</p>
          </h2>
        </div>

        <div class="panel-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="row">
                <div class="col-md-2 icon-tt">
                  <i class="material-icons md-tt">loyalty</i>
                </div>
                <div class="col-md-8">
                  <div class="form-group label-floating">
                    <label class="control-label">Mã khuyến mãi</label>
                    <input type="email" class="form-control" disabled="true" />
                  </div>
                </div>
                <div class="col-md-2 icon-tt-km">
                  <button class="btn btn-primary btn-fab btn-fab-mini btn-round md-tt" disabled="true">
                    <i class="material-icons ">label_outline</i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <button id="payment-ok" class="btn btn-warning btn-round btn-timchuyenbay"> <i class="material-icons">check_circle</i> XÁC NHẬN </button>

      </div>
    </div><!--end trái-->

    <!--phải-->
    <!--tóm tắt đặt vé-->
    <?php
    include ("app/controllers/c_tom_tat_ve.php"); 
    $c_tom_tat_ve = new C_tom_tat_ve();
    $c_tom_tat_ve->Hien_thi_tom_tat_ve() ?>

  </div><!--end main-2nd-->
</div>