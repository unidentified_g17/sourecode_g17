<div class="panel panel-blue800">
  <div class="panel-heading">
    <div class="col-md-6">
      <h2 class="panel-flex">
        <i class="material-icons">skip_next</i>
        <span>Chiều đi</span>
        &nbsp;
        <span class="badge badge-cd"><?php echo count($cbs) ?> kết quả</span>
      </h2>
    </div>
    <div class="col-md-6">
     <p class="text-right">
       <?php echo $san_bay_di ?> &rarr; <?php echo $san_bay_den ?><br/>
       Ngày <?php echo date('d/m/Y', strtotime($ngay_di)) ?>
     </p>
   </div>
   <div class="clearfix"></div>
 </div><!--panel heading-->

 <div class="panel-body">
  <?php foreach($cbs as $cb) { ?>
  <!--1 chuyến bay loop-->
  <div class="well well-chuyen-bay">
    <!--thông tin chuyến bay-->
    <div class="row chi-tiet-chuyen-bay">
      <div class="col-md-1 chi-tiet-ma-chuyen">
        <i class="material-icons blue800">room</i>  
        <span><strong><?php echo $cb->ma_chuyen_bay ?></strong></span>
      </div>

      <div class="col-md-2 chi-tiet-diem-di">
        <p><small><?php echo date("d/m/Y", strtotime($cb->thoi_gian_bay)) ?></small> <span class="text-danger" style="font-size:22px; font-weight: bold"><?php echo date("H:i", strtotime($cb->thoi_gian_bay)) ?></span></p>
        <p class="chi-tiet-san-bay"><strong><?php echo $cb->san_bay_di ?></strong></p>
      </div>

      <div class="col-md-2 chi-tiet-tram-dung">
        <p><?php echo $cb->duration ?></p>
        <?php if($cb->transition != null){
          echo '<hr class="tram-dung"><dung></dung></hr><p>' . $cb->transition .'</p>';
        } 
        else {
          echo '<hr class="tram-dung"/>
          <p>Bay thẳng</p>';
        }
        ?>
      </div>

      <div class="col-md-2 chi-tiet-diem-den">
        <p><span class="text-danger" style="font-size:22px; font-weight: bold"><?php echo date("H:i", strtotime($cb->thoi_gian_den)) ?></span> <small><?php echo date("d/m/Y", strtotime($cb->thoi_gian_den)) ?></small></p>
        <p class="chi-tiet-san-bay"><strong><?php echo $cb->san_bay_den ?></strong></p>
      </div>

      <div class="col-md-2 text-center">
        <p class="chi-tiet-gia"><?php echo number_format($cb->gia_ve_goc) ?> VNĐ</p>
      </div>

      <div class="col-md-2 center-block">
          <button class="btn btn-warning dat_ve_cb_nay" data-id="<?php echo $cb->ma_chuyen_bay ?>">Đặt vé chuyến bay này</button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
</div>