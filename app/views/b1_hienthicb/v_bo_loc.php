<div class="panel panel-info">
        <div class="panel-heading">
          <h2 class="panel-title">Bộ Lọc</h2>
        </div>
        <div class="panel-body">
          <!--điểm dừng-->
          <div class="bo-loc-title"><i class="material-icons">alarm_on</i> Điểm dừng</div>
          <div class="checkbox">
            <label><input type="checkbox" name="optionsCheckboxes" checked>Bay thẳng</label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" name="optionsCheckboxes" checked>1 điểm dừng</label>
          </div>
          <div class="padding10"></div>
          <!--hãng bay-->
          <div class="bo-loc-title"><i class="material-icons">add_shopping_cart</i> Hãng bay</div>
          <div class="checkbox">
            <label><input type="checkbox" name="optionsCheckboxes" checked>Vietjet Air</label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" name="optionsCheckboxes" checked>Vietnam Airlines</label>
          </div>
        </div>
      </div>