<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form role="form" method="post">
			<fieldset>
				<h2 class="text-center text-success">Đăng nhập</h2>
				<p class="text-center">Đăng nhập để chúng tôi có thể xác thực thông tin của bạn</p>
				<hr/>
				<div class="form-group">
					<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" value="<?php echo (isset($email))?$email:"" ?>" required="required">
				</div>
				<div class="form-group">
					<input type="password" name="mat_khau" id="mat_khau" class="form-control input-lg" placeholder="Mật khẩu" required="required">
				</div>
				<?php 
				if(isset($err))
				{
					echo '<div class="alert alert-danger" role="alert"><strong>Oh nooooo! </strong>' . $err . '</div>';
				}
				?>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-8 col-sm-8 col-md-8">
						<input type="submit" class="btn btn-lg btn-success btn-block" name="btn_dang_nhap" value="Đăng nhập">
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4">
						<a href="dang-ky.php" class="btn btn-lg btn-primary btn-block">Đăng ký</a>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
