<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form role="form" method="post">
			<fieldset>
				<h2 class="text-center text-info">Đăng ký</h2>
				<p class="text-center">Nhập đúng thông tin vì sẽ không được sửa lại hoặc tạo tài khoản mới nếu cần sửa. Email không được đã tồn tại trong hệ thống!</p>
				<hr/>
				<?php 
				if(isset($ok))
				{
					echo '<div class="alert alert-success" role="alert">' . $ok . '</div>';
				}
				?>
				<div class="form-group">
					<input type="text" name="ho_ten" id="ho_ten" class="form-control input-lg kiemtra" placeholder="Họ và tên" tabindex="1" data_error="Nhập họ và tên" value="<?php echo (isset($ho_ten)?$ho_ten:"") ?>" required="required">
				</div>
				<div class="form-group">
					<p class="help-block" id="ajax-tk-trung"></p>
					<input type="email" name="email" id="email" class="form-control input-lg kiemtra" placeholder="Email" tabindex="2" data_error="Nhập email" value="<?php echo (isset($email)?$email:"") ?>" required="required">
				</div>
				<div class="form-group">
					<input type="password" name="mat_khau" id="mat_khau" class="form-control input-lg kiemtra" placeholder="Mật khẩu" tabindex="3" data_error="Nhập mật khẩu" required="required">
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="dia_chi" id="dia_chi" class="form-control input-lg kiemtra" placeholder="Địa chỉ" tabindex="4" data_error="Nhập địa chỉ" value="<?php echo (isset($dia_chi)?$dia_chi:"") ?>" required="required">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="dien_thoai" id="dien_thoai" class="form-control input-lg kiemtra" placeholder="Điện thoại" tabindex="5" data_error="Nhập số điện thoại" value="<?php echo (isset($dien_thoai)?$dien_thoai:"") ?>" required="required">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-8">
						<input type="submit" name="btn_dang_ky" value="Đăng ký" class="btn btn-primary btn-block btn-lg" tabindex="8"></div>
						<div class="col-xs-12 col-md-4"><a href="dang-nhap.php" class="btn btn-success btn-block btn-lg">Đăng nhập</a></div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>