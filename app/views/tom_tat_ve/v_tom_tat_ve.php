<div class="col-md-3">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h2 class="panel-title">Tóm tắt đặt vé</h2>
    </div>
    <div class="panel-body">
      <!--từ-->
      <table class="table">
        <tr>
          <td>Từ</td>
          <td class="text-primary"><strong><?php echo $dia_diem_di ?><strong></td>
        </tr>

        <tr>
          <td>Sân bay</td>
          <td class="text-primary"><?php echo $san_bay_di ?></td>
        </tr>

        <tr>
          <td>Thời gian</td>
          <td class="text-primary"><?php echo $thoi_gian->thoigianbay ?></td>
        </tr>
      </table>

      <div class="padding10"></div>

      <!--đến-->
      <table class="table">
        <tr>
          <td>Đến</td>
          <td class="text-primary"><strong><?php echo $dia_diem_den ?><strong></td>
        </tr>

        <tr>
          <td>Sân bay</td>
          <td class="text-primary"><?php echo $san_bay_den ?></td>
        </tr>

        <tr>
          <td>Thời gian</td>
          <td class="text-primary"><?php echo $thoi_gian->thoigianden ?></td>
        </tr>
      </table>

      <div class="padding10"></div>

      <!--chi phí-->
      <table class="table">
        <tr>
          <td>Chuyến bay</td>
          <td class="text-primary"><strong><?php echo $ma_cbay ?><strong></td>
        </tr>

        <tr>
          <td>Số lượng</td>
          <td class="text-primary"><?php echo $tong_sl_ve_dat ?>
            (NL: <?php echo $sl_nl ?> |
            TE: <?php echo $sl_te ?> |
            EB: <?php echo $sl_eb ?>)</td>
          </tr>

          <tr>
            <td>Giá</td>
            <td class="text-primary"><?php echo number_format($gia_ve_goc) ?> VNĐ</td>
            <input type="hidden" name="gia_ve_goc" value="<?php echo $gia_ve_goc ?>" />
          </tr>

          <tr>
            <td>Khuyến mãi</td>
            <td class="text-primary">0 VNĐ</td>
          </tr>

          <tr>
            <td><strong>Tổng cộng</strong></td>
            <td class="text-warning text-large"><?php echo number_format($gia_ve_total) ?> VNĐ</td>
            <input type="hidden" name="thanhtien" value="<?php echo $gia_ve_total ?>" />
          </tr>

        </table>
      </div>
    </div><!--end phải-->
  </div>