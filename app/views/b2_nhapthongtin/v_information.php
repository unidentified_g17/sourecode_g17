<div class="main-2nd">
  <form method="post">
    <div class="row">
      <!--trái-->
      <div class="col-md-9">
        <div class="panel panel-blue800">
          <div class="panel-heading">
            <h2 class="panel-flex"><i class="material-icons">fingerprint</i> &nbsp; 1. THÔNG TIN HÀNH KHÁCH</h2>
            <p>Nhập theo thứ tự: Người lớn => Trẻ em => Em bé</p>
          </div>

          <div class="panel-body panel-tt">
            <?php for($i = 1; $i<=$tong_sl_ve_dat; $i++){ ?>
            <div class="row">
              <div class="col-md-2 icon-tt">
                <i class="material-icons md-tt">face</i>
              </div>
              <div class="col-md-10">
                <div class="form-group label-floating">
                  <label class="control-label">Họ tên hành khách <?php echo $i ?></label>
                  <!-- <input type="text" class="form-control" name="tenhk_<?php echo $i ?>" /> -->
                  <input type="text" class="form-control" name="tenhk[]" required="required"/>
                </div>
              </div>
            </div><!--họ tên-->
            <?php } ?>

          </div>
        </div>
        <!--end phân 1-->

        <div class="panel panel-blue800">
          <div class="panel-heading">
            <h2 class="panel-flex"><i class="material-icons">fingerprint</i> &nbsp; 2. THÔNG TIN NGƯỜI ĐẶT VÉ </h2>
            <p>Chúng tôi sẽ liên hệ theo những thông tin sau để giao vé</p>
          </div>

          <div class="panel-body panel-tt">
            <div class="row">
              <div class="col-md-2 icon-tt">
                <i class="material-icons md-tt">face</i>
              </div>
              <div class="col-md-10">
                <div class="form-group label-floating">
                  <label class="control-label">Họ và tên</label>
                  <input type="text" class="form-control" name="ho_ten" value="<?php echo $kh->TENKHACHHANG ?>" disabled>
                </div>
              </div>
            </div><!--họ tên-->

            <div class="row">
              <div class="col-md-2 icon-tt">
                <i class="material-icons md-tt">ring_volume</i>
              </div>
              <div class="col-md-4">

                <div class="form-group label-floating">
                  <label class="control-label">Số điện thoại</label>
                  <input type="text" class="form-control" name="so_dien_thoai" value="<?php echo $kh->PHONE ?>" disabled>
                </div>
              </div>

              <div class="col-md-2 icon-tt">
                <i class="material-icons md-tt">mail</i>
              </div>
              <div class="col-md-4">

                <div class="form-group label-floating">
                  <label class="control-label">Email</label>
                  <input type="email" class="form-control" name="email" value="<?php echo $kh->EMAIL ?>" disabled>
                  <input type="hidden" name="email" value="<?php echo $kh->EMAIL ?>">
                </div>
              </div>
            </div><!--email + sđt-->

            <div class="row">
              <div class="col-md-2 icon-tt">
                <i class="material-icons md-tt">send</i>
              </div>
              <div class="col-md-10">
                <div class="form-group label-floating">
                  <label class="control-label">Địa chỉ</label>
                  <input type="text" class="form-control" name="dia_chi" value="<?php echo $kh->DIACHI ?>" disabled>
                </div>
              </div>
            </div><!--dịa chỉ-->
            <a href="registeragain.php" class="btn btn-sm">Nếu thông tin chưa chính xác, hãy đăng kí tài khoản mới!</a>
          </div> <!-- panel-body panel-tt -->

          <input type="submit" class="btn btn-warning btn-flat btn-timchuyenbay" name="xac-nhan" value="XÁC NHẬN" />

        </div>
      </div><!--end trái-->

      <!--phải-->
      <!--tóm tắt đặt vé-->
      <?php
      include ("app/controllers/c_tom_tat_ve.php"); 
      $c_tom_tat_ve = new C_tom_tat_ve();
      $c_tom_tat_ve->Hien_thi_tom_tat_ve() ?>

    </div>
  </form>
</div><!--end main-2nd-->