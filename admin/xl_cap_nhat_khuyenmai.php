<?php 
$id = $_POST["id"];
include_once("app/models/m_khuyenmai.php");
$m_khuyenmai = new M_khuyenmai();
$khuyenmai = $m_khuyenmai->Doc_khuyenmai_theo_ma($id);
?>

<div class="box box-warning">
	<div class="box-header">
		<h3 class="box-title">Cập nhật khuyến mãi <p class="badge"></h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body table table-body">
		<form method="post">
			<div class="input-group">
				<span class="input-group-addon">@MÃ KHUYẾN MÃI</span>
				<input type="text" class="form-control" name="ma_khuyen_mai" placeholder="5 ký tự" value="<?php echo $khuyenmai->MAKHUYENMAI ?>" readonly >
			</div>
			<br/>
			<div class="input-group">
				<input type="number" class="form-control kiemtra" name="khuyen_mai" placeholder="Giá tiền giảm" value="<?php echo $khuyenmai->KHUYENMAI ?>" data_error="Nhập số tiền khuyến mãi" >
				<span class="input-group-addon">VNĐ</span>
			</div>
			<br/>
			<input type="submit" name="cap_nhat" id="cap_nhat_km" class="btn btn-warning btn-flat" value="Cập nhật" onclick="return Kiemtradulieu()"/>
			<button type="button" id="huy" class="btn btn-default btn-flat">Hủy</button>
		</form>
	</div>
	<!-- /.box-body -->
</div>

<script type="text/javascript">
	$('#huy').click(function()
	{
		$('#div_cap_nhat_khuyen_mai').hide();
		$('#them_khuyen_mai').show();
	});
</script>