<?php 
@session_start();
require_once("app/models/m_dashboard.php");
class C_dashboard
{
	public function Hien_thi_dashboard()
	{
		$m_dashboard = new M_dashboard();
		$arrayThongke = $m_dashboard->Thong_ke();
		$doanh_thus = $m_dashboard->Doc_doanh_thu();
		$cbtns = $m_dashboard->Chuyen_bay_trong_ngay();
		//View
		include("app/views/dashboard/v_dashboard.php");
	}
}
?>