<?php 
@session_start();
if(!($_SESSION["quyen"]=="AD" || $_SESSION["quyen"]=="QL"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_diadiem.php");
	class C_diadiem
	{
		public function Hien_thi_diadiem()
		{
		//Model
			$m_diadiem = new M_diadiem();
			$diadiems = $m_diadiem->Doc_diadiem();

		//View
			$title = "Danh sách địa điểm";
			$view = "app/views/thongtinbay/v_diadiem.php";
			include("public/include/layout.php");
		}

	//Thêm
		public function Hien_thi_them_diadiem()
		{
		//Model
			$m_diadiem = new M_diadiem();
			$oldRow = $m_diadiem->getRowCount();
			if(isset($_POST["them_moi"]))
			{
				$ma_dia_diem = $_POST["ma_dia_diem"];
				$ten_dia_diem = $_POST["ten_dia_diem"];
				$kq = $m_diadiem->Them_diadiem($ma_dia_diem, $ten_dia_diem);
				$newRow = $m_diadiem->getRowCount();
				if($oldRow != $newRow)
				{
				//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Thêm thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="diadiem.php"} , 2000);';
					echo '</script>';
				}
				else
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("FAIL!","Thêm thất bại! Kiểm tra lại mã địa điểm","error");}, 1000);';
					echo '</script>';
				}
			}

		//View
			$title = "Thêm địa điểm";
			$view = "app/views/thongtinbay/v_them_diadiem.php";
			include("public/include/layout.php");
		}

	//Sửa
		public function Hien_thi_sua_diadiem()
		{
			$ma_dia_diem = $_GET["ma"];
		//Model
			$m_diadiem = new M_diadiem();
			$diadiem = $m_diadiem->Doc_diadiem_theo_ma($ma_dia_diem);
			if(isset($_POST["cap_nhat"]))
			{
				$ten_dia_diem = $_POST["ten_dia_diem"];
				$kq = $m_diadiem->Sua_diadiem($ten_dia_diem, $ma_dia_diem);
				if($kq)
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Sửa thành công!","success");}, 1000);';
					echo 'setTimeout(function () { location.href="diadiem.php"} , 2000);';
					echo '</script>';
				}
			//Mã readonly nên ko cần else
			}
		//View
			$title = "Sửa địa điểm";
			$view = "app/views/thongtinbay/v_sua_diadiem.php";
			include("public/include/layout.php");
		}
	}
}
?>