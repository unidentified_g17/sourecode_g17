	<?php 
	@session_start();
	if (!($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "QL"))
	{
		header('location:no-permission.php');
	}
	else
	{
		include_once("app/models/m_chuyenbay.php");
		class C_chuyenbay
		{
			public function Hien_thi_sua_chuyenbay()
			{
				//Model
				$ma_chuyen_bay = $_GET["ma"];
				
				$m_chuyen_bay = new M_chuyenbay();

				$cb = $m_chuyen_bay->Doc_chuyenbay_theo_ma($ma_chuyen_bay);
				
				include_once("app/models/m_sanbay.php");
				$m_sanbay = new M_sanbay();
				$sanbays = $m_sanbay->Doc_sanbay();

				include_once("app/models/m_maybay.php");
				$m_maybay = new M_maybay();
				$maybays = $m_maybay->Doc_maybay();

				$m_chuyenbay = new M_chuyenbay();
				$oldRow = $m_chuyenbay->getRowCount();

				if(isset($_POST["them_moi"]))
				{
				//Bảng chuyến bay
					$ma_chuyen_bay = $_POST["ma_chuyen_bay"];
					$san_bay_di = $_POST["san_bay_di"];
					$san_bay_den = $_POST["san_bay_den"];
					$may_bay = $_POST["may_bay"];
				$ma_may_bay = explode("-", $may_bay)[0]; //276

				$thoi_gian = $_POST["thoi_gian"];
				//13-04-2017 16:00 - 13-04-2017 23:59
				//Xử lý thời gian đến và đi
				$t1 = explode("-", $thoi_gian)[0];
				$t1_rp = str_replace('/', '-', $t1); //PHP ko nhận dd/mm/yyyy => dd-mm-yyyy
				$thoi_gian_di = date("Y-m-d H:i:s",strtotime($t1_rp)); //2017-04-13 16:00:00
				$t2 = explode("-", $thoi_gian)[1];
				$t2_rp = str_replace('/', '-', $t2); //PHP ko nhận dd/mm/yyyy => dd-mm-yyyy
				$thoi_gian_den = date("Y-m-d H:i:s",strtotime($t2_rp));

				//Bảng vé chuyến bay
				$gia_ve = $_POST["gia_ve"];
				$so_luong_ve = explode("-", $may_bay)[1]; //276

				$tram_dung = "";
				if(isset($_POST["checkbox_tram_dung"]))
				{
					if($_POST["checkbox_tram_dung"] == "on")
					{
						//insert into chuyenbay_cotrans
						$tram_dung = $_POST["tram_dung"];
						$kq1 = $m_chuyenbay->Them_chuyenbay_cotrans($ma_chuyen_bay, $ma_may_bay, $san_bay_di, $san_bay_den, $gia_ve, $thoi_gian_di, $thoi_gian_den, $tram_dung);
					}
				}
				else
				{
					//insert into chuyenbay
					$kq1 = $m_chuyenbay->Them_chuyenbay($ma_chuyen_bay, $ma_may_bay, $san_bay_di, $san_bay_den, $gia_ve, $thoi_gian_di, $thoi_gian_den);
				}

				$newRow = $m_chuyenbay->getRowCount();

				//insert into vechuyenbay
				for($i = 0; $i < $so_luong_ve; $i++)
				{
					$kq2 = $m_chuyenbay->Them_ve_chuyenbay($ma_chuyen_bay);
				}

				if($oldRow != $newRow)
				{
					//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Thêm thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="chuyenbay.php"} , 2000);';
					echo '</script>';
				}
				else
				{
					//Thêm thất bại
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("FAIL!","Thêm thất bại! Kiểm tra lại mã chuyến bay","error");}, 1000);';
					echo '</script>';
				}
			}

			//View
			$title = "Sửa chuyến bay";
			$view = "app/views/chuyenbay/v_sua_chuyenbay.php";
			include("public/include/layout.php");
		}


		public function Hien_thi_chuyenbay()
		{
			//Model
			$m_chuyenbay = new M_chuyenbay();
			$chuyenbays = $m_chuyenbay->Doc_chuyenbay();

			//View
			$title = "Danh sách chuyến bay";
			$view = "app/views/chuyenbay/v_chuyenbay.php";
			include("public/include/layout.php");
		}

		public function Them_chuyenbay()
		{
				//Model
			include_once("app/models/m_sanbay.php");
			$m_sanbay = new M_sanbay();
			$sanbays = $m_sanbay->Doc_sanbay();

			include_once("app/models/m_maybay.php");
			$m_maybay = new M_maybay();
			$maybays = $m_maybay->Doc_maybay();

			$m_chuyenbay = new M_chuyenbay();
			$oldRow = $m_chuyenbay->getRowCount();

			if(isset($_POST["them_moi"]))
			{
				//Bảng chuyến bay
				$ma_chuyen_bay = $_POST["ma_chuyen_bay"];
				$san_bay_di = $_POST["san_bay_di"];
				$san_bay_den = $_POST["san_bay_den"];
				$may_bay = $_POST["may_bay"];
				$ma_may_bay = explode("-", $may_bay)[0]; //276

				$thoi_gian = $_POST["thoi_gian"];
				//13-04-2017 16:00 - 13-04-2017 23:59
				//Xử lý thời gian đến và đi
				$t1 = explode("-", $thoi_gian)[0];
				$t1_rp = str_replace('/', '-', $t1); //PHP ko nhận dd/mm/yyyy => dd-mm-yyyy
				$thoi_gian_di = date("Y-m-d H:i:s",strtotime($t1_rp)); //2017-04-13 16:00:00
				$t2 = explode("-", $thoi_gian)[1];
				$t2_rp = str_replace('/', '-', $t2); //PHP ko nhận dd/mm/yyyy => dd-mm-yyyy
				$thoi_gian_den = date("Y-m-d H:i:s",strtotime($t2_rp));

				//Bảng vé chuyến bay
				$gia_ve = $_POST["gia_ve"];
				$so_luong_ve = explode("-", $may_bay)[1]; //276

				$tram_dung = "";
				if(isset($_POST["checkbox_tram_dung"]))
				{
					if($_POST["checkbox_tram_dung"] == "on")
					{
						//insert into chuyenbay_cotrans
						$tram_dung = $_POST["tram_dung"];
						$kq1 = $m_chuyenbay->Them_chuyenbay_cotrans($ma_chuyen_bay, $ma_may_bay, $san_bay_di, $san_bay_den, $gia_ve, $thoi_gian_di, $thoi_gian_den, $tram_dung);
					}
				}
				else
				{
					//insert into chuyenbay
					$kq1 = $m_chuyenbay->Them_chuyenbay($ma_chuyen_bay, $ma_may_bay, $san_bay_di, $san_bay_den, $gia_ve, $thoi_gian_di, $thoi_gian_den);
				}

				$newRow = $m_chuyenbay->getRowCount();

				//insert into vechuyenbay
				for($i = 0; $i < $so_luong_ve; $i++)
				{
					$kq2 = $m_chuyenbay->Them_ve_chuyenbay($ma_chuyen_bay);
				}

				if($oldRow != $newRow)
				{
					//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Thêm thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="chuyenbay.php"} , 2000);';
					echo '</script>';
				}
				else
				{
					//Thêm thất bại
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("FAIL!","Thêm thất bại! Kiểm tra lại mã chuyến bay","error");}, 1000);';
					echo '</script>';
				}

			}
			//View
			$title = "Thêm chuyến bay";
			$view = "app/views/chuyenbay/v_them_chuyenbay.php";
			include("public/include/layout.php");
		}

		
	}
}
?>