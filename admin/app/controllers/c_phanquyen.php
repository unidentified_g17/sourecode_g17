<?php 
@session_start();
require_once("app/models/m_phanquyen.php");
class C_phanquyen
{
	private $err="";

	public function Hien_thi_dang_nhap()
	{
		if(isset($_POST["btnLogin"]))
		{
			$username = $_POST["username"];
			$password = $_POST["password"];
			$this->Kiem_tra_role($username, $password);
		}

		if(isset($_SESSION["quyen"]))
		{
			$title = "SkyTicket CMS";
			include("public/include/layout.php");
		}
		else
		{
			$_SESSION["errorLogin"] = $this->err;
			header("location:login.php");
		}
	}

	function Kiem_tra_role($username, $password)
	{
		$m_phanquyen = new M_phanquyen();
		$users = $m_phanquyen->Load_Taikhoan();
		foreach ($users as $u) {
			if($u->TENTAIKHOAN != $username)
			{
				$this->err = "Không tìm thấy tên đăng nhập này.";
			}
			elseif($u->MATKHAU != $password)
			{
				$this->err = "Bạn đã nhập sai mật khẩu.";
			}
			else
			{
				$user = $m_phanquyen->Doc_phan_quyen($username, $password);
				//Thành công thì lưu vô session
				$_SESSION["quyen"]=$user->MAQUYEN;
				$_SESSION["tennhanvien"]=$user->TENNHANVIEN;
			}
		}
	}
}

?>