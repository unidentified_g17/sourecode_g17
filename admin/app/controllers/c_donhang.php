<?php 
@session_start();
if(!($_SESSION["quyen"]=="AD" || $_SESSION["quyen"]=="KT"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_donhang.php");
	class C_donhang
	{
		public function Hien_thi_donhang()
		{
		//Model
			$m_donhang = new M_donhang();
			$dhs = $m_donhang->Doc_donhang();

		//View
			$title = "Danh sách đơn hàng";
			$view = "app/views/donhang/v_donhang.php";
			include("public/include/layout.php");
		}
	}
}
?>