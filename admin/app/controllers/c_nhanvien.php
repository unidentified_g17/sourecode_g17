<?php
@session_start();
if(!($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "NS"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_nhanvien.php");
	class C_nhanvien
	{
		public function Hien_thi_nhanvien()
		{
		//Model
			$m_nhanvien = new M_nhanvien();
			$nvs = $m_nhanvien->Doc_nhanvien();

		//View
			$title = "Danh sách nhân viên";
			$view = "app/views/nhanvien/v_nhanvien.php";
			include("public/include/layout.php");
		}

		public function Hien_thi_them_nhanvien()
		{
		//Model
			$m_nhanvien = new M_nhanvien();
			if(isset($_POST["them_moi"]))
			{
				$ten_nhan_vien = $_POST["ten_nhan_vien"];
				$ngay_sinh = date('Y-m-d', strtotime($_POST["ngay_sinh"]));
				$dia_chi = $_POST["dia_chi"];
				$phone = $_POST["phone"];
				$email = $_POST["email"];
				$kq1 = $m_nhanvien->Them_nhanvien($ten_nhan_vien, $dia_chi, $email, $phone, $ngay_sinh);
				$newID = $m_nhanvien->getLastID1();

				if($_POST["checkbox_cap_quyen"] == "on")
				{
					$ma_quyen = $_POST["ma_quyen"];
					$tai_khoan = $_POST["tai_khoan"];
					$mat_khau = $_POST["mat_khau"];
					$kq2 = $m_nhanvien->Them_quyen_nhanvien($ma_quyen, $newID, $tai_khoan, $mat_khau);
				}

				if($kq1)
				{
				//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Thêm thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="nhanvien.php"} , 2000);';
					echo '</script>';
				}
			}

		//View
			$title = "Thêm nhân viên";
			$view = "app/views/nhanvien/v_them_nhanvien.php";
			include("public/include/layout.php");
		}

		public function Hien_thi_sua_nhanvien()
		{
		//Model
			$ma = $_GET["ma"];
			$m_nhanvien = new M_nhanvien();
			$nhanvien = $m_nhanvien->Doc_nhanvien_theo_ma($ma);

			if(isset($_POST["cap_nhat"]))
			{
				$ten_nhan_vien = $_POST["ten_nhan_vien"];
				$ngay_sinh = date('Y-m-d', strtotime($_POST["ngay_sinh"]));
				$dia_chi = $_POST["dia_chi"];
				$phone = $_POST["phone"];
				$email = $_POST["email"];
				$kq1 = $m_nhanvien->Sua_nhanvien($ten_nhan_vien, $dia_chi, $email, $phone, $ngay_sinh, $ma);

			/*if($_POST["checkbox_cap_quyen"] == "on")
			{
				$ma_quyen = $_POST["ma_quyen"];
				$tai_khoan = $_POST["tai_khoan"];
				$mat_khau = $_POST["mat_khau"];
				$kq2 = $m_nhanvien->Them_quyen_nhanvien($ma_quyen, $newID, $tai_khoan, $mat_khau);
			}*/

			if($kq1)
			{
				//Thêm thành công
				echo '<script type="text/javascript">';
				echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Sửa thành công!","success"); }, 1000);';
				echo 'setTimeout(function () { location.href="nhanvien.php"} , 2000);';
				echo '</script>';
			}
		}

		//View
		$title = "Sửa nhân viên";
		$view = "app/views/nhanvien/v_sua_nhanvien.php";
		include("public/include/layout.php");
	}
}
}
?>