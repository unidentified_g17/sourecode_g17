<?php 
@session_start();
require_once("app/models/m_backup.php");
class C_backup
{
	public function Hien_thi_khachhang()
	{
		//Model
		$m_backup = new M_backup();
		$khs = $m_backup->Doc_bangKH();

		//View
		$title = "Danh sách history khách hàng";
		$view = "app/views/backup/v_khachhang.php";
		include("public/include/layout.php");
	}

	public function Hien_thi_ddv()
	{
		//Model
		$m_backup = new M_backup();
		$dhs = $m_backup->Doc_donhang();

		//View
		$title = "Danh sách history đơn đặt vé";
		$view = "app/views/backup/v_ddv.php";
		include("public/include/layout.php");
	}
}
?>