<?php 
@session_start();
if(!($_SESSION["quyen"]=="AD" || $_SESSION["quyen"]=="QL"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_maybay.php");
	class C_maybay
	{
		public function Hien_thi_maybay()
		{
		//Model
			$m_maybay = new M_maybay();
			$maybays = $m_maybay->Doc_maybay();

		//View
			$title = "Danh sách máy bay";
			$view = "app/views/thongtinbay/v_maybay.php";
			include("public/include/layout.php");
		}

		public function Hien_thi_them_maybay()
		{
		//Models
			$m_maybay = new M_maybay();
			$oldRow = $m_maybay->getRowCount();
			if(isset($_POST["them_moi"]))
			{
				$ma_may_bay = $_POST["ma_may_bay"];
				$ten_may_bay = $_POST["ten_may_bay"];
				$so_luong_ghe = $_POST["so_luong_ghe"];
				$kq = $m_maybay->Them_maybay($ma_may_bay, $ten_may_bay, $so_luong_ghe);
				$newRow = $m_maybay->getRowCount();
				if($oldRow != $newRow)
				{
				//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Thêm thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="maybay.php"} , 2000);';
					echo '</script>';
				}
				else
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("FAIL!","Thêm thất bại! Kiểm tra lại mã máy bay","error");}, 1000);';
					echo '</script>';
				}
			}
		//Views
			$title = "Thêm máy bay";
			$view = "app/views/thongtinbay/v_them_maybay.php";
			include("public/include/layout.php");
		}

	//Sửa
		public function Hien_thi_sua_maybay()
		{
			$ma_may_bay = $_GET["ma"];
		//Model
			$m_maybay = new M_maybay();
			$maybay = $m_maybay->Doc_maybay_theo_ma($ma_may_bay);
			if(isset($_POST["cap_nhat"]))
			{
				$ten_may_bay = $_POST["ten_may_bay"];
				$so_luong_ghe = $_POST["so_luong_ghe"];
				$kq = $m_maybay->Sua_maybay($ten_may_bay, $so_luong_ghe, $ma_may_bay);
				if($kq)
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Sửa thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="maybay.php"} , 2000);';
					echo '</script>';
				}
			}
		//View
			$title = "Sửa máy bay";
			$view = "app/views/thongtinbay/v_sua_maybay.php";
			include("public/include/layout.php");
		}
	}
}
?>