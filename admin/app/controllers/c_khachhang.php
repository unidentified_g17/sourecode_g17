<?php
@session_start();
if(!($_SESSION["quyen"]=="AD"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_khachhang.php");
	class C_khachhang
	{
		public function Hien_thi_khachhang()
		{
		//Model
			$m_khachhang = new M_khachhang();
			$khs = $m_khachhang->Doc_khachhang();

		//View
			$title = "Danh sách khách hàng";
			$view = "app/views/khachhang/v_khachhang.php";
			include("public/include/layout.php");
		}

	//Sửa
		public function Hien_thi_sua_khachhang()
		{
			$ma_khach_hang = $_GET["ma"];
			//Model
			$m_khachhang = new M_khachhang();
			$khachhang = $m_khachhang->Doc_khachhang_theo_ma($ma_khach_hang);
			if(isset($_POST["cap_nhat"]))
			{
				$ten_khach_hang = $_POST["ten_khach_hang"];
				$dia_chi = $_POST["dia_chi"];
				$phone = $_POST["phone"];
				$email = $_POST["email"];
				$kq = $m_khachhang->Sua_khachhang($ten_khach_hang, $dia_chi, $phone, $email, $ma_khach_hang);
				if($kq)
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Sửa thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="khachhang.php"} , 2000);';
					echo '</script>';
				}
		}
		//View
		$title = "Sửa khách hàng";
		$view = "app/views/khachhang/v_sua_khachhang.php";
		include("public/include/layout.php");
	}
}
}
?>