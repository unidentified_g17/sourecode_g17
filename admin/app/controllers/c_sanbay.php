<?php 
@session_start();
if(!($_SESSION["quyen"]=="AD" || $_SESSION["quyen"]=="QL"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_sanbay.php");
	class C_sanbay
	{
		public function Hien_thi_sanbay()
		{
		//Model
			$m_sanbay = new M_sanbay();
			$sanbays = $m_sanbay->Doc_sanbay();

		/*//Phân trang
		include("../configs/Pager.php");
		$p = new pager();
		$limit = 10;
		$count = count($sanbayss);
		$vt = $p->findStart($limit);
		$pages = $p->findPages($count, $limit);
		$curpage = $_GET["page"];
		$list = $p->pageList($curpage,$pages);
		$sanbays = $m_sanbay->Doc_sanbay($vt, $limit);*/

		//View
		$title = "Danh sách sân bay";
		$view = "app/views/thongtinbay/v_sanbay.php";
		include("public/include/layout.php");
	}

	//`MASANBAY`, `TENSANBAY`, `MADIADIEM`
	public function Hien_thi_them_sanbay()
	{
		$diadiems = $this->Doc_diadiem();

		//Models
		$m_sanbay = new M_sanbay();
		$oldRow = $m_sanbay->getRowCount();
		if(isset($_POST["them_moi"]))
		{
			$ma_san_bay = $_POST["ma_san_bay"];
			$ten_san_bay = $_POST["ten_san_bay"];
			$ma_dia_diem = $_POST["ma_dia_diem"];
			$kq = $m_sanbay->Them_sanbay($ma_san_bay, $ten_san_bay, $ma_dia_diem);
			$newRow = $m_sanbay->getRowCount();
			if($oldRow != $newRow)
			{
				//Thêm thành công
				echo '<script type="text/javascript">';
				echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Thêm thành công!","success"); }, 1000);';
				echo 'setTimeout(function () { location.href="sanbay.php"} , 2000);';
				echo '</script>';
			}
			else
			{
				echo '<script type="text/javascript">';
				echo 'setTimeout(function () { swal("FAIL!","Thêm thất bại! Kiểm tra lại mã sân bay","error");}, 1000);';
				echo '</script>';
			}
		}
		//Views
		$title = "Thêm sân bay";
		$view = "app/views/thongtinbay/v_them_sanbay.php";
		include("public/include/layout.php");
	}

	//Sửa
	public function Hien_thi_sua_sanbay()
	{
		$diadiems = $this->Doc_diadiem();

		$ma_san_bay = $_GET["ma"];
		//Model
		$m_sanbay = new M_sanbay();
		$sanbay = $m_sanbay->Doc_sanbay_theo_ma($ma_san_bay);
		if(isset($_POST["cap_nhat"]))
		{
			$ma_san_bay = $_POST["ma_san_bay"];
			$ten_san_bay = $_POST["ten_san_bay"];
			$ma_dia_diem = $_POST["ma_dia_diem"];
			$kq = $m_sanbay->Sua_sanbay($ten_san_bay, $ma_dia_diem, $ma_san_bay);
			if($kq)
			{
				echo '<script type="text/javascript">';
				echo 'setTimeout(function () { $(".trang-sua").fadeOut(0); swal("WOW!","Sửa thành công!","success"); }, 1000);';
				echo 'setTimeout(function () { location.href="sanbay.php"} , 2000);';
				echo '</script>';
			}
		}
		//View
		$title = "Sửa sân bay";
		$view = "app/views/thongtinbay/v_sua_sanbay.php";
		include("public/include/layout.php");
	}

	public function Doc_diadiem()
	{
		include("app/models/m_diadiem.php");
		$m_diadiem = new M_diadiem();
		return $m_diadiem->Doc_diadiem();
	}
}
}
?>