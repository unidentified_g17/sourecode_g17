<?php
@session_start();
if(!($_SESSION["quyen"]=="AD" || $_SESSION["quyen"]=="KT"))
{
	header('location:no-permission.php');
}
else
{
	include("app/models/m_khuyenmai.php");
	class C_khuyenmai
	{
		public function Hien_thi_khuyenmai()
		{
		//Model
			$m_khuyenmai = new M_khuyenmai();
			$kms = $m_khuyenmai->Doc_khuyenmai();

		//View
			$title = "Cập nhật khuyến mãi";
			$view = "app/views/khuyenmai/v_khuyenmai.php";
			include("public/include/layout.php");
		}

		public function Them_khuyenmai()
		{
			$m_khuyenmai = new M_khuyenmai();
			$oldRow = $m_khuyenmai->getRowCount();
		//Model
			if(isset($_POST["them_moi"]))
			{
				$ma_khuyen_mai = $_POST["ma_khuyen_mai"];
				$khuyen_mai = $_POST["khuyen_mai"];
				$kq = $m_khuyenmai->Them_khuyenmai($ma_khuyen_mai, $khuyen_mai);
				$newRow = $m_khuyenmai->getRowCount();
				if($oldRow != $newRow)
				{
				//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("WOW!","Thêm thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="khuyenmai.php"} , 1000);';
					echo '</script>';
				}
				else
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("FAIL!","Thêm thất bại! Kiểm tra lại mã khuyến mãi","error");}, 1000);';
					echo '</script>';
				}
			}
		}

		public function Capnhat_khuyenmai()
		{
			$m_khuyenmai = new M_khuyenmai();
		//Model
			if(isset($_POST["cap_nhat"]))
			{
				$ma_khuyen_mai = $_POST["ma_khuyen_mai"];
				$khuyen_mai = $_POST["khuyen_mai"];
				$kq = $m_khuyenmai->Sua_khuyenmai($khuyen_mai, $ma_khuyen_mai);
				if($kq)
				{
				//Thêm thành công
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("WOW!","Cập nhật thành công!","success"); }, 1000);';
					echo 'setTimeout(function () { location.href="khuyenmai.php"} , 1000);';
					echo '</script>';
				}
				else
				{
					echo '<script type="text/javascript">';
					echo 'setTimeout(function () { swal("FAIL!","Cập nhật thất bại! Kiểm tra lại mã khuyến mãi","error");}, 1000);';
					echo '</script>';
				}
			}
		}
	}
}
?>