<?php 
@session_start();
if (!($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "KT"))
{
	header('location:no-permission.php');
}
else
{
	include_once("app/models/m_report.php");
	class C_report
	{
		public function Hien_thi_bao_cao_doanh_thu_thang_nam()
		{
			//Model

			//View
			$title = "Báo cáo doanh thu tháng năm";
			$view = "app/views/report/v_thang_nam.php";
			include("public/include/layout.php");
		}

		public function Hien_thi_bao_cao_doanh_thu_nam()
		{
			//Model

			//View
			$title = "Báo cáo doanh thu năm";
			$view = "app/views/report/v_nam.php";
			include("public/include/layout.php");
		}
	}
}


?>