<?php 
include_once("../configs/database.php");
class M_khachhang extends database
{
	public function Doc_khachhang()
	{
		$sql = "select * from khachhang";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_khachhang_theo_ma($MAKHACHHANG)
	{
		$sql = "select * from khachhang where MAKHACHHANG = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($MAKHACHHANG));
	}

	public function getRowCount()
	{
		$sql = "select count(*) from khachhang";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	//`MAKHACHHANG`, `TENKHACHHANG`, `DIACHI`, `PHONE`, `EMAIL`
	public function Sua_khachhang($TENKHACHHANG, $DIACHI, $PHONE, $EMAIL, $MAKHACHHANG)
	{
		$sql = "update khachhang set TENKHACHHANG = ?, DIACHI = ?, PHONE = ?, EMAIL = ? where MAKHACHHANG = ?";
		$this->setQuery($sql);
		return $this->execute(array($TENKHACHHANG, $DIACHI, $PHONE, $EMAIL, $MAKHACHHANG));
	}

	public function Xoa_khachhang($MAKHACHHANG)
	{
		$sql = "update khachhang set DELETEFLAG = 1 where MAKHACHHANG = ?; call xoakhachhang(" . $MAKHACHHANG . ")";
		$this->setQuery($sql);
		return $this->execute(array($MAKHACHHANG));
	}
}
 ?>