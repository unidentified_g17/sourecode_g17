<?php 
include_once("../configs/database.php");
class M_sanbay extends database
{
	public function Doc_sanbay()
	{
		$sql = "select sb.*, dd.TENDIADIEM from sanbay sb, diadiem dd where sb.MADIADIEM = dd.MADIADIEM order by dd.TENDIADIEM";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_sanbay_theo_ma($ma_san_bay)
	{
		$sql = "select * from sanbay where MASANBAY = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_san_bay));
	}

	public function getRowCount()
	{
		$sql = "select count(*) from sanbay";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	//`MASANBAY`, `TENSANBAY`, `MADIADIEM`
	public function Them_sanbay($ma_san_bay, $ten_san_bay, $ma_dia_diem)
	{
		$sql = "insert into sanbay values(?,?,?)";
		$this->setQuery($sql);
		return $this->execute(array($ma_san_bay,$ten_san_bay,$ma_dia_diem));
	}

	public function Sua_sanbay($ten_san_bay, $ma_dia_diem, $ma_san_bay)
	{
		$sql = "update sanbay set TENSANBAY = ?, MADIADIEM = ? where MASANBAY=?";
		$this->setQuery($sql);
		return $this->execute(array($ten_san_bay, $ma_dia_diem, $ma_san_bay));
	}

	public function Xoa_sanbay($ma_san_bay)
	{
		$sql = "delete from sanbay where MASANBAY = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_san_bay));
	}
}
?>