<?php 
include_once("../configs/database.php");
class M_khuyenmai extends database
{
	public function Doc_khuyenmai()
	{
		$sql = "select * from khuyenmai";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_khuyenmai_theo_ma($ma_khuyen_mai)
	{
		$sql = "select * from khuyenmai where MAKHUYENMAI = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_khuyen_mai));
	}

	public function getRowCount()
	{
		$sql = "select count(*) from khuyenmai";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	//MAKHUYENMAI, KHUYENMAI
	public function Them_khuyenmai($ma_khuyen_mai, $khuyen_mai)
	{
		$sql = "insert into khuyenmai values(?,?)";
		$this->setQuery($sql);
		return $this->execute(array($ma_khuyen_mai, $khuyen_mai));
	}

	public function Sua_khuyenmai($khuyen_mai, $ma_khuyen_mai)
	{
		$sql = "update khuyenmai set KHUYENMAI = ? where MAKHUYENMAI=?";
		$this->setQuery($sql);
		return $this->execute(array($khuyen_mai, $ma_khuyen_mai));
	}

	public function Xoa_khuyenmai($ma_khuyen_mai)
	{
		$sql = "delete from khuyenmai where MAKHUYENMAI = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_khuyen_mai));
	}
}

 ?>