<?php 
include("../configs/database.php");
class M_report extends database
{
	public function Bao_cao_doanh_thu_thang_nam($thang, $nam)
	{
		$sql = "CALL baocaodoanhthuthangnam (?, ?)";
		$this->setQuery($sql);
		return $this->loadAllRows(array($thang, $nam));
	}

	public function Bao_cao_doanh_thu_nam($nam)
	{
		$sql = "CALL baocaodoanhthunam (?)";
		$this->setQuery($sql);
		return $this->loadAllRows(array($nam));
	}
}
?>