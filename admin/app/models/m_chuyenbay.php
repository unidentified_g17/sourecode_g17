<?php 
include("../configs/database.php");
class M_chuyenbay extends database
{
	public function Doc_chuyenbay()
	{
		// MACHUYENBAY
		// MAMAYBAY
		// SANBAYDI
		// TENSANBAYDI
		// DIADIEMDI
		// SANBAYDEN
		// TENSANBAYDEN
		// DIADIEMDEN
		// THOIGIANBAY
		// THOIGIANDEN
		// TRANSITION
		// TENSANBAYTRANS
		// DIADIEMTRANS
		// DABAY
		$sql = "select cb.MACHUYENBAY, cb.MAMAYBAY,
		cb.SANBAYDI, sb1.TENSANBAY as 'TENSANBAYDI', dd1.TENDIADIEM as 'DIADIEMDI',
		cb.SANBAYDEN, sb2.TENSANBAY as 'TENSANBAYDEN', dd2.TENDIADIEM as 'DIADIEMDEN',
		cb.THOIGIANBAY, cb.THOIGIANDEN, 
		cb.TRANSITION, sb3.TENSANBAY as 'TENSANBAYTRANS', dd3.TENDIADIEM as 'DIADIEMTRANS',
		cb.DABAY,
		( SELECT COUNT( v1.MAVE )
		FROM ve AS v1
		WHERE cb.MACHUYENBAY = v1.MACHUYENBAY
		AND v1.TRANGTHAI =1
		) AS 'DABAN', 

		( SELECT COUNT( v2.MAVE )
		FROM ve AS v2
		WHERE cb.MACHUYENBAY = v2.MACHUYENBAY
		AND (
		v2.TRANGTHAI =0
		OR v2.TRANGTHAI =2
		)
		) AS 'CHUABAN', 

		( SELECT COUNT( v3.MAVE )
		FROM ve AS v3
		WHERE v3.MACHUYENBAY = cb.MACHUYENBAY
		) AS 'TONGSO'

		from chuyenbay as cb
		left join sanbay as sb1 on cb.SANBAYDI = sb1.MASANBAY
		left join diadiem as dd1 on sb1.MADIADIEM = dd1.MADIADIEM
		left join sanbay as sb2 on cb.SANBAYDEN = sb2.MASANBAY
		left join diadiem as dd2 on sb2.MADIADIEM = dd2.MADIADIEM
		left join sanbay as sb3 on cb.TRANSITION = sb3.MASANBAY
		left join diadiem as dd3 on sb3.MADIADIEM = dd3.MADIADIEM
		order by cb.DABAY, cb.THOIGIANBAY desc";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Xoa_chuyenbay($ma_chuyen_bay)
	{
		$sql = "delete from chuyenbay where MACHUYENBAY = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_chuyen_bay));
	}

	//MACHUYENBAY, MAMAYBAY, SANBAYDI, SANBAYDEN, THOIGIANBAY, THOIGIANDEN, TRANSITION, DABAY
	// b?: đã bay (bit)
	public function Them_chuyenbay($MACHUYENBAY, $MAMAYBAY, $SANBAYDI, $SANBAYDEN, $GIAVEGOC, $THOIGIANBAY, $THOIGIANDEN)
	{
		$sql = "insert into chuyenbay values (?,?,?,?,?,?,?,?,b?)";
		$this->setQuery($sql);
		return $this->execute(array($MACHUYENBAY, $MAMAYBAY, $SANBAYDI, $SANBAYDEN, $GIAVEGOC, $THOIGIANBAY, $THOIGIANDEN, NULL, 0));
	}

	public function Them_chuyenbay_cotrans($MACHUYENBAY, $MAMAYBAY, $SANBAYDI, $SANBAYDEN, $GIAVEGOC, $THOIGIANBAY, $THOIGIANDEN, $TRANSITION)
	{
		$sql = "insert into chuyenbay values (?,?,?,?,?,?,?,?,b?)";
		$this->setQuery($sql);
		return $this->execute(array($MACHUYENBAY, $MAMAYBAY, $SANBAYDI, $SANBAYDEN, $GIAVEGOC, $THOIGIANBAY, $THOIGIANDEN, $TRANSITION, 0));
	}

	//MAVE, MACHUYENBAY, LOAIVE, TRANGTHAI, DELETEFLAG, DELETETIME
	public function Them_ve_chuyenbay($MACHUYENBAY)
	{
		$sql = "insert into ve values (?, ?, ?, ?, b?, ?)";
		$this->setQuery($sql);
		return $this->execute(array(NULL, $MACHUYENBAY, 0, 0, 0, NULL));
	}

	public function getRowCount()
	{
		$sql = "select count(*) from chuyenbay";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Doc_chuyenbay_theo_ma($ma_chuyen_bay)
	{
		$sql = "select * from chuyenbay where MACHUYENBAY = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_chuyen_bay));
	}

}
?>