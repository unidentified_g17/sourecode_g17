<?php 
include_once("../configs/database.php");
class M_dashboard extends database
{
	public function Thong_ke()
	{
		$sql1 = "select count(*) from chuyenbay";
		$this->setQuery($sql1);
		$chuyen_bay = $this->loadRecord();

		$sql2 = "select count(*) from khachhang";
		$this->setQuery($sql2);
		$khach_hang = $this->loadRecord();

		$sql3 = "select count(*) from dondatve";
		$this->setQuery($sql3);
		$don_dat_ve = $this->loadRecord();

		$sql4 = "select count(*) from nhanvien";
		$this->setQuery($sql4);
		$nhan_vien = $this->loadRecord();

		return $arrayThongKe = array(
			'chuyenbay' => $chuyen_bay,
			'khachhang' => $khach_hang,
			'dondatve' => $don_dat_ve,
			'nhanvien' => $nhan_vien,
			);
	}

	public function Doc_doanh_thu()
	{
		//SET sql_mode = "";
		$sql = 'select concat(Year(NGAYMUAVE),"-",Month(NGAYMUAVE)) as ThangNam, sum(THANHTIEN) as Tong from dondatve where DATHANHTOAN in(1) group by Year(NGAYMUAVE),Month(NGAYMUAVE) order by YEAR(NGAYMUAVE)';
		//$sql="select sum(THANHTIEN) from dondatve";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Chuyen_bay_trong_ngay()
	{
		$sql = "select * from chuyenbay where date(THOIGIANBAY) = '" . date("Y-m-d") ."'";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}
}

?>