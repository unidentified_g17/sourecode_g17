<?php 
include_once("../configs/database.php");
class M_diadiem extends database
{
	public function Doc_diadiem($vt = -1, $limit =-1)
	{
		$sql = "select * from diadiem";
		if($vt>=0 && $limit>0)
		{
			$sql .= " limit $vt, $limit";
		}
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_diadiem_theo_ma($ma_dia_diem)
	{
		$sql = "select * from diadiem where MADIADIEM = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_dia_diem));
	}

	//`MADIADIEM`, `TENDIADIEM`
	public function Them_diadiem($ma_dia_diem, $ten_dia_diem)
	{
		$sql = "insert into diadiem values (?, ?)";
		$this->setQuery($sql);
		return $this->execute(array($ma_dia_diem, $ten_dia_diem));
	}

	public function getRowCount()
	{
		$sql = "select count(*) from diadiem";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Sua_diadiem($ten_dia_diem, $ma_dia_diem)
	{
		$sql = "update diadiem set TENDIADIEM = ? where MADIADIEM = ?";
		$this->setQuery($sql);
		return $this->execute(array($ten_dia_diem, $ma_dia_diem));
	}

	public function Xoa_diadiem($ma_dia_diem)
	{
		$sql = "delete from diadiem where MADIADIEM = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_dia_diem));
	}
}
?>