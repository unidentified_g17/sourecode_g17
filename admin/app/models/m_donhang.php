<?php 
include("../configs/database.php");
class M_donhang extends database
{
	public function Doc_donhang()
	{
		$sql = "select ddv.*, kh.TENKHACHHANG from dondatve ddv, khachhang kh where ddv.MAKHACHHANG = kh.MAKHACHHANG order by MADONDATVE desc";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_chitietdonhang($ma_don_dat_ve)
	{
		$sql = "select * from chitietdatve as ct, ve where ct.MAVE = ve.MAVE and MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_don_dat_ve));
	}

	public function Doc_thongtinchuyenbay($ma_don_dat_ve)
	{
		$sql = "select kh.TENKHACHHANG, kh.PHONE, cb.MACHUYENBAY, cb.SANBAYDI, cb.THOIGIANBAY, cb.SANBAYDEN, cb.THOIGIANDEN from dondatve ddv, chitietdatve ct, chuyenbay cb, khachhang kh, ve where ddv.MADONDATVE = ct.MADONDATVE and ct.MAVE = ve.MAVE and ve.MACHUYENBAY = cb.MACHUYENBAY and kh.MAKHACHHANG = ddv.MAKHACHHANG and ct.MADONDATVE = ? limit 0,1";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_don_dat_ve));
	}

	public function Doc_donhang_theoma($ma_don_dat_ve)
	{
		$sql = "select DATHANHTOAN from dondatve where MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_don_dat_ve));
	}

	public function Cap_nhat_thanh_toan_0($ma_don_dat_ve)
	{
		$sql = "update dondatve set DATHANHTOAN = 0 where MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_don_dat_ve));
	}

	public function Cap_nhat_thanh_toan_1($ma_don_dat_ve)
	{
		$sql = "update dondatve set DATHANHTOAN = 1 where MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_don_dat_ve));
	}

	public function Xoa_donhang($ma_don_dat_ve)
	{
		$sql = "update dondatve set DELETEFLAG = 1 where MADONDATVE = ?; call xoaddvvactdv(" . $ma_don_dat_ve . ")";
		$this->setQuery($sql);
		return $this->execute(array($ma_don_dat_ve));

	}
}
?>