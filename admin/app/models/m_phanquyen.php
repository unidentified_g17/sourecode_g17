<?php 
include_once("../configs/database.php");
class M_phanquyen extends database
{
	public function Doc_phan_quyen($tentaikhoan, $matkhau)
	{
		$sql = "select nv.TENNHANVIEN, tk.* from nhanvien nv, taikhoan tk where nv.MANHANVIEN = tk.MANHANVIEN and tk.TENTAIKHOAN=? and tk.MATKHAU=?";
		$this->setQuery($sql);
		return $this->loadRow(array($tentaikhoan, $matkhau));
	}

	public function Load_Taikhoan()
	{
		$sql = "select TENTAIKHOAN, MATKHAU from taikhoan";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}
}

 ?>
