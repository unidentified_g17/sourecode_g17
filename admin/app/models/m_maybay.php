<?php 
include_once("../configs/database.php");
class M_maybay extends database
{
	public function Doc_maybay()
	{
		$sql = "select * from maybay";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_maybay_theo_ma($ma_may_bay)
	{
		$sql = "select * from maybay where MAMAYBAY = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_may_bay));
	}

	public function getRowCount()
	{
		$sql = "select count(*) from maybay";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	//`MAMAYBAY`, `TENMAYBAY`, `SOLUONGGHE`
	public function Them_maybay($ma_may_bay, $ten_may_bay, $so_luong_ghe)
	{
		$sql = "insert into maybay values(?,?,?)";
		$this->setQuery($sql);
		return $this->execute(array($ma_may_bay,$ten_may_bay,$so_luong_ghe));
	}

	public function Sua_maybay($ten_may_bay, $so_luong_ghe, $ma_may_bay)
	{
		$sql = "update maybay set TENMAYBAY = ?, SOLUONGGHE = ? where MAMAYBAY=?";
		$this->setQuery($sql);
		return $this->execute(array($ten_may_bay, $so_luong_ghe, $ma_may_bay));
	}

	public function Xoa_maybay($ma_may_bay)
	{
		$sql = "delete from maybay where MAMAYBAY = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_may_bay));
	}
}
?>