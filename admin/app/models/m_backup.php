<?php 
include_once("../configs/database.php");
class M_backup extends database
{
	public function Doc_bangKH()
	{
		$sql = "select * from historykhachhang";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_donhang()
	{
		$sql = "select * from historydondatve";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_chitietdonhang($ma_don_dat_ve)
	{
		$sql = "select * from historychitietdatve as ct, ve where ct.MAVE = ve.MAVE and MADONDATVE = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_don_dat_ve));
	}
}
?>