<?php 
include_once("../configs/database.php");
class M_nhanvien extends database
{
	public function Doc_nhanvien()
	{
		$sql = "select nhanvien.*, MAQUYEN from nhanvien LEFT JOIN taikhoan ON nhanvien.MANHANVIEN = taikhoan.MANHANVIEN ORDER by MANHANVIEN";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_nhanvien_theo_ma($ma_nhan_vien)
	{
		$sql = "select * from nhanvien where MANHANVIEN = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_nhan_vien));
	}

	//MANHANVIEN, TENNHANVIEN, DIACHI, EMAIL, PHONE, NGAYSINH
	public function Them_nhanvien($TENNHANVIEN, $DIACHI, $EMAIL, $PHONE, $NGAYSINH)
	{
		$sql = "insert into nhanvien values(?,?,?,?,?,?)";
		$this->setQuery($sql);
		return $this->execute(array(NULL, $TENNHANVIEN, $DIACHI, $EMAIL, $PHONE, $NGAYSINH));
	}

	//MAQUYEN, MANHANVIEN, TENTAIKHOAN, MATKHAU
	public function Them_quyen_nhanvien($MAQUYEN, $MANHANVIEN, $TENTAIKHOAN, $MATKHAU)
	{
		$sql = "insert into taikhoan values(?,?,?,?)";
		$this->setQuery($sql);
		return $this->execute(array($MAQUYEN, $MANHANVIEN, $TENTAIKHOAN, $MATKHAU));
	}

	public function Sua_nhanvien($TENNHANVIEN, $DIACHI, $EMAIL, $PHONE, $NGAYSINH, $MANHANVIEN)
	{
		$sql = "update nhanvien set TENNHANVIEN = ?, DIACHI = ?, EMAIL = ?, PHONE=?, NGAYSINH = ? where MANHANVIEN = ?";
		$this->setQuery($sql);
		return $this->execute(array($TENNHANVIEN, $DIACHI, $EMAIL, $PHONE, $NGAYSINH, $MANHANVIEN));
	}

	public function getLastID1()
	{
		return $this->getLastId();
	}

	public function Xoa_nhanvien($ma_nhan_vien)
	{
		$sql = "delete from nhanvien where MANHANVIEN = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_nhan_vien));
	}
}

 ?>