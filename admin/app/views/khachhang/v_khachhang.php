<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách khách hàng <p class="badge"><?php echo count($khs) ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-khachhang" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width="1%">#</th>
            <th>Họ tên</th>
            <th>Địa chỉ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($khs as $kh)
          { ?>
          <tr>
            <td><?php echo $kh->MAKHACHHANG ?></td>
            <td><?php echo $kh->TENKHACHHANG ?></td>
            <td><?php echo $kh->DIACHI ?></td>
            <td><?php echo $kh->PHONE ?></td>
            <td><?php echo $kh->EMAIL ?></td>
            <td>
              <a href="sua-khachhang.php?ma=<?php echo $kh->MAKHACHHANG ?>" class="btn btn-default btn-flat btn-suaxoa">Sửa</a>
              <button ma_khach_hang="<?php echo $kh->MAKHACHHANG ?>" class="xoa-khach-hang btn btn-danger btn-flat btn-suaxoa">Backup</button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th width="1%">#</th>
            <th>Họ tên</th>
            <th>Địa chỉ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->