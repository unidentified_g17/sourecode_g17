  <div class="row trang-sua">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Sửa khách hàng</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Mã khách hàng</label>
                  <input type="text" class="form-control input-lg" value="<?php echo $khachhang->MAKHACHHANG ?>" readonly />
                </div>
              </div>

              <div class="col-md-8">
               <div class="form-group">
                <label>Tên khách hàng</label>
                <input type="text" class="form-control input-lg kiemtra" id="ten_khach_hang" name="ten_khach_hang" placeholder="Tên khách hàng" data_error="Nhập tên khách hàng" value="<?php echo $khachhang->TENKHACHHANG ?>" />
              </div>
            </div>
        </div>

        <div class="form-group">
          <label>Địa chỉ</label>
          <input type="text" class="form-control kiemtra" id="dia_chi" name="dia_chi" placeholder="Địa chỉ" data_error="Nhập địa chỉ" value="<?php echo $khachhang->DIACHI ?>" />
        </div>

        <div class="form-group">
          <label>Phone</label>
          <input type="text" class="form-control kiemtra" id="phone" name="phone" placeholder="Điện thoại" data_error="Nhập điện thoại" value="<?php echo $khachhang->PHONE ?>" />
        </div>

        <div class="form-group">
          <label>Email</label>
          <input type="email" class="form-control kiemtra" id="email" name="email" placeholder="Email" data_error="Nhập email" value="<?php echo $khachhang->EMAIL ?>" />
        </div>

        <!--button-->
        <div class="row text-center">
          <input type="submit" class="btn btn-app bg-orange btn-flat" value="Cập nhật" name="cap_nhat" onclick="return Kiemtradulieu()" />
          <a href="khachhang.php" class="btn btn-app">
            <i class="glyphicon glyphicon-remove"></i> Hủy
          </a>
        </div>
      </form>
    </div><!--body-->
  </div><!-- /.box-body -->
</div><!-- /.box -->
</div>

<script type="text/javascript">
  $(function()
  {
    $('#ngay_sinh').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy'
    });
  })
</script>