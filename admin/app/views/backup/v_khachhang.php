<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách history khách hàng<p class="badge"><?php echo count($khs) ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-khachhang" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width="1%">#</th>
            <th>Họ tên</th>
            <th>Địa chỉ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Ngày backup</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($khs as $kh)
          { ?>
          <tr>
            <td><?php echo $kh->MAKHACHHANG ?></td>
            <td><?php echo $kh->TENKHACHHANG ?></td>
            <td><?php echo $kh->DIACHI ?></td>
            <td><?php echo $kh->PHONE ?></td>
            <td><?php echo $kh->EMAIL ?></td>
            <td><?php echo $kh->DELETETIME ?></td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th width="1%">#</th>
            <th>Họ tên</th>
            <th>Địa chỉ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Ngày xóa</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->