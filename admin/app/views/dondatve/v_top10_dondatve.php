<div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Đơn đặt vé mới nhất</h3>
      </div>
      <div class="box-body chart-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>ID</th>
              <th>Tên khách hàng</th>
              <th>Ngày mua vé</th>
              <th>Thành tiền</th>
              <th>Đã thanh toán</th>
            </tr>
          </thead>
          <tbody>
          <?php 
          foreach($dondatves as $ve)
          {
           ?>
            <tr>
              <td><a href="#"><?php echo $ve->MADONDATVE ?></a></td> <!--dẫn tới trang chi tiết đơn hàng (để in)-->
              <td><?php echo $ve->TENKHACHHANG ?></td>
              <td><?php echo $ve->NGAYMUAVE ?></td>
              <td><?php echo number_format($ve->THANHTIEN) ?></td>
              <td><?php echo ($ve->DATHANHTOAN==1)?"<span class='label label-success'>ĐÃ THANH TOÁN</span>":"<span class='label label-danger'>CHƯA THANH TOÁN</span>" ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>