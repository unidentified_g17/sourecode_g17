  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Thêm chuyến bay</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="row">
              <!--cột trái-->  
              <div class="col-md-8">
               <div class="form-group">
                <label for="ma_chuyen_bay">Mã chuyến bay (5 ký tự)</label>
                <input type="text" class="form-control input-lg kiemtra" id="ma_chuyen_bay" name="ma_chuyen_bay" placeholder="Mã chuyến bay" maxlength="5" data_error="Nhập mã chuyến bay">
              </div>

              <!--đi / đến-->
              <div class="row">
                <div class="col-md-6">
                  <!--sân bay đi-->
                  <div class="form-group">
                    <label>Sân bay đi</label>
                    <select class="form-control select2" style="width: 100%;" name="san_bay_di" id="sbdi">
                      <?php foreach($sanbays as $sanbay) { ?>
                      <option value="<?php echo $sanbay->MASANBAY ?>"><?php echo $sanbay->MASANBAY ?> - <?php echo $sanbay->TENSANBAY ?> :: <?php echo $sanbay->TENDIADIEM ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <!--đến-->
                <div class="col-md-6">
                  <!--sân bay đến-->
                  <div class="form-group">
                    <label>Sân bay đến</label>
                    <select class="form-control select2" style="width: 100%;" name="san_bay_den" id="sbden">
                      <?php foreach($sanbays as $sanbay) { ?>
                      <option value="<?php echo $sanbay->MASANBAY ?>"><?php echo $sanbay->MASANBAY ?> - <?php echo $sanbay->TENSANBAY ?> :: <?php echo $sanbay->TENDIADIEM ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>

              <!-- Date and time range -->
              <div class="form-group">
                <label>Thời gian bay: (đi -> đến)</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="glyphicon glyphicon-time"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservationtime" name="thoi_gian">
                </div>
                <!-- /.input group -->
              </div>

            </div><!--end cột trái-->

            <!--cột phải-->
            <div class="col-md-4">
              <!--máy bay-->
              <div class="form-group">
                <label>Máy bay </label>
                <select class="form-control select2" style="width: 100%;" name="may_bay">
                  <?php foreach($maybays as $maybay) { ?>
                  <option value="<?php echo $maybay->MAMAYBAY ?> - <?php echo $maybay->SOLUONGGHE ?>"><?php echo $maybay->MAMAYBAY ?> - <?php echo $maybay->TENMAYBAY ?> :: <?php echo $maybay->SOLUONGGHE ?> ghế</option>
                  <?php } ?>
                </select>
              </div>
              <hr/>

              <!--transition-->
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="checkbox" id="checkbox_tram_dung" name="checkbox_tram_dung"> Có trạm dừng cho chuyến bay này
                </label>
              </div>

              <div class="form-group" id="chon_tram_dung">
                <label>Chọn trạm dừng</label>
                <select class="form-control select2" style="width: 100%;" name="tram_dung">
                  <?php foreach($sanbays as $sanbay) { ?>
                  <option value="<?php echo $sanbay->MASANBAY ?>"><?php echo $sanbay->MASANBAY ?> - <?php echo $sanbay->TENSANBAY ?> :: <?php echo $sanbay->MADIADIEM ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>

          <!-- giá cho chuyến bay-->
          <div class="form-group">
            <label for="gia_ve">Giá vé chuyến bay</label>
            <input type="text" class="form-control kiemtra" id="gia_ve" name="gia_ve" placeholder="Giá vé chuyến bay" data_error="Nhập giá vé chuyến bay">
          </div>

          <!--button-->
          <div class="row text-center">
            <input type="submit" onclick="return Kiemtradulieu_cbay()" class="btn btn-app bg-orange btn-flat" value="Thêm mới" name="them_moi" />
            <a href="chuyenbay.php" class="btn btn-app">
              <i class="glyphicon glyphicon-remove"></i> Hủy
            </a>
          </div>
        </form>
      </div><!--body-->



    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div>


<script>
  $(function () {

    $('#chon_tram_dung').hide();
    $('#checkbox_tram_dung').click(function(){
      if($(this).is(":checked"))
      {
        $('#chon_tram_dung').show(500);
        $('#tram_dung').addClass('kiemtra');
      }
      else
      {
        $('#chon_tram_dung').hide(200);
        $('#tram_dung').removeClass('kiemtra');
      }
    });

      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        minDate: new Date(),
        timePicker: true, 
        timePicker24Hour: true,
        timePickerIncrement: 5, 
        locale: {
          format: 'DD/MM/YYYY HH:mm'
        }
      });
      
    });
  </script>