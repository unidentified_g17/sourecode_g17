<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách chuyến bay <p class="badge"><?php echo count($chuyenbays) ?></h3>
    <div class="box-tools">
      <p><a href="them-chuyenbay.php" class="btn btn-block btn-warning btn-flat">Thêm mới <span class="glyphicon glyphicon-plus-sign"></span></a></p></div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-chuyenbay" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Mã chuyến bay</th>
            <th>Mã máy bay</th>
            <th>Thông tin đi</th>
            <th>Thông tin đến</th>
            <th width="19%">Tổng số vé</th>
            <th>Trạm dừng</th>
            <th>Đã bay</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($chuyenbays as $chuyenbay)
          { ?>
          <tr>
            <td><strong><?php echo $chuyenbay->MACHUYENBAY ?></strong></td>
            <td><?php echo $chuyenbay->MAMAYBAY ?></td>

            <!--đi-->
            <td>
              <strong><?php echo $chuyenbay->SANBAYDI ?></strong> - <?php echo $chuyenbay->TENSANBAYDI ?> (<?php echo $chuyenbay->DIADIEMDI ?>)
              <br/>
              <?php echo $chuyenbay->THOIGIANBAY ?>
            </td>

            <!--đến-->
            <td>
              <strong><?php echo $chuyenbay->SANBAYDEN ?></strong> - <?php echo $chuyenbay->TENSANBAYDEN ?> (<?php echo $chuyenbay->DIADIEMDEN ?>)
              <br/>
              <?php echo $chuyenbay->THOIGIANDEN ?>
            </td>
            <td>
              <h5>Tổng số vé: <?php echo $chuyenbay->TONGSO ?></h5>
              <p class="small">Đã bán: <?php echo $chuyenbay->DABAN ?> | Chưa bán: <?php echo $chuyenbay->CHUABAN ?></p>
            </td>
            <td><?php echo ($chuyenbay->TRANSITION==NULL)?"Bay thẳng":$chuyenbay->TRANSITION ?></td>
            <td><?php echo ($chuyenbay->DABAY==0)?'<span class="label label-info">Chưa</span>':'<span class="label label-danger">Rồi</span>' ?></td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
           <th>Mã chuyến bay</th>
           <th>Mã máy bay</th>
           <th>Thông tin đi</th>
           <th>Thông tin đến</th>
           <th>Tổng số vé</th>
           <th>Trạm dừng</th>
           <th>Đã bay</th>
         </tr>
       </tfoot>
     </table>
   </div>
   <!-- /.box-body -->
 </div>
  <!-- /.box -->