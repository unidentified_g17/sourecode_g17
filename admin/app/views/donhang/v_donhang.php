<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách đơn hàng <p class="badge"><?php echo count($dhs) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="table-donhang" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th width="1%">#</th>
          <th>Khách hàng</th>
          <th>SL đặt</th>
          <th>Ngày đặt</th>
          <th>Thành tiền</th>
          <th>Tình trạng</th>
          <th>Xem chi tiết</th>
          <!-- <th></th> -->
        </tr>
      </thead>
      <tbody>
        <?php foreach($dhs as $dh)
        { ?>
        <tr>
          <td><?php echo $dh->MADONDATVE ?></td>
          <td><?php echo $dh->TENKHACHHANG ?></td>
          <td><?php echo $dh->SOLUONGDAT ?></td>
          <td><?php echo $dh->NGAYMUAVE ?></td>
          <td><?php echo number_format($dh->THANHTIEN) ?> VNĐ</td>
          <td>
            <?php echo ($dh->DATHANHTOAN==0)?"<span class='text-danger'>Chưa thanh toán</span>":"Đã thanh toán" ?>
            <?php if ($dh->DATHANHTOAN == 0) {?>
            <button ma_donhang="<?php echo $dh->MADONDATVE ?>" class="btn btn-sm btn-warning btn-capnhatthanhtoan">$</button>
            <?php } ?>
          </td>
          <td>
            <button ma_donhang="<?php echo $dh->MADONDATVE ?>" class="btn btn-info xem-cthd" data-toggle="modal" data-target="#xemCTHD">Xem chi tiết</button>
          </td>
          <!-- <td>
            <?php if($dh->DATHANHTOAN==1) {
              ?>
              <button ma_don_hang="<?php echo $dh->MADONDATVE ?>" class="xoa-don-hang btn btn-danger btn-flat btn-suaxoa">Xóa</button>
              <?php } ?>
            </td> -->
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th width="1%">#</th>
            <th>Khách hàng</th>
            <th>SL đặt</th>
            <th>Ngày đặt</th>
            <th>Thành tiền</th>
            <th>Tình trạng</th>
            <th>Xem chi tiết</th>
            <!-- <th></th> -->
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

  <!-- Modal -->
  <div id="xemCTHD" class="modal fade" role="dialog">
    <div class="modal-dialog">

    </div>
  </div>