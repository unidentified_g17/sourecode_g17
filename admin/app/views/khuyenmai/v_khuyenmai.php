<div class="row">
	<div class="col-md-8">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Danh sách khuyến mãi <p class="badge"><?php echo count($kms) ?></p></h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>
						<th>Mã khuyến mãi</th>
						<th>Giá trị</th>
						<th>&nbsp;</th>
					</tr>
					<?php foreach($kms as $km)
					{ ?>
					<tr>
						<td><strong><?php echo $km->MAKHUYENMAI ?></strong></td>
						<td><?php echo $km->KHUYENMAI ?></td>
						<td>
							<a ma_khuyen_mai="<?php echo $km->MAKHUYENMAI ?>" class="sua-km btn btn-default btn-flat btn-suaxoa">Sửa</a>
							<a ma_khuyen_mai="<?php echo $km->MAKHUYENMAI ?>" class="xoa-km btn btn-danger btn-flat btn-suaxoa">Xóa</a>
						</td>
					</tr>
					<?php } ?>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>

	<div class="col-md-4">
		<div id="div_cap_nhat_khuyen_mai"></div>

		<div class="box box-success" id="them_khuyen_mai">
			<div class="box-header">
				<h3 class="box-title">Thêm khuyến mãi <p class="badge"></h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table table-body">
				<form method="post">
					<div class="input-group">
						<span class="input-group-addon">@MÃ KHUYẾN MÃI</span>
						<input type="text" class="form-control kiemtra" placeholder="5 ký tự" data_error="Nhập mã khuyến mãi" name="ma_khuyen_mai" id="them_ma_khuyen_mai" />
					</div>
					<br/>
					<div class="input-group">
						<input type="number" class="form-control kiemtra" placeholder="Giá tiền giảm" name="khuyen_mai" id="them_tien_khuyen_mai" data_error="Nhập số tiền khuyến mãi" />
						<span class="input-group-addon">VNĐ</span>
					</div>
					<br/>
					<input type="submit" name="them_moi" class="btn btn-success btn-flat pull-right" value="Thêm mới" onclick="return Kiemtradulieu()" />
				</form>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
