<!-- Content Header (Page header) -->
<section class="content-header" style="padding:0">
  <h1>Dashboard</h1>
</section>
<br/>
<!-- Info boxes -->
<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-plane"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Số lượng chuyến bay</span>
        <span class="info-box-number"><?php echo $arrayThongke["chuyenbay"] ?></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-user"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Khách hàng</span>
        <span class="info-box-number"><?php echo $arrayThongke["khachhang"] ?></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-shopping-cart"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Đơn hàng</span>
        <span class="info-box-number"><?php echo $arrayThongke["dondatve"] ?></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-yellow"><i class="glyphicon glyphicon-tint"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Nhân viên</span>
        <span class="info-box-number"><?php echo $arrayThongke["nhanvien"] ?></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <!-- LINE CHART -->
  <div class="col-md-8">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Doanh thu theo thời gian</h3>
      </div>
      <div class="box-body chart-responsive">
        <div id="chartDoanhThu" style="height: 300px;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- Đơn đặt vé mới nhất -->
    <?php 
    include("app/controllers/c_dondatve.php");
    $c_dondatve = new C_dondatve();
    $c_dondatve->Hien_thi_top10_dondatve();
    ?>
  </div>

  <div class="col-md-4">
    <!-- Đánh giá -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Đánh giá tháng này (so với tháng trước)</h3>
      </div>
      <div class="box-body chart-responsive">
        Đang cập nhật...
      </div>
      <!-- /.box-body -->
    </div>

    <!-- Chuyến bay trong ngày -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Chuyến bay trong ngày</h3><span class="badge badge-danger"><?php echo count($cbtns) ?></span>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <ul>
        <?php foreach($cbtns as $cbtn){ ?>
          <li class="item">
            <div class="product-info">
              <?php echo $cbtn->SANBAYDI ?> --> <?php echo $cbtn->SANBAYDEN ?>
              <span class="label label-warning pull-right"><?php echo number_format($cbtn->GIAVEGOC) ?> VNĐ</span>
            </a>
            <p class="product-description">
              <small>Thời gian bay: <?php echo $cbtn->THOIGIANBAY ?></small>
            </p>
          </div>
        </li>
        <!-- /.item -->
        <?php } ?>
      </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
      <a href="chuyenbay.php" class="uppercase">Xem tất cả</a>
    </div>
    <!-- /.box-footer -->
  </div>
</div>
</div>


<!--script thần thánh-->
<?php 
$data = array();
foreach ($doanh_thus as $row) {
  $data[] = $row;
}
//print_r($data);
$arrJson = json_encode($data);
//echo($data);
?>

<script type="text/javascript">
  new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'chartDoanhThu',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: <?php echo $arrJson; ?>,
  /*[
    { year: '2008', value: 20 },
    { year: '2009', value: 10 },
    { year: '2010', value: 5 },
    { year: '2011', value: 5 },
    { year: '2012', value: 20 }
    ],*/
  // The name of the data record attribute that contains x-values.
  xkey: 'ThangNam',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['Tong'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Tổng']
});
</script>