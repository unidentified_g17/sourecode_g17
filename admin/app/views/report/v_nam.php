<div class="box">
	<div class="box-header">
		<h3 class="box-title">Báo cáo doanh thu năm</h3>
		<div class="box-tools">
			<p class="hidden-print" onclick="window.print()"><a href="#" class="btn btn-block btn-warning btn-flat">In báo cáo <span class="glyphicon glyphicon-plus-sign"></span></a></p></div>
		</div>
		<div class="choose-year">
			<span class="badge">Chọn</span> năm: <input id="nam" type="number" min="2017" value="2017">
			<button id="xem-bc-nam" class="btn btn-success">Xem</button>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Tháng</th>
						<th>Số chuyến bay</th>
						<th>Tổng doanh thu</th>
					</tr>
				</thead>
				<tbody class="xuat-bc-nam">
				</tbody>
				
			</table>
		</div>
		<!-- /.box-body -->
	</div>
  <!-- /.box -->