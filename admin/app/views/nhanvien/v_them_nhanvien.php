  <div class="row trang-sua">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Thêm nhân viên</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="row">

              <div class="col-md-8">
               <div class="form-group">
                <label>Tên nhân viên</label>
                <input type="text" class="form-control input-lg kiemtra" id="ten_nhan_vien" name="ten_nhan_vien" placeholder="Tên nhân viên" data_error="Nhập tên nhân viên" />
              </div>
            </div>

            <div class="col-md-4">
             <div class="form-group">
              <label>Ngày sinh</label>
              <input type="text" class="form-control input-lg kiemtra" id="ngay_sinh1" name="ngay_sinh" placeholder="Ngày sinh" data_error="Nhập ngày sinh" />
            </div>
          </div>

        </div>

        <div class="form-group">
          <label>Địa chỉ</label>
          <input type="text" class="form-control kiemtra" id="dia_chi" name="dia_chi" placeholder="Địa chỉ" data_error="Nhập địa chỉ" />
        </div>

        <div class="form-group">
          <label>Phone</label>
          <input type="text" class="form-control kiemtra" id="phone" name="phone" placeholder="Điện thoại" data_error="Nhập điện thoại" />
        </div>

        <div class="form-group">
          <label>Email</label>
          <input type="email" class="form-control kiemtra" id="email" name="email" placeholder="Email" data_error="Nhập email" />
        </div>

        <hr/>
        <div class="checkbox">
          <label>
            <input type="checkbox" class="checkbox" id="checkbox_cap_quyen" name="checkbox_cap_quyen"> Cấp quyền cho nhân viên này
          </label>
        </div>

        <div class="row cap_quyen">
          <div class="col-md-2">
            <div class="form-group">
              <label>Mã Quyền</label>
              <select name="ma_quyen">
                <option value="AD">Admin</option>
                <option value="NS">Nhân sự</option>
                <option value="KT">Kế toán</option>
                <option value="BH">Bán hàng</option>
                <option value="QL">Quản lý chuyến bay</option>
              </select>
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group">
              <label>Tài khoản</label>
              <input type="text" class="form-control" id="tai_khoan" name="tai_khoan" placeholder="Tài khoản" data_error="Nhập tài khoản" />
            </div>
          </div>

          <div class="col-md-5">
            <div class="form-group">
              <label>Mật khẩu</label>
              <input type="text" class="form-control" id="mat_khau" name="mat_khau" placeholder="Mật khẩu" data_error="Nhập mật khẩu" />
            </div>
          </div>
          
        </div>
        

        <!--button-->
        <div class="row text-center">
          <input type="submit" class="btn btn-app bg-orange btn-flat" value="Thêm mới" name="them_moi" onclick="return Kiemtradulieu()" />
          <a href="nhanvien.php" class="btn btn-app">
            <i class="glyphicon glyphicon-remove"></i> Hủy
          </a>
        </div>
      </form>
    </div><!--body-->
  </div><!-- /.box-body -->
</div><!-- /.box -->
</div>

<style>
.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover{
  color: #777!important;
}
</style>
<script type="text/javascript">
  $(function()
  {
    $('#ngay_sinh1').datepicker({
      endDate: '-18y',
      autoclose: true,
      format: 'dd-mm-yyyy'
    });

    $('.cap_quyen').hide();
    $('#checkbox_cap_quyen').click(function(){
      if($(this).is(":checked"))
      {
        $('.cap_quyen').show(500);
        $('#tai_khoan').addClass('kiemtra');
        $('#mat_khau').addClass('kiemtra');
      }
      else
      {
        $('.cap_quyen').hide(200);
        $('#tai_khoan').removeClass('kiemtra');
        $('#mat_khau').removeClass('kiemtra');
      }
    });

  })
</script>