<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách nhân viên <p class="badge"><?php echo count($nvs) ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-nhanvien" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width="1%">#</th>
            <th>Role</th>
            <th>Họ tên</th>
            <th>Địa chỉ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($nvs as $nv)
          { ?>
          <tr>
            <td><?php echo $nv->MANHANVIEN ?></td>
            <td><?php echo $nv->MAQUYEN ?></td>
            <td><?php echo $nv->TENNHANVIEN ?></td>
            <td><?php echo $nv->DIACHI ?></td>
            <td><?php echo $nv->PHONE ?></td>
            <td><?php echo $nv->EMAIL ?></td>
            <td>
              <a href="sua-nhanvien.php?ma=<?php echo $nv->MANHANVIEN ?>" class="btn btn-default btn-flat btn-suaxoa">Sửa</a>
              <button ma_nhan_vien="<?php echo $nv->MANHANVIEN ?>" class="xoa-nhan-vien btn btn-danger btn-flat btn-suaxoa">Xóa</button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th width="1%">#</th>
            <th>Họ tên</th>
            <th>Địa chỉ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Ngày sinh</th>
            <th>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->