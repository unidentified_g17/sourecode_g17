  <div class="row trang-sua">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Thêm sân bay</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="form-group">
              <label for="ma_san_bay">Mã sân bay (3 ký tự)</label>
              <input type="text" class="form-control input-lg kiemtra" id="them_ma_san_bay" name="ma_san_bay" placeholder="Mã sân bay" data_error="Nhập mã sân bay" maxlength="3" value="<?php echo (isset($_POST["them_moi"]))?$ma_san_bay:"" ?>" >
            </div>

            <div class="form-group">
              <label for="ten_san_bay">Tên sân bay</label>
              <input type="text" class="form-control kiemtra" id="ten_san_bay" name="ten_san_bay" placeholder="Tên sân bay" data_error="Nhập tên sân bay" maxlength="20" value="<?php echo (isset($_POST["them_moi"]))?$ten_san_bay:"" ?>">
            </div>

            <div class="form-group">
              <label for="ma_dia_diem">Mã địa điểm</label>
              <select class="form-control select2" style="width: 100%;" name="ma_dia_diem">
              <?php foreach ($diadiems as $diadiem) { ?>
                <option value="<?php echo $diadiem->MADIADIEM ?>"><strong><?php echo $diadiem->MADIADIEM ?></strong> - <?php echo $diadiem->TENDIADIEM ?></option>
                <?php } ?>
              </select>
            </div>

            <!--button-->
            <div class="row text-center">
              <input type="submit" class="btn btn-app bg-orange btn-flat" value="Thêm mới" name="them_moi" onclick="return Kiemtradulieu_sanbay()" />
              <a href="sanbay.php" class="btn btn-app">
                <i class="glyphicon glyphicon-remove"></i> Hủy
              </a>
            </div>
          </form>
        </div><!--body-->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>