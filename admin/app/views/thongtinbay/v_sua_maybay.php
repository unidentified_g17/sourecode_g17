  <div class="row trang-sua">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Sửa máy bay</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="form-group">
              <label for="ma_may_bay">Mã máy bay</label>
              <input type="text" class="form-control input-lg kiemtra" id="ma_may_bay" name="ma_may_bay" placeholder="Mã máy bay" value="<?php echo $maybay->MAMAYBAY ?>" readonly />
            </div>

            <div class="form-group">
              <label for="ma_may_bay">Tên máy bay</label>
              <input type="text" class="form-control kiemtra" id="ten_may_bay" name="ten_may_bay" placeholder="Tên máy bay" data_error="Nhập tên máy bay" maxlength="20" value="<?php echo $maybay->TENMAYBAY ?>" />
            </div>

            <div class="form-group">
              <label for="ma_may_bay">Số lượng ghế</label>
              <input type="number" class="form-control kiemtra" id="so_luong_ghe" name="so_luong_ghe" placeholder="Số lượng ghế" data_error="Nhập số lượng ghế" value="<?php echo $maybay->SOLUONGGHE ?>" />
            </div>


            <!--button-->
            <div class="row text-center">
              <input type="submit" class="btn btn-app bg-orange btn-flat" value="Cập nhật" name="cap_nhat" onclick="return Kiemtradulieu_maybay()" />
              <a href="maybay.php" class="btn btn-app">
                <i class="glyphicon glyphicon-remove"></i> Hủy
              </a>
            </div>
          </form>
        </div><!--body-->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
