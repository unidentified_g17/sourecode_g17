  <div class="row trang-sua">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Sửa địa điểm</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="form-group">
              <label for="ma_dia_diem">Mã địa điểm</label>
              <input type="text" class="form-control input-lg kiemtra" id="ma_dia_diem" name="ma_dia_diem" placeholder="Mã địa điểm" value="<?php echo $diadiem->MADIADIEM ?>" readonly />
            </div>

            <div class="form-group">
              <label for="ma_dia_diem">Tên địa điểm</label>
              <input type="text" class="form-control kiemtra" id="ten_dia_diem" name="ten_dia_diem" placeholder="Tên địa điểm" data_error="Nhập tên địa điểm" maxlength="20" value="<?php echo $diadiem->TENDIADIEM ?>" />
            </div>
            <!--button-->
            <div class="row text-center">
              <input type="submit" class="btn btn-app bg-orange btn-flat" value="Cập nhật" name="cap_nhat" onclick="return Kiemtradulieu_diadiem()" />
              <a href="diadiem.php" class="btn btn-app">
                <i class="glyphicon glyphicon-remove"></i> Hủy
              </a>
            </div>
          </form>
        </div><!--body-->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
