<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách sân bay <p class="badge"><?php echo count($sanbays) ?></h3>
    <div class="box-tools">
      <p><a href="them-sanbay.php" class="btn btn-block btn-warning btn-flat">Thêm mới <span class="glyphicon glyphicon-plus-sign"></span></a></p></div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-sanbay" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Mã sân bay</th>
            <th>Tên sân bay</th>
            <th>Địa điểm</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($sanbays as $sanbay)
          { ?>
          <tr>
            <td><?php echo $sanbay->MASANBAY ?></td>
            <td><?php echo $sanbay->TENSANBAY ?></td>
            <td><?php echo $sanbay->TENDIADIEM ?></td>
            <td>
              <a href="sua-sanbay.php?ma=<?php echo $sanbay->MASANBAY ?>" class="btn btn-default btn-flat btn-suaxoa">Sửa</a>
              <!-- <button ma_san_bay="<?php echo $sanbay->MASANBAY ?>" class="xoa-san-bay btn btn-danger btn-flat btn-suaxoa">Xóa</button> -->
            </td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>Mã sân bay</th>
            <th>Tên sân bay</th>
            <th>Địa điểm</th>
            <th>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->