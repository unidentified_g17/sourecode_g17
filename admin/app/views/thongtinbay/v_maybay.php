<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách máy bay <p class="badge"><?php echo count($maybays) ?></h3>
    <div class="box-tools">
      <p><a href="them-maybay.php" class="btn btn-block btn-warning btn-flat">Thêm mới <span class="glyphicon glyphicon-plus-sign"></span></a></p></div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-maybay" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Mã máy bay</th>
            <th>Tên máy bay</th>
            <th>Số lượng ghế</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($maybays as $maybay)
          { ?>
          <tr>
            <td><?php echo $maybay->MAMAYBAY ?></td>
            <td><?php echo $maybay->TENMAYBAY ?></td>
            <td><?php echo $maybay->SOLUONGGHE ?></td>
            <td>
              <a href="sua-maybay.php?ma=<?php echo $maybay->MAMAYBAY ?>" class="btn btn-default btn-flat btn-suaxoa">Sửa</a>
              <!-- <button ma_may_bay="<?php echo $maybay->MAMAYBAY ?>" class="xoa-may-bay btn btn-danger btn-flat btn-suaxoa">Xóa</button> -->
            </td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>Mã máy bay</th>
            <th>Tên máy bay</th>
            <th>Số lượng ghế</th>
            <th>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->