  <div class="row trang-sua">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Thêm máy bay</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body padding">
          <form method="post">
            <div class="form-group">
              <label for="ma_may_bay">Mã máy bay (5 ký tự)</label>
              <input type="text" class="form-control input-lg kiemtra" id="them_ma_may_bay" name="ma_may_bay" placeholder="Mã máy bay" data_error="Nhập mã máy bay" maxlength="5" value="<?php echo (isset($_POST["them_moi"]))?$ma_may_bay:"" ?>" >
            </div>

            <div class="form-group">
              <label for="ten_may_bay">Tên máy bay</label>
              <input type="text" class="form-control kiemtra" id="ten_may_bay" name="ten_may_bay" placeholder="Tên máy bay" data_error="Nhập tên máy bay" maxlength="50" value="<?php echo (isset($_POST["them_moi"]))?$ten_may_bay:"" ?>">
            </div>

            <div class="form-group">
              <label for="so_luong_ghe">Số lượng ghế</label>
              <input type="number" class="form-control kiemtra" id="so_luong_ghe" name="so_luong_ghe" placeholder="Số lượng ghế" data_error="Nhập số lượng ghế" value="<?php echo (isset($_POST["them_moi"]))?$so_luong_ghe:"" ?>">
            </div>

            <!--button-->
            <div class="row text-center">
              <input type="submit" class="btn btn-app bg-orange btn-flat" value="Thêm mới" name="them_moi" onclick="return Kiemtradulieu_maybay()" />
              <a href="maybay.php" class="btn btn-app">
                <i class="glyphicon glyphicon-remove"></i> Hủy
              </a>
            </div>
          </form>
        </div><!--body-->
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
