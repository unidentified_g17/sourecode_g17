<div class="box">
  <div class="box-header">
    <h3 class="box-title">Danh sách địa điểm <p class="badge"><?php echo count($diadiems) ?></h3>
    <div class="box-tools">
      <p><a href="them-diadiem.php" class="btn btn-block btn-warning btn-flat">Thêm mới <span class="glyphicon glyphicon-plus-sign"></span></a></p></div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table-diadiem" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Mã địa điểm</th>
            <th>Tên địa điểm</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($diadiems as $diadiem)
          { ?>
          <tr>
            <td><?php echo $diadiem->MADIADIEM ?></td>
            <td><?php echo $diadiem->TENDIADIEM ?></td>
            <td>
              <a href="sua-diadiem.php?ma=<?php echo $diadiem->MADIADIEM ?>" class="btn btn-default btn-flat btn-suaxoa">Sửa</a>
              <!-- <button ma_dia_diem="<?php echo $diadiem->MADIADIEM ?>" class="xoa-dia-diem btn btn-danger btn-flat btn-suaxoa">Xóa</button> -->
            </td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>Mã địa điểm</th>
            <th>Tên địa điểm</th>
            <th>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->