<?php 
$id = $_POST["id"];
include_once("app/models/m_donhang.php");
$m_donhang = new M_donhang();
$tt = $m_donhang->Doc_thongtinchuyenbay($id);
$cthds = $m_donhang->Doc_chitietdonhang($id);
?>

<!-- Button trigger modal -->
<!-- Modal content-->
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Thông tin đơn hàng <strong><?php echo $id ?> - Mã chuyến bay: <?php echo $tt->MACHUYENBAY ?></strong></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<strong>Tên khách hàng: </strong><?php echo $tt->TENKHACHHANG ?>
				<hr/>
				<strong>Sân bay đi: </strong><?php echo $tt->SANBAYDI ?><br/>
				<strong>Thời gian đi: </strong><?php echo $tt->THOIGIANBAY ?>
			</div>

			<div class="col-md-6">
				<strong>Số đt: </strong><?php echo $tt->PHONE ?>
				<hr/>
				<strong>Sân bay đến: </strong><?php echo $tt->SANBAYDEN ?><br/>
				<strong>Thời gian đến: </strong><?php echo $tt->THOIGIANDEN ?>
			</div>
		</div>
		<br/>
		<table id="table-donhang" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Họ tên hành khách</th>
					<th>Loại vé</th>
					<th>Mã vé</th>
					<th>Giá vé</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($cthds as $dh)
				{ ?>
				<tr>
					<td><?php echo $dh->HOTENHANHKHACH ?></td>
					<td>
						<?php
						if($dh->LOAIVE == 1)
							echo 'Người lớn';
						elseif($dh->LOAIVE == 2)
							echo 'Trẻ em';
						else echo 'Em bé';
						?>
					</td>
					<td><?php echo $dh->MAVE ?></td>
					<td><?php echo number_format($dh->GIAVE) ?> VNĐ</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
	</div>
</div>