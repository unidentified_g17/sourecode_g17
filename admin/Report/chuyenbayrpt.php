<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start();
?>
<?php include_once "phprptinc/ewrcfg10.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "phprptinc/ewmysql.php") ?>
<?php include_once "phprptinc/ewrfn10.php" ?>
<?php include_once "phprptinc/ewrusrfn10.php" ?>
<?php include_once "chuyenbayrptinfo.php" ?>
<?php

//
// Page class
//

$chuyenbay_rpt = NULL; // Initialize page object first

class crchuyenbay_rpt extends crchuyenbay {

	// Page ID
	var $PageID = 'rpt';

	// Project ID
	var $ProjectID = "{3fff1c9b-373a-4b74-aaf0-daf5da7ce23b}";

	// Page object name
	var $PageObjName = 'chuyenbay_rpt';

	// Page name
	function PageName() {
		return ewr_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ewr_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Export URLs
	var $ExportPrintUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportPdfUrl;
	var $ReportTableClass;
	var $ReportTableStyle = "";

	// Custom export
	var $ExportPrintCustom = FALSE;
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EWR_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ewr_AddMessage($_SESSION[EWR_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EWR_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ewr_AddMessage($_SESSION[EWR_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EWR_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ewr_AddMessage($_SESSION[EWR_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EWR_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ewr_AddMessage($_SESSION[EWR_SESSION_WARNING_MESSAGE], $v);
	}

		// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EWR_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EWR_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EWR_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EWR_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog ewDisplayTable\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") // Header exists, display
			echo $sHeader;
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") // Fotoer exists, display
			echo $sFooter;
	}

	// Validate page request
	function IsPageRequest() {
		if ($this->UseTokenInUrl) {
			if (ewr_IsHttpPost())
				return ($this->TableVar == @$_POST("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == @$_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $CheckToken = EWR_CHECK_TOKEN;
	var $CheckTokenFn = "ewr_CheckToken";
	var $CreateTokenFn = "ewr_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ewr_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EWR_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EWR_TOKEN_NAME]);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $ReportLanguage;

		// Language object
		$ReportLanguage = new crLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (chuyenbay)
		if (!isset($GLOBALS["chuyenbay"])) {
			$GLOBALS["chuyenbay"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["chuyenbay"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";

		// Page ID
		if (!defined("EWR_PAGE_ID"))
			define("EWR_PAGE_ID", 'rpt', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EWR_TABLE_NAME"))
			define("EWR_TABLE_NAME", 'chuyenbay', TRUE);

		// Start timer
		$GLOBALS["gsTimer"] = new crTimer();

		// Open connection
		if (!isset($conn)) $conn = ewr_Connect($this->DBID);

		// Export options
		$this->ExportOptions = new crListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Search options
		$this->SearchOptions = new crListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Filter options
		$this->FilterOptions = new crListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fchuyenbayrpt";

		// Generate report options
		$this->GenerateOptions = new crListOptions();
		$this->GenerateOptions->Tag = "div";
		$this->GenerateOptions->TagClassName = "ewGenerateOption";
	}

	//
	// Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $gsEmailContentType, $ReportLanguage, $Security;
		global $gsCustomExport;

		// Get export parameters
		if (@$_GET["export"] <> "")
			$this->Export = strtolower($_GET["export"]);
		elseif (@$_POST["export"] <> "")
			$this->Export = strtolower($_POST["export"]);
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header
		$gsEmailContentType = @$_POST["contenttype"]; // Get email content type

		// Setup placeholder
		// Setup export options

		$this->SetupExportOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $ReportLanguage->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Security, $ReportLanguage, $ReportOptions;
		$exportid = session_id();
		$ReportTypes = array();

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a title=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("PrinterFriendly", TRUE)) . "\" data-caption=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("PrinterFriendly", TRUE)) . "\" href=\"" . $this->ExportPrintUrl . "\">" . $ReportLanguage->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;
		$ReportTypes["print"] = $item->Visible ? $ReportLanguage->Phrase("ReportFormPrint") : "";

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a title=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToExcel", TRUE)) . "\" data-caption=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToExcel", TRUE)) . "\" href=\"" . $this->ExportExcelUrl . "\">" . $ReportLanguage->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;
		$ReportTypes["excel"] = $item->Visible ? $ReportLanguage->Phrase("ReportFormExcel") : "";

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a title=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToWord", TRUE)) . "\" data-caption=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToWord", TRUE)) . "\" href=\"" . $this->ExportWordUrl . "\">" . $ReportLanguage->Phrase("ExportToWord") . "</a>";

		//$item->Visible = FALSE;
		$item->Visible = FALSE;
		$ReportTypes["word"] = $item->Visible ? $ReportLanguage->Phrase("ReportFormWord") : "";

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a title=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToPDF", TRUE)) . "\" data-caption=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToPDF", TRUE)) . "\" href=\"" . $this->ExportPdfUrl . "\">" . $ReportLanguage->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Uncomment codes below to show export to Pdf link
//		$item->Visible = FALSE;

		$ReportTypes["pdf"] = $item->Visible ? $ReportLanguage->Phrase("ReportFormPdf") : "";

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = $this->PageUrl() . "export=email";
		$item->Body = "<a title=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToEmail", TRUE)) . "\" data-caption=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ExportToEmail", TRUE)) . "\" id=\"emf_chuyenbay\" href=\"javascript:void(0);\" onclick=\"ewr_EmailDialogShow({lnk:'emf_chuyenbay',hdr:ewLanguage.Phrase('ExportToEmail'),url:'$url',exportid:'$exportid',el:this});\">" . $ReportLanguage->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;
		$ReportTypes["email"] = $item->Visible ? $ReportLanguage->Phrase("ReportFormEmail") : "";
		$ReportOptions["ReportTypes"] = $ReportTypes;

		// Drop down button for export
		$this->ExportOptions->UseDropDownButton = FALSE;
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = $this->ExportOptions->UseDropDownButton;
		$this->ExportOptions->DropDownButtonPhrase = $ReportLanguage->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fchuyenbayrpt\" href=\"#\">" . $ReportLanguage->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fchuyenbayrpt\" href=\"#\">" . $ReportLanguage->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton; // v8
		$this->FilterOptions->DropDownButtonPhrase = $ReportLanguage->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Set up options (extended)
		$this->SetupExportOptionsExt();

		// Hide options for export
		if ($this->Export <> "") {
			$this->ExportOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}

		// Set up table class
		if ($this->Export == "word" || $this->Export == "excel" || $this->Export == "pdf")
			$this->ReportTableClass = "ewTable";
		else
			$this->ReportTableClass = "table ewTable";
	}

	// Set up search options
	function SetupSearchOptions() {
		global $ReportLanguage;

		// Filter panel button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = $this->FilterApplied ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $ReportLanguage->Phrase("SearchBtn", TRUE) . "\" data-caption=\"" . $ReportLanguage->Phrase("SearchBtn", TRUE) . "\" data-toggle=\"button\" data-form=\"fchuyenbayrpt\">" . $ReportLanguage->Phrase("SearchBtn") . "</button>";
		$item->Visible = FALSE;

		// Reset filter
		$item = &$this->SearchOptions->Add("resetfilter");
		$item->Body = "<button type=\"button\" class=\"btn btn-default\" title=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ResetAllFilter", TRUE)) . "\" data-caption=\"" . ewr_HtmlEncode($ReportLanguage->Phrase("ResetAllFilter", TRUE)) . "\" onclick=\"location='" . ewr_CurrentPage() . "?cmd=reset'\">" . $ReportLanguage->Phrase("ResetAllFilter") . "</button>";
		$item->Visible = FALSE && $this->FilterApplied;

		// Button group for reset filter
		$this->SearchOptions->UseButtonGroup = TRUE;

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->SearchOptions->HideAllOptions();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $ReportLanguage, $EWR_EXPORT, $gsExportFile;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		if ($this->Export <> "" && array_key_exists($this->Export, $EWR_EXPORT)) {
			$sContent = ob_get_contents();
			if (ob_get_length())
				ob_end_clean();

			// Remove all <div data-tagid="..." id="orig..." class="hide">...</div> (for customviewtag export, except "googlemaps")
			if (preg_match_all('/<div\s+data-tagid=[\'"]([\s\S]*?)[\'"]\s+id=[\'"]orig([\s\S]*?)[\'"]\s+class\s*=\s*[\'"]hide[\'"]>([\s\S]*?)<\/div\s*>/i', $sContent, $divmatches, PREG_SET_ORDER)) {
				foreach ($divmatches as $divmatch) {
					if ($divmatch[1] <> "googlemaps")
						$sContent = str_replace($divmatch[0], '', $sContent);
				}
			}
			$fn = $EWR_EXPORT[$this->Export];
			if ($this->Export == "email") { // Email
				if (@$this->GenOptions["reporttype"] == "email") {
					$saveResponse = $this->$fn($sContent, $this->GenOptions);
					$this->WriteGenResponse($saveResponse);
				} else {
					echo $this->$fn($sContent, array());
				}
				$url = ""; // Avoid redirect
			} else {
				$saveToFile = $this->$fn($sContent, $this->GenOptions);
				if (@$this->GenOptions["reporttype"] <> "") {
					$saveUrl = ($saveToFile <> "") ? ewr_ConvertFullUrl($saveToFile) : $ReportLanguage->Phrase("GenerateSuccess");
					$this->WriteGenResponse($saveUrl);
					$url = ""; // Avoid redirect
				}
			}
		}

		 // Close connection
		ewr_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EWR_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Initialize common variables
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $FilterOptions; // Filter options

	// Paging variables
	var $RecIndex = 0; // Record index
	var $RecCount = 0; // Record count
	var $StartGrp = 0; // Start group
	var $StopGrp = 0; // Stop group
	var $TotalGrps = 0; // Total groups
	var $GrpCount = 0; // Group count
	var $GrpCounter = array(); // Group counter
	var $DisplayGrps = 3; // Groups per page
	var $GrpRange = 10;
	var $Sort = "";
	var $Filter = "";
	var $PageFirstGroupFilter = "";
	var $UserIDFilter = "";
	var $DrillDown = FALSE;
	var $DrillDownInPanel = FALSE;
	var $DrillDownList = "";

	// Clear field for ext filter
	var $ClearExtFilter = "";
	var $PopupName = "";
	var $PopupValue = "";
	var $FilterApplied;
	var $SearchCommand = FALSE;
	var $ShowHeader;
	var $GrpColumnCount = 0;
	var $SubGrpColumnCount = 0;
	var $DtlColumnCount = 0;
	var $Cnt, $Col, $Val, $Smry, $Mn, $Mx, $GrandCnt, $GrandSmry, $GrandMn, $GrandMx;
	var $TotCount;
	var $GrandSummarySetup = FALSE;
	var $GrpIdx;
	var $DetailRows = array();

	//
	// Page main
	//
	function Page_Main() {
		global $rs;
		global $rsgrp;
		global $Security;
		global $gsFormError;
		global $gbDrillDownInPanel;
		global $ReportBreadcrumb;
		global $ReportLanguage;

		// Set field visibility for detail fields
		$this->MACHUYENBAY->SetVisibility();
		$this->MAMAYBAY->SetVisibility();
		$this->SANBAYDI->SetVisibility();
		$this->SANBAYDEN->SetVisibility();
		$this->GIAVEGOC->SetVisibility();
		$this->THOIGIANBAY->SetVisibility();
		$this->THOIGIANDEN->SetVisibility();
		$this->TRANSITION->SetVisibility();

		// Aggregate variables
		// 1st dimension = no of groups (level 0 used for grand total)
		// 2nd dimension = no of fields

		$nDtls = 9;
		$nGrps = 1;
		$this->Val = &ewr_InitArray($nDtls, 0);
		$this->Cnt = &ewr_Init2DArray($nGrps, $nDtls, 0);
		$this->Smry = &ewr_Init2DArray($nGrps, $nDtls, 0);
		$this->Mn = &ewr_Init2DArray($nGrps, $nDtls, NULL);
		$this->Mx = &ewr_Init2DArray($nGrps, $nDtls, NULL);
		$this->GrandCnt = &ewr_InitArray($nDtls, 0);
		$this->GrandSmry = &ewr_InitArray($nDtls, 0);
		$this->GrandMn = &ewr_InitArray($nDtls, NULL);
		$this->GrandMx = &ewr_InitArray($nDtls, NULL);

		// Set up array if accumulation required: array(Accum, SkipNullOrZero)
		$this->Col = array(array(FALSE, FALSE), array(FALSE,FALSE), array(FALSE,FALSE), array(FALSE,FALSE), array(FALSE,FALSE), array(FALSE,FALSE), array(FALSE,FALSE), array(FALSE,FALSE), array(FALSE,FALSE));

		// Set up groups per page dynamically
		$this->SetUpDisplayGrps();

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();

		// Load custom filters
		$this->Page_FilterLoad();

		// Set up popup filter
		$this->SetupPopup();

		// Load group db values if necessary
		$this->LoadGroupDbValues();

		// Handle Ajax popup
		$this->ProcessAjaxPopup();

		// Extended filter
		$sExtendedFilter = "";

		// Build popup filter
		$sPopupFilter = $this->GetPopupFilter();

		//ewr_SetDebugMsg("popup filter: " . $sPopupFilter);
		ewr_AddFilter($this->Filter, $sPopupFilter);

		// No filter
		$this->FilterApplied = FALSE;
		$this->FilterOptions->GetItem("savecurrentfilter")->Visible = FALSE;
		$this->FilterOptions->GetItem("deletefilter")->Visible = FALSE;

		// Call Page Selecting event
		$this->Page_Selecting($this->Filter);

		// Search options
		$this->SetupSearchOptions();

		// Get sort
		$this->Sort = $this->GetSort($this->GenOptions);

		// Get total count
		$sSql = ewr_BuildReportSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(), $this->Filter, $this->Sort);
		$this->TotalGrps = $this->GetCnt($sSql);
		if ($this->DisplayGrps <= 0 || $this->DrillDown) // Display all groups
			$this->DisplayGrps = $this->TotalGrps;
		$this->StartGrp = 1;

		// Show header
		$this->ShowHeader = ($this->TotalGrps > 0);

		// Set up start position if not export all
		if ($this->ExportAll && $this->Export <> "")
			$this->DisplayGrps = $this->TotalGrps;
		else
			$this->SetUpStartGroup($this->GenOptions);

		// Set no record found message
		if ($this->TotalGrps == 0) {
				if ($this->Filter == "0=101") {
					$this->setWarningMessage($ReportLanguage->Phrase("EnterSearchCriteria"));
				} else {
					$this->setWarningMessage($ReportLanguage->Phrase("NoRecord"));
				}
		}

		// Hide export options if export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();

		// Hide search/filter options if export/drilldown
		if ($this->Export <> "" || $this->DrillDown) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
			$this->GenerateOptions->HideAllOptions();
		}

		// Get current page records
		$rs = $this->GetRs($sSql, $this->StartGrp, $this->DisplayGrps);
		$this->SetupFieldCount();
	}

	// Accummulate summary
	function AccumulateSummary() {
		$cntx = count($this->Smry);
		for ($ix = 0; $ix < $cntx; $ix++) {
			$cnty = count($this->Smry[$ix]);
			for ($iy = 1; $iy < $cnty; $iy++) {
				if ($this->Col[$iy][0]) { // Accumulate required
					$valwrk = $this->Val[$iy];
					if (is_null($valwrk)) {
						if (!$this->Col[$iy][1])
							$this->Cnt[$ix][$iy]++;
					} else {
						$accum = (!$this->Col[$iy][1] || !is_numeric($valwrk) || $valwrk <> 0);
						if ($accum) {
							$this->Cnt[$ix][$iy]++;
							if (is_numeric($valwrk)) {
								$this->Smry[$ix][$iy] += $valwrk;
								if (is_null($this->Mn[$ix][$iy])) {
									$this->Mn[$ix][$iy] = $valwrk;
									$this->Mx[$ix][$iy] = $valwrk;
								} else {
									if ($this->Mn[$ix][$iy] > $valwrk) $this->Mn[$ix][$iy] = $valwrk;
									if ($this->Mx[$ix][$iy] < $valwrk) $this->Mx[$ix][$iy] = $valwrk;
								}
							}
						}
					}
				}
			}
		}
		$cntx = count($this->Smry);
		for ($ix = 0; $ix < $cntx; $ix++) {
			$this->Cnt[$ix][0]++;
		}
	}

	// Reset level summary
	function ResetLevelSummary($lvl) {

		// Clear summary values
		$cntx = count($this->Smry);
		for ($ix = $lvl; $ix < $cntx; $ix++) {
			$cnty = count($this->Smry[$ix]);
			for ($iy = 1; $iy < $cnty; $iy++) {
				$this->Cnt[$ix][$iy] = 0;
				if ($this->Col[$iy][0]) {
					$this->Smry[$ix][$iy] = 0;
					$this->Mn[$ix][$iy] = NULL;
					$this->Mx[$ix][$iy] = NULL;
				}
			}
		}
		$cntx = count($this->Smry);
		for ($ix = $lvl; $ix < $cntx; $ix++) {
			$this->Cnt[$ix][0] = 0;
		}

		// Reset record count
		$this->RecCount = 0;
	}

	// Accummulate grand summary
	function AccumulateGrandSummary() {
		$this->TotCount++;
		$cntgs = count($this->GrandSmry);
		for ($iy = 1; $iy < $cntgs; $iy++) {
			if ($this->Col[$iy][0]) {
				$valwrk = $this->Val[$iy];
				if (is_null($valwrk) || !is_numeric($valwrk)) {
					if (!$this->Col[$iy][1])
						$this->GrandCnt[$iy]++;
				} else {
					if (!$this->Col[$iy][1] || $valwrk <> 0) {
						$this->GrandCnt[$iy]++;
						$this->GrandSmry[$iy] += $valwrk;
						if (is_null($this->GrandMn[$iy])) {
							$this->GrandMn[$iy] = $valwrk;
							$this->GrandMx[$iy] = $valwrk;
						} else {
							if ($this->GrandMn[$iy] > $valwrk) $this->GrandMn[$iy] = $valwrk;
							if ($this->GrandMx[$iy] < $valwrk) $this->GrandMx[$iy] = $valwrk;
						}
					}
				}
			}
		}
	}

	// Get count
	function GetCnt($sql) {
		$conn = &$this->Connection();
		$rscnt = $conn->Execute($sql);
		$cnt = ($rscnt) ? $rscnt->RecordCount() : 0;
		if ($rscnt) $rscnt->Close();
		return $cnt;
	}

	// Get recordset
	function GetRs($wrksql, $start, $grps) {
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EWR_ERROR_FN"];
		$rswrk = $conn->SelectLimit($wrksql, $grps, $start - 1);
		$conn->raiseErrorFn = '';
		return $rswrk;
	}

	// Get row values
	function GetRow($opt) {
		global $rs;
		if (!$rs)
			return;
		if ($opt == 1) { // Get first row
			$rs->MoveFirst(); // Move first
				$this->FirstRowData = array();
				$this->FirstRowData['MACHUYENBAY'] = ewr_Conv($rs->fields('MACHUYENBAY'), 200);
				$this->FirstRowData['MAMAYBAY'] = ewr_Conv($rs->fields('MAMAYBAY'), 200);
				$this->FirstRowData['SANBAYDI'] = ewr_Conv($rs->fields('SANBAYDI'), 200);
				$this->FirstRowData['SANBAYDEN'] = ewr_Conv($rs->fields('SANBAYDEN'), 200);
				$this->FirstRowData['GIAVEGOC'] = ewr_Conv($rs->fields('GIAVEGOC'), 3);
				$this->FirstRowData['THOIGIANBAY'] = ewr_Conv($rs->fields('THOIGIANBAY'), 135);
				$this->FirstRowData['THOIGIANDEN'] = ewr_Conv($rs->fields('THOIGIANDEN'), 135);
				$this->FirstRowData['TRANSITION'] = ewr_Conv($rs->fields('TRANSITION'), 200);
		} else { // Get next row
			$rs->MoveNext();
		}
		if (!$rs->EOF) {
			$this->MACHUYENBAY->setDbValue($rs->fields('MACHUYENBAY'));
			$this->MAMAYBAY->setDbValue($rs->fields('MAMAYBAY'));
			$this->SANBAYDI->setDbValue($rs->fields('SANBAYDI'));
			$this->SANBAYDEN->setDbValue($rs->fields('SANBAYDEN'));
			$this->GIAVEGOC->setDbValue($rs->fields('GIAVEGOC'));
			$this->THOIGIANBAY->setDbValue($rs->fields('THOIGIANBAY'));
			$this->THOIGIANDEN->setDbValue($rs->fields('THOIGIANDEN'));
			$this->TRANSITION->setDbValue($rs->fields('TRANSITION'));
			$this->Val[1] = $this->MACHUYENBAY->CurrentValue;
			$this->Val[2] = $this->MAMAYBAY->CurrentValue;
			$this->Val[3] = $this->SANBAYDI->CurrentValue;
			$this->Val[4] = $this->SANBAYDEN->CurrentValue;
			$this->Val[5] = $this->GIAVEGOC->CurrentValue;
			$this->Val[6] = $this->THOIGIANBAY->CurrentValue;
			$this->Val[7] = $this->THOIGIANDEN->CurrentValue;
			$this->Val[8] = $this->TRANSITION->CurrentValue;
		} else {
			$this->MACHUYENBAY->setDbValue("");
			$this->MAMAYBAY->setDbValue("");
			$this->SANBAYDI->setDbValue("");
			$this->SANBAYDEN->setDbValue("");
			$this->GIAVEGOC->setDbValue("");
			$this->THOIGIANBAY->setDbValue("");
			$this->THOIGIANDEN->setDbValue("");
			$this->TRANSITION->setDbValue("");
		}
	}

	// Set up starting group
	function SetUpStartGroup($options = array()) {

		// Exit if no groups
		if ($this->DisplayGrps == 0)
			return;
		$startGrp = (@$options["start"] <> "") ? $options["start"] : @$_GET[EWR_TABLE_START_GROUP];
		$pageNo = (@$options["pageno"] <> "") ? $options["pageno"] : @$_GET["pageno"];

		// Check for a 'start' parameter
		if ($startGrp != "") {
			$this->StartGrp = $startGrp;
			$this->setStartGroup($this->StartGrp);
		} elseif ($pageNo != "") {
			$nPageNo = $pageNo;
			if (is_numeric($nPageNo)) {
				$this->StartGrp = ($nPageNo-1)*$this->DisplayGrps+1;
				if ($this->StartGrp <= 0) {
					$this->StartGrp = 1;
				} elseif ($this->StartGrp >= intval(($this->TotalGrps-1)/$this->DisplayGrps)*$this->DisplayGrps+1) {
					$this->StartGrp = intval(($this->TotalGrps-1)/$this->DisplayGrps)*$this->DisplayGrps+1;
				}
				$this->setStartGroup($this->StartGrp);
			} else {
				$this->StartGrp = $this->getStartGroup();
			}
		} else {
			$this->StartGrp = $this->getStartGroup();
		}

		// Check if correct start group counter
		if (!is_numeric($this->StartGrp) || $this->StartGrp == "") { // Avoid invalid start group counter
			$this->StartGrp = 1; // Reset start group counter
			$this->setStartGroup($this->StartGrp);
		} elseif (intval($this->StartGrp) > intval($this->TotalGrps)) { // Avoid starting group > total groups
			$this->StartGrp = intval(($this->TotalGrps-1)/$this->DisplayGrps) * $this->DisplayGrps + 1; // Point to last page first group
			$this->setStartGroup($this->StartGrp);
		} elseif (($this->StartGrp-1) % $this->DisplayGrps <> 0) {
			$this->StartGrp = intval(($this->StartGrp-1)/$this->DisplayGrps) * $this->DisplayGrps + 1; // Point to page boundary
			$this->setStartGroup($this->StartGrp);
		}
	}

	// Load group db values if necessary
	function LoadGroupDbValues() {
		$conn = &$this->Connection();
	}

	// Process Ajax popup
	function ProcessAjaxPopup() {
		global $ReportLanguage;
		$conn = &$this->Connection();
		$fld = NULL;
		if (@$_GET["popup"] <> "") {
			$popupname = $_GET["popup"];

			// Check popup name
			// Output data as Json

			if (!is_null($fld)) {
				$jsdb = ewr_GetJsDb($fld, $fld->FldType);
				if (ob_get_length())
					ob_end_clean();
				echo $jsdb;
				exit();
			}
		}
	}

	// Set up popup
	function SetupPopup() {
		global $ReportLanguage;
		$conn = &$this->Connection();
		if ($this->DrillDown)
			return;

		// Process post back form
		if (ewr_IsHttpPost()) {
			$sName = @$_POST["popup"]; // Get popup form name
			if ($sName <> "") {
				$cntValues = (is_array(@$_POST["sel_$sName"])) ? count($_POST["sel_$sName"]) : 0;
				if ($cntValues > 0) {
					$arValues = ewr_StripSlashes($_POST["sel_$sName"]);
					if (trim($arValues[0]) == "") // Select all
						$arValues = EWR_INIT_VALUE;
					$_SESSION["sel_$sName"] = $arValues;
					$_SESSION["rf_$sName"] = ewr_StripSlashes(@$_POST["rf_$sName"]);
					$_SESSION["rt_$sName"] = ewr_StripSlashes(@$_POST["rt_$sName"]);
					$this->ResetPager();
				}
			}

		// Get 'reset' command
		} elseif (@$_GET["cmd"] <> "") {
			$sCmd = $_GET["cmd"];
			if (strtolower($sCmd) == "reset") {
				$this->ResetPager();
			}
		}

		// Load selection criteria to array
	}

	// Reset pager
	function ResetPager() {

		// Reset start position (reset command)
		$this->StartGrp = 1;
		$this->setStartGroup($this->StartGrp);
	}

	// Set up number of groups displayed per page
	function SetUpDisplayGrps() {
		$sWrk = @$_GET[EWR_TABLE_GROUP_PER_PAGE];
		if ($sWrk <> "") {
			if (is_numeric($sWrk)) {
				$this->DisplayGrps = intval($sWrk);
			} else {
				if (strtoupper($sWrk) == "ALL") { // Display all groups
					$this->DisplayGrps = -1;
				} else {
					$this->DisplayGrps = 3; // Non-numeric, load default
				}
			}
			$this->setGroupPerPage($this->DisplayGrps); // Save to session

			// Reset start position (reset command)
			$this->StartGrp = 1;
			$this->setStartGroup($this->StartGrp);
		} else {
			if ($this->getGroupPerPage() <> "") {
				$this->DisplayGrps = $this->getGroupPerPage(); // Restore from session
			} else {
				$this->DisplayGrps = 3; // Load default
			}
		}
	}

	// Render row
	function RenderRow() {
		global $rs, $Security, $ReportLanguage;
		$conn = &$this->Connection();
		if (!$this->GrandSummarySetup) { // Get Grand total
			$bGotCount = FALSE;
			$bGotSummary = FALSE;

			// Get total count from sql directly
			$sSql = ewr_BuildReportSql($this->getSqlSelectCount(), $this->getSqlWhere(), $this->getSqlGroupBy(), $this->getSqlHaving(), "", $this->Filter, "");
			$rstot = $conn->Execute($sSql);
			if ($rstot) {
				$this->TotCount = ($rstot->RecordCount()>1) ? $rstot->RecordCount() : $rstot->fields[0];
				$rstot->Close();
				$bGotCount = TRUE;
			} else {
				$this->TotCount = 0;
			}
		$bGotSummary = TRUE;

			// Accumulate grand summary from detail records
			if (!$bGotCount || !$bGotSummary) {
				$sSql = ewr_BuildReportSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(), $this->getSqlHaving(), "", $this->Filter, "");
				$rs = $conn->Execute($sSql);
				if ($rs) {
					$this->GetRow(1);
					while (!$rs->EOF) {
						$this->AccumulateGrandSummary();
						$this->GetRow(2);
					}
					$rs->Close();
				}
			}
			$this->GrandSummarySetup = TRUE; // No need to set up again
		}

		// Call Row_Rendering event
		$this->Row_Rendering();

		//
		// Render view codes
		//

		if ($this->RowType == EWR_ROWTYPE_TOTAL && !($this->RowTotalType == EWR_ROWTOTAL_GROUP && $this->RowTotalSubType == EWR_ROWTOTAL_HEADER)) { // Summary row
			ewr_PrependClass($this->RowAttrs["class"], ($this->RowTotalType == EWR_ROWTOTAL_PAGE || $this->RowTotalType == EWR_ROWTOTAL_GRAND) ? "ewRptGrpAggregate" : "ewRptGrpSummary" . $this->RowGroupLevel); // Set up row class

			// MACHUYENBAY
			$this->MACHUYENBAY->HrefValue = "";

			// MAMAYBAY
			$this->MAMAYBAY->HrefValue = "";

			// SANBAYDI
			$this->SANBAYDI->HrefValue = "";

			// SANBAYDEN
			$this->SANBAYDEN->HrefValue = "";

			// GIAVEGOC
			$this->GIAVEGOC->HrefValue = "";

			// THOIGIANBAY
			$this->THOIGIANBAY->HrefValue = "";

			// THOIGIANDEN
			$this->THOIGIANDEN->HrefValue = "";

			// TRANSITION
			$this->TRANSITION->HrefValue = "";
		} else {
			if ($this->RowTotalType == EWR_ROWTOTAL_GROUP && $this->RowTotalSubType == EWR_ROWTOTAL_HEADER) {
			} else {
			}

			// MACHUYENBAY
			$this->MACHUYENBAY->ViewValue = $this->MACHUYENBAY->CurrentValue;
			$this->MACHUYENBAY->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// MAMAYBAY
			$this->MAMAYBAY->ViewValue = $this->MAMAYBAY->CurrentValue;
			$this->MAMAYBAY->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// SANBAYDI
			$this->SANBAYDI->ViewValue = $this->SANBAYDI->CurrentValue;
			$this->SANBAYDI->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// SANBAYDEN
			$this->SANBAYDEN->ViewValue = $this->SANBAYDEN->CurrentValue;
			$this->SANBAYDEN->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// GIAVEGOC
			$this->GIAVEGOC->ViewValue = $this->GIAVEGOC->CurrentValue;
			$this->GIAVEGOC->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// THOIGIANBAY
			$this->THOIGIANBAY->ViewValue = $this->THOIGIANBAY->CurrentValue;
			$this->THOIGIANBAY->ViewValue = ewr_FormatDateTime($this->THOIGIANBAY->ViewValue, 0);
			$this->THOIGIANBAY->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// THOIGIANDEN
			$this->THOIGIANDEN->ViewValue = $this->THOIGIANDEN->CurrentValue;
			$this->THOIGIANDEN->ViewValue = ewr_FormatDateTime($this->THOIGIANDEN->ViewValue, 0);
			$this->THOIGIANDEN->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// TRANSITION
			$this->TRANSITION->ViewValue = $this->TRANSITION->CurrentValue;
			$this->TRANSITION->CellAttrs["class"] = ($this->RecCount % 2 <> 1) ? "ewTableAltRow" : "ewTableRow";

			// MACHUYENBAY
			$this->MACHUYENBAY->HrefValue = "";

			// MAMAYBAY
			$this->MAMAYBAY->HrefValue = "";

			// SANBAYDI
			$this->SANBAYDI->HrefValue = "";

			// SANBAYDEN
			$this->SANBAYDEN->HrefValue = "";

			// GIAVEGOC
			$this->GIAVEGOC->HrefValue = "";

			// THOIGIANBAY
			$this->THOIGIANBAY->HrefValue = "";

			// THOIGIANDEN
			$this->THOIGIANDEN->HrefValue = "";

			// TRANSITION
			$this->TRANSITION->HrefValue = "";
		}

		// Call Cell_Rendered event
		if ($this->RowType == EWR_ROWTYPE_TOTAL) { // Summary row
		} else {

			// MACHUYENBAY
			$CurrentValue = $this->MACHUYENBAY->CurrentValue;
			$ViewValue = &$this->MACHUYENBAY->ViewValue;
			$ViewAttrs = &$this->MACHUYENBAY->ViewAttrs;
			$CellAttrs = &$this->MACHUYENBAY->CellAttrs;
			$HrefValue = &$this->MACHUYENBAY->HrefValue;
			$LinkAttrs = &$this->MACHUYENBAY->LinkAttrs;
			$this->Cell_Rendered($this->MACHUYENBAY, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// MAMAYBAY
			$CurrentValue = $this->MAMAYBAY->CurrentValue;
			$ViewValue = &$this->MAMAYBAY->ViewValue;
			$ViewAttrs = &$this->MAMAYBAY->ViewAttrs;
			$CellAttrs = &$this->MAMAYBAY->CellAttrs;
			$HrefValue = &$this->MAMAYBAY->HrefValue;
			$LinkAttrs = &$this->MAMAYBAY->LinkAttrs;
			$this->Cell_Rendered($this->MAMAYBAY, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// SANBAYDI
			$CurrentValue = $this->SANBAYDI->CurrentValue;
			$ViewValue = &$this->SANBAYDI->ViewValue;
			$ViewAttrs = &$this->SANBAYDI->ViewAttrs;
			$CellAttrs = &$this->SANBAYDI->CellAttrs;
			$HrefValue = &$this->SANBAYDI->HrefValue;
			$LinkAttrs = &$this->SANBAYDI->LinkAttrs;
			$this->Cell_Rendered($this->SANBAYDI, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// SANBAYDEN
			$CurrentValue = $this->SANBAYDEN->CurrentValue;
			$ViewValue = &$this->SANBAYDEN->ViewValue;
			$ViewAttrs = &$this->SANBAYDEN->ViewAttrs;
			$CellAttrs = &$this->SANBAYDEN->CellAttrs;
			$HrefValue = &$this->SANBAYDEN->HrefValue;
			$LinkAttrs = &$this->SANBAYDEN->LinkAttrs;
			$this->Cell_Rendered($this->SANBAYDEN, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// GIAVEGOC
			$CurrentValue = $this->GIAVEGOC->CurrentValue;
			$ViewValue = &$this->GIAVEGOC->ViewValue;
			$ViewAttrs = &$this->GIAVEGOC->ViewAttrs;
			$CellAttrs = &$this->GIAVEGOC->CellAttrs;
			$HrefValue = &$this->GIAVEGOC->HrefValue;
			$LinkAttrs = &$this->GIAVEGOC->LinkAttrs;
			$this->Cell_Rendered($this->GIAVEGOC, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// THOIGIANBAY
			$CurrentValue = $this->THOIGIANBAY->CurrentValue;
			$ViewValue = &$this->THOIGIANBAY->ViewValue;
			$ViewAttrs = &$this->THOIGIANBAY->ViewAttrs;
			$CellAttrs = &$this->THOIGIANBAY->CellAttrs;
			$HrefValue = &$this->THOIGIANBAY->HrefValue;
			$LinkAttrs = &$this->THOIGIANBAY->LinkAttrs;
			$this->Cell_Rendered($this->THOIGIANBAY, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// THOIGIANDEN
			$CurrentValue = $this->THOIGIANDEN->CurrentValue;
			$ViewValue = &$this->THOIGIANDEN->ViewValue;
			$ViewAttrs = &$this->THOIGIANDEN->ViewAttrs;
			$CellAttrs = &$this->THOIGIANDEN->CellAttrs;
			$HrefValue = &$this->THOIGIANDEN->HrefValue;
			$LinkAttrs = &$this->THOIGIANDEN->LinkAttrs;
			$this->Cell_Rendered($this->THOIGIANDEN, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);

			// TRANSITION
			$CurrentValue = $this->TRANSITION->CurrentValue;
			$ViewValue = &$this->TRANSITION->ViewValue;
			$ViewAttrs = &$this->TRANSITION->ViewAttrs;
			$CellAttrs = &$this->TRANSITION->CellAttrs;
			$HrefValue = &$this->TRANSITION->HrefValue;
			$LinkAttrs = &$this->TRANSITION->LinkAttrs;
			$this->Cell_Rendered($this->TRANSITION, $CurrentValue, $ViewValue, $ViewAttrs, $CellAttrs, $HrefValue, $LinkAttrs);
		}

		// Call Row_Rendered event
		$this->Row_Rendered();
		$this->SetupFieldCount();
	}

	// Setup field count
	function SetupFieldCount() {
		$this->GrpColumnCount = 0;
		$this->SubGrpColumnCount = 0;
		$this->DtlColumnCount = 0;
		if ($this->MACHUYENBAY->Visible) $this->DtlColumnCount += 1;
		if ($this->MAMAYBAY->Visible) $this->DtlColumnCount += 1;
		if ($this->SANBAYDI->Visible) $this->DtlColumnCount += 1;
		if ($this->SANBAYDEN->Visible) $this->DtlColumnCount += 1;
		if ($this->GIAVEGOC->Visible) $this->DtlColumnCount += 1;
		if ($this->THOIGIANBAY->Visible) $this->DtlColumnCount += 1;
		if ($this->THOIGIANDEN->Visible) $this->DtlColumnCount += 1;
		if ($this->TRANSITION->Visible) $this->DtlColumnCount += 1;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $ReportBreadcrumb;
		$ReportBreadcrumb = new crBreadcrumb();
		$url = substr(ewr_CurrentUrl(), strrpos(ewr_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$ReportBreadcrumb->Add("rpt", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	function SetupExportOptionsExt() {
		global $ReportLanguage, $ReportOptions;
		$ReportTypes = $ReportOptions["ReportTypes"];
		$ReportOptions["ReportTypes"] = $ReportTypes;
	}

	// Return popup filter
	function GetPopupFilter() {
		$sWrk = "";
		if ($this->DrillDown)
			return "";
		return $sWrk;
	}

	//-------------------------------------------------------------------------------
	// Function GetSort
	// - Return Sort parameters based on Sort Links clicked
	// - Variables setup: Session[EWR_TABLE_SESSION_ORDER_BY], Session["sort_Table_Field"]
	function GetSort($options = array()) {
		if ($this->DrillDown)
			return "";
		$bResetSort = @$options["resetsort"] == "1" || @$_GET["cmd"] == "resetsort";
		$orderBy = (@$options["order"] <> "") ? @$options["order"] : ewr_StripSlashes(@$_GET["order"]);
		$orderType = (@$options["ordertype"] <> "") ? @$options["ordertype"] : ewr_StripSlashes(@$_GET["ordertype"]);

		// Check for a resetsort command
		if ($bResetSort) {
			$this->setOrderBy("");
			$this->setStartGroup(1);
			$this->MACHUYENBAY->setSort("");
			$this->MAMAYBAY->setSort("");
			$this->SANBAYDI->setSort("");
			$this->SANBAYDEN->setSort("");
			$this->GIAVEGOC->setSort("");
			$this->THOIGIANBAY->setSort("");
			$this->THOIGIANDEN->setSort("");
			$this->TRANSITION->setSort("");

		// Check for an Order parameter
		} elseif ($orderBy <> "") {
			$this->CurrentOrder = $orderBy;
			$this->CurrentOrderType = $orderType;
			$sSortSql = $this->SortSql();
			$this->setOrderBy($sSortSql);
			$this->setStartGroup(1);
		}
		return $this->getOrderBy();
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ewr_Header(FALSE) ?>
<?php

// Create page object
if (!isset($chuyenbay_rpt)) $chuyenbay_rpt = new crchuyenbay_rpt();
if (isset($Page)) $OldPage = $Page;
$Page = &$chuyenbay_rpt;

// Page init
$Page->Page_Init();

// Page main
$Page->Page_Main();

// Global Page Rendering event (in ewrusrfn*.php)
Page_Rendering();

// Page Rendering event
$Page->Page_Render();
?>
<?php include_once "phprptinc/header.php" ?>
<script type="text/javascript">

// Create page object
var chuyenbay_rpt = new ewr_Page("chuyenbay_rpt");

// Page properties
chuyenbay_rpt.PageID = "rpt"; // Page ID
var EWR_PAGE_ID = chuyenbay_rpt.PageID;

// Extend page with Chart_Rendering function
chuyenbay_rpt.Chart_Rendering = 
 function(chart, chartid) { // DO NOT CHANGE THIS LINE!

 	//alert(chartid);
 }

// Extend page with Chart_Rendered function
chuyenbay_rpt.Chart_Rendered = 
 function(chart, chartid) { // DO NOT CHANGE THIS LINE!

 	//alert(chartid);
 }
</script>
<?php if (!$Page->DrillDown) { ?>
<?php } ?>
<?php if (!$Page->DrillDown) { ?>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<!-- container (begin) -->
<div id="ewContainer" class="ewContainer">
<!-- top container (begin) -->
<div id="ewTop" class="ewTop">
<a id="top"></a>
<?php if (@$Page->GenOptions["showfilter"] == "1") { ?>
<?php $Page->ShowFilterList(TRUE) ?>
<?php } ?>
<!-- top slot -->
<div class="ewToolbar">
<?php if (!$Page->DrillDown || !$Page->DrillDownInPanel) { ?>
<?php if ($ReportBreadcrumb) $ReportBreadcrumb->Render(); ?>
<?php } ?>
<?php
if (!$Page->DrillDownInPanel) {
	$Page->ExportOptions->Render("body");
	$Page->SearchOptions->Render("body");
	$Page->FilterOptions->Render("body");
	$Page->GenerateOptions->Render("body");
}
?>
<?php if (!$Page->DrillDown) { ?>
<?php echo $ReportLanguage->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php $Page->ShowPageHeader(); ?>
<?php $Page->ShowMessage(); ?>
</div>
<!-- top container (end) -->
	<!-- left container (begin) -->
	<div id="ewLeft" class="ewLeft">
	<!-- Left slot -->
	</div>
	<!-- left container (end) -->
	<!-- center container - report (begin) -->
	<div id="ewCenter" class="ewCenter">
	<!-- center slot -->
<!-- summary report starts -->
<div id="report_summary">
<?php

// Set the last group to display if not export all
if ($Page->ExportAll && $Page->Export <> "") {
	$Page->StopGrp = $Page->TotalGrps;
} else {
	$Page->StopGrp = $Page->StartGrp + $Page->DisplayGrps - 1;
}

// Stop group <= total number of groups
if (intval($Page->StopGrp) > intval($Page->TotalGrps))
	$Page->StopGrp = $Page->TotalGrps;
$Page->RecCount = 0;
$Page->RecIndex = 0;

// Get first row
if ($Page->TotalGrps > 0) {
	$Page->GetRow(1);
	$Page->GrpCount = 1;
}
$Page->GrpIdx = ewr_InitArray(2, -1);
$Page->GrpIdx[0] = -1;
$Page->GrpIdx[1] = $Page->StopGrp - $Page->StartGrp + 1;
while ($rs && !$rs->EOF && $Page->GrpCount <= $Page->DisplayGrps || $Page->ShowHeader) {

	// Show dummy header for custom template
	// Show header

	if ($Page->ShowHeader) {
?>
<?php if ($Page->Export == "word" || $Page->Export == "excel") { ?>
<div class="ewGrid"<?php echo $Page->ReportTableStyle ?>>
<?php } else { ?>
<div class="panel panel-default ewGrid"<?php echo $Page->ReportTableStyle ?>>
<?php } ?>
<!-- Report grid (begin) -->
<div class="<?php if (ewr_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="<?php echo $Page->ReportTableClass ?>">
<thead>
	<!-- Table header -->
	<tr class="ewTableHeader">
<?php if ($Page->MACHUYENBAY->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="MACHUYENBAY"><div class="chuyenbay_MACHUYENBAY"><span class="ewTableHeaderCaption"><?php echo $Page->MACHUYENBAY->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="MACHUYENBAY">
<?php if ($Page->SortUrl($Page->MACHUYENBAY) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_MACHUYENBAY">
			<span class="ewTableHeaderCaption"><?php echo $Page->MACHUYENBAY->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_MACHUYENBAY" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->MACHUYENBAY) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->MACHUYENBAY->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->MACHUYENBAY->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->MACHUYENBAY->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->MAMAYBAY->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="MAMAYBAY"><div class="chuyenbay_MAMAYBAY"><span class="ewTableHeaderCaption"><?php echo $Page->MAMAYBAY->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="MAMAYBAY">
<?php if ($Page->SortUrl($Page->MAMAYBAY) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_MAMAYBAY">
			<span class="ewTableHeaderCaption"><?php echo $Page->MAMAYBAY->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_MAMAYBAY" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->MAMAYBAY) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->MAMAYBAY->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->MAMAYBAY->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->MAMAYBAY->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->SANBAYDI->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="SANBAYDI"><div class="chuyenbay_SANBAYDI"><span class="ewTableHeaderCaption"><?php echo $Page->SANBAYDI->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="SANBAYDI">
<?php if ($Page->SortUrl($Page->SANBAYDI) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_SANBAYDI">
			<span class="ewTableHeaderCaption"><?php echo $Page->SANBAYDI->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_SANBAYDI" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->SANBAYDI) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->SANBAYDI->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->SANBAYDI->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->SANBAYDI->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->SANBAYDEN->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="SANBAYDEN"><div class="chuyenbay_SANBAYDEN"><span class="ewTableHeaderCaption"><?php echo $Page->SANBAYDEN->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="SANBAYDEN">
<?php if ($Page->SortUrl($Page->SANBAYDEN) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_SANBAYDEN">
			<span class="ewTableHeaderCaption"><?php echo $Page->SANBAYDEN->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_SANBAYDEN" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->SANBAYDEN) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->SANBAYDEN->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->SANBAYDEN->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->SANBAYDEN->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->GIAVEGOC->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="GIAVEGOC"><div class="chuyenbay_GIAVEGOC"><span class="ewTableHeaderCaption"><?php echo $Page->GIAVEGOC->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="GIAVEGOC">
<?php if ($Page->SortUrl($Page->GIAVEGOC) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_GIAVEGOC">
			<span class="ewTableHeaderCaption"><?php echo $Page->GIAVEGOC->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_GIAVEGOC" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->GIAVEGOC) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->GIAVEGOC->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->GIAVEGOC->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->GIAVEGOC->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->THOIGIANBAY->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="THOIGIANBAY"><div class="chuyenbay_THOIGIANBAY"><span class="ewTableHeaderCaption"><?php echo $Page->THOIGIANBAY->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="THOIGIANBAY">
<?php if ($Page->SortUrl($Page->THOIGIANBAY) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_THOIGIANBAY">
			<span class="ewTableHeaderCaption"><?php echo $Page->THOIGIANBAY->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_THOIGIANBAY" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->THOIGIANBAY) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->THOIGIANBAY->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->THOIGIANBAY->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->THOIGIANBAY->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->THOIGIANDEN->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="THOIGIANDEN"><div class="chuyenbay_THOIGIANDEN"><span class="ewTableHeaderCaption"><?php echo $Page->THOIGIANDEN->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="THOIGIANDEN">
<?php if ($Page->SortUrl($Page->THOIGIANDEN) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_THOIGIANDEN">
			<span class="ewTableHeaderCaption"><?php echo $Page->THOIGIANDEN->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_THOIGIANDEN" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->THOIGIANDEN) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->THOIGIANDEN->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->THOIGIANDEN->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->THOIGIANDEN->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
<?php if ($Page->TRANSITION->Visible) { ?>
<?php if ($Page->Export <> "" || $Page->DrillDown) { ?>
	<td data-field="TRANSITION"><div class="chuyenbay_TRANSITION"><span class="ewTableHeaderCaption"><?php echo $Page->TRANSITION->FldCaption() ?></span></div></td>
<?php } else { ?>
	<td data-field="TRANSITION">
<?php if ($Page->SortUrl($Page->TRANSITION) == "") { ?>
		<div class="ewTableHeaderBtn chuyenbay_TRANSITION">
			<span class="ewTableHeaderCaption"><?php echo $Page->TRANSITION->FldCaption() ?></span>
		</div>
<?php } else { ?>
		<div class="ewTableHeaderBtn ewPointer chuyenbay_TRANSITION" onclick="ewr_Sort(event,'<?php echo $Page->SortUrl($Page->TRANSITION) ?>',0);">
			<span class="ewTableHeaderCaption"><?php echo $Page->TRANSITION->FldCaption() ?></span>
			<span class="ewTableHeaderSort"><?php if ($Page->TRANSITION->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($Page->TRANSITION->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span>
		</div>
<?php } ?>
	</td>
<?php } ?>
<?php } ?>
	</tr>
</thead>
<tbody>
<?php
		if ($Page->TotalGrps == 0) break; // Show header only
		$Page->ShowHeader = FALSE;
	}
	$Page->RecCount++;
	$Page->RecIndex++;
?>
<?php

		// Render detail row
		$Page->ResetAttrs();
		$Page->RowType = EWR_ROWTYPE_DETAIL;
		$Page->RenderRow();
?>
	<tr<?php echo $Page->RowAttributes(); ?>>
<?php if ($Page->MACHUYENBAY->Visible) { ?>
		<td data-field="MACHUYENBAY"<?php echo $Page->MACHUYENBAY->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_MACHUYENBAY"<?php echo $Page->MACHUYENBAY->ViewAttributes() ?>><?php echo $Page->MACHUYENBAY->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->MAMAYBAY->Visible) { ?>
		<td data-field="MAMAYBAY"<?php echo $Page->MAMAYBAY->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_MAMAYBAY"<?php echo $Page->MAMAYBAY->ViewAttributes() ?>><?php echo $Page->MAMAYBAY->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->SANBAYDI->Visible) { ?>
		<td data-field="SANBAYDI"<?php echo $Page->SANBAYDI->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_SANBAYDI"<?php echo $Page->SANBAYDI->ViewAttributes() ?>><?php echo $Page->SANBAYDI->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->SANBAYDEN->Visible) { ?>
		<td data-field="SANBAYDEN"<?php echo $Page->SANBAYDEN->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_SANBAYDEN"<?php echo $Page->SANBAYDEN->ViewAttributes() ?>><?php echo $Page->SANBAYDEN->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->GIAVEGOC->Visible) { ?>
		<td data-field="GIAVEGOC"<?php echo $Page->GIAVEGOC->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_GIAVEGOC"<?php echo $Page->GIAVEGOC->ViewAttributes() ?>><?php echo $Page->GIAVEGOC->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->THOIGIANBAY->Visible) { ?>
		<td data-field="THOIGIANBAY"<?php echo $Page->THOIGIANBAY->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_THOIGIANBAY"<?php echo $Page->THOIGIANBAY->ViewAttributes() ?>><?php echo $Page->THOIGIANBAY->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->THOIGIANDEN->Visible) { ?>
		<td data-field="THOIGIANDEN"<?php echo $Page->THOIGIANDEN->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_THOIGIANDEN"<?php echo $Page->THOIGIANDEN->ViewAttributes() ?>><?php echo $Page->THOIGIANDEN->ListViewValue() ?></span></td>
<?php } ?>
<?php if ($Page->TRANSITION->Visible) { ?>
		<td data-field="TRANSITION"<?php echo $Page->TRANSITION->CellAttributes() ?>>
<span data-class="tpx<?php echo $Page->RecCount ?>_<?php echo $Page->RecCount ?>_chuyenbay_TRANSITION"<?php echo $Page->TRANSITION->ViewAttributes() ?>><?php echo $Page->TRANSITION->ListViewValue() ?></span></td>
<?php } ?>
	</tr>
<?php

		// Accumulate page summary
		$Page->AccumulateSummary();

		// Get next record
		$Page->GetRow(2);
	$Page->GrpCount++;
} // End while
?>
<?php if ($Page->TotalGrps > 0) { ?>
</tbody>
<tfoot>
	</tfoot>
<?php } elseif (!$Page->ShowHeader && FALSE) { // No header displayed ?>
<?php if ($Page->Export == "word" || $Page->Export == "excel") { ?>
<div class="ewGrid"<?php echo $Page->ReportTableStyle ?>>
<?php } else { ?>
<div class="panel panel-default ewGrid"<?php echo $Page->ReportTableStyle ?>>
<?php } ?>
<!-- Report grid (begin) -->
<div class="<?php if (ewr_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="<?php echo $Page->ReportTableClass ?>">
<?php } ?>
<?php if ($Page->TotalGrps > 0 || FALSE) { // Show footer ?>
</table>
</div>
<?php if (!($Page->DrillDown && $Page->TotalGrps > 0)) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php include "chuyenbayrptpager.php" ?>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
</div>
<!-- Summary Report Ends -->
	</div>
	<!-- center container - report (end) -->
	<!-- right container (begin) -->
	<div id="ewRight" class="ewRight">
	<!-- Right slot -->
	</div>
	<!-- right container (end) -->
<div class="clearfix"></div>
<!-- bottom container (begin) -->
<div id="ewBottom" class="ewBottom">
	<!-- Bottom slot -->
	</div>
<!-- Bottom Container (End) -->
</div>
<!-- Table Container (End) -->
<?php $Page->ShowPageFooter(); ?>
<?php if (EWR_DEBUG_ENABLED) echo ewr_DebugMsg(); ?>
<?php

// Close recordsets
if ($rsgrp) $rsgrp->Close();
if ($rs) $rs->Close();
?>
<?php if (!$Page->DrillDown) { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "phprptinc/footer.php" ?>
<?php
$Page->Page_Terminate();
if (isset($OldPage)) $Page = $OldPage;
?>
