<!-- Begin Main Menu -->
<?php

// Generate all menu items
$RootMenu->IsRoot = TRUE;
$RootMenu->AddMenuItem(1, "mmi_chitietdatve", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("1", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "chitietdatverpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(2, "mmi_chuyenbay", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("2", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "chuyenbayrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(3, "mmi_diadiem", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("3", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "diadiemrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(4, "mmi_dondatve", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("4", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "dondatverpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(5, "mmi_historychitietdatve", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("5", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "historychitietdatverpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(6, "mmi_historydondatve", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("6", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "historydondatverpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(7, "mmi_historykhachhang", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("7", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "historykhachhangrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(8, "mmi_historyve", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("8", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "historyverpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(9, "mmi_khachhang", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("9", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "khachhangrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(10, "mmi_khuyenmai", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("10", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "khuyenmairpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(11, "mmi_loaive", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("11", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "loaiverpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(12, "mmi_maybay", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("12", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "maybayrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(13, "mmi_nhanvien", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("13", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "nhanvienrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(14, "mmi_quyen", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("14", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "quyenrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(15, "mmi_sanbay", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("15", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "sanbayrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(16, "mmi_taikhoan", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("16", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "taikhoanrpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(17, "mmi_thamso", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("17", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "thamsorpt.php", -1, "", TRUE, FALSE);
$RootMenu->AddMenuItem(18, "mmi_ve", $ReportLanguage->Phrase("SimpleReportMenuItemPrefix") . $ReportLanguage->MenuPhrase("18", "MenuText") . $ReportLanguage->Phrase("SimpleReportMenuItemSuffix"), "verpt.php", -1, "", TRUE, FALSE);
$RootMenu->Render();
?>
<!-- End Main Menu -->
