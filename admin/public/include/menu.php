<?php 
$urlArr = explode('/', $_SERVER['REQUEST_URI']);
$thisUrl = $urlArr[2];
//var_dump($urlArr[3]);
/*if($thisUrl == "them-chuyenbay.php"){
  echo "1";
}*/

function strposa($haystack, $needles=array(), $offset=0) {
  $chr = array();

  foreach($needles as $needle) {
    $res = strpos($haystack, $needle, $offset);

    if ($res !== false)
      $chr[$needle] = $res;
  }

  if (empty($chr))
    return false;

  return min($chr);
}

?>

<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="../images/contact/man.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $_SESSION["tennhanvien"] ?></p>
        <a href="#"><i class="glyphicon glyphicon-heart text-danger"></i> <?php echo $_SESSION["quyen"] ?></a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="glyphicon glyphicon-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">DANH MỤC</li>

      <li class="treeview <?php echo ($thisUrl=="")?"active":"" ?>">
        <a href=".">
          <i class="glyphicon glyphicon-list-alt"></i> <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="glyphicon glyphicon-chevron-right pull-right"></i>
          </span>
        </a>
      </li>

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "QL")
      { ?>
      <li class="treeview <?php echo (strpos($thisUrl, "chuyenbay") > -1)?"active":"" ?>">
        <a href="#">
          <i class="glyphicon glyphicon-plane"></i>
          <span>Quản lý chuyến bay</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php echo ($thisUrl=="chuyenbay.php")?"active":"" ?>"><a href="chuyenbay.php"><i class= "glyphicon glyphicon-plus text-info"></i> Danh sách chuyến bay</a></li>
          <li class="<?php echo ($thisUrl=="them-chuyenbay.php")?"active":"" ?>"><a href="them-chuyenbay.php"><i class= "glyphicon glyphicon-plus text-info""></i> Thêm chuyến bay</a></li>
        </ul>
      </li>
      <?php } ?>

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "QL")
      { ?>
      <li class="treeview <?php echo (strposa($thisUrl, array("sanbay","diadiem","maybay")) > -1)?"active":"" ?>">
        <a href="#">
          <i class="glyphicon glyphicon-map-marker"></i>
          <span>Quản lý thông tin bay</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php echo (strpos($thisUrl, "sanbay") > -1)?"active":"" ?>"><a href="sanbay.php"><i class= "glyphicon glyphicon-plus text-info"></i> Sân bay</a></li>
          <li class="<?php echo (strpos($thisUrl, "diadiem") > -1)?"active":"" ?>"><a href="diadiem.php"><i class= "glyphicon glyphicon-plus text-info""></i> Địa điểm</a></li>
          <li class="<?php echo (strpos($thisUrl, "maybay") > -1)?"active":"" ?>"><a href="maybay.php"><i class= "glyphicon glyphicon-plus text-info""></i> Máy bay</a></li>
        </ul>
      </li>
      <?php } ?>

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "BH")
      { ?>
      <li class="treeview <?php echo (strpos($thisUrl, "khachhang") > -1)?"active":"" ?>">
        <a href="khachhang.php">
          <i class="glyphicon glyphicon-user"></i> <span>Quản lý khách hàng</span>
        </a>
      </li>
      <?php } ?>

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "KT" || $_SESSION["quyen"] == "BH")
      { ?>
      <li class="treeview <?php echo (strpos($thisUrl, "donhang") > -1)?"active":"" ?>">
        <a href="donhang.php">
          <i class="glyphicon glyphicon-lock"></i> <span>Quản lý đơn hàng</span>
        </a>
      </li>
      <?php } ?>

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "KT" || $_SESSION["quyen"] == "BH")
      { ?>
      <li class="treeview <?php echo (strpos($thisUrl, "khuyenmai") > -1)?"active":"" ?>">
        <a href="khuyenmai.php">
          <i class="glyphicon glyphicon-qrcode"></i> <span>Cập nhật khuyến mãi</span>
        </a>
      </li>  
      <?php } ?>

      <!-- <li class="treeview <?php echo (strpos($thisUrl, "quydinh") > -1)?"active":"" ?>">
        <a href="quydinh.php">
          <i class="glyphicon glyphicon-bell"></i>
          <span>Quản lý quy định</span>
        </a>
      </li> -->

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "NS")
      { ?>
      <li class="treeview <?php echo (strpos($thisUrl, "nhanvien") > -1)?"active":"" ?>">
        <a href="#">
          <i class="glyphicon glyphicon-tint"></i>
          <span>Quản lý nhân viên</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php echo ($thisUrl=="nhanvien.php")?"active":"" ?>"><a href="nhanvien.php"><i class= "glyphicon glyphicon-plus text-info"></i> Danh sách nhân viên</a></li>
          <li class="<?php echo ($thisUrl=="them-nhanvien.php")?"active":"" ?>"><a href="them-nhanvien.php"><i class= "glyphicon glyphicon-plus text-info""></i> Thêm nhân viên</a></li>
        </ul>
      </li>
      <?php } ?>

      <li class="treeview <?php echo (strpos($thisUrl, "backup") > -1)?"active":"" ?>">
        <a href="#">
          <i class="glyphicon glyphicon-print"></i>
          <span>History</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php echo ($thisUrl=="backup-kh.php")?"active":"" ?>"><a href="backup-kh.php"><i class= "glyphicon glyphicon-plus text-info"></i> History khách hàng</a></li>
          <!-- <li class="<?php echo ($thisUrl=="backup-ddv.php")?"active":"" ?>"><a href="backup-ddv.php"><i class= "glyphicon glyphicon-plus text-info""></i> History đơn đặt vé</a></li> -->
        </ul>
      </li>

      <?php if ($_SESSION["quyen"] == "AD" || $_SESSION["quyen"] == "KT")
      { ?>
      <li class="treeview <?php echo (strpos($thisUrl, "bao-cao") > -1)?"active":"" ?>">
        <a href="#">
          <i class="glyphicon glyphicon-signal"></i>
          <span>Báo cáo doanh thu</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php echo ($thisUrl=="bao-cao-doanh-thu-nam.php")?"active":"" ?>"><a href="bao-cao-doanh-thu-nam.php"><i class= "glyphicon glyphicon-plus text-info"></i> Theo năm</a></li>
          <li class="<?php echo ($thisUrl=="bao-cao-doanh-thu-thang-nam.php")?"active":"" ?>"><a href="bao-cao-doanh-thu-thang-nam.php"><i class= "glyphicon glyphicon-plus text-info""></i> Theo tháng năm</a></li>
        </ul>
      </li>
      <?php } ?>

      <!-- <li class="treeview">
        <a href="Report">
          <i class="glyphicon glyphicon-bell"></i>
          <span>Report</span>
        </a>
      </li> -->

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>