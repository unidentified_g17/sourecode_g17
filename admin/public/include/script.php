<!-- jQuery 2.2.3 -->
<script src="public/layout/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="public/layout/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="public/layout/plugins/select2/select2.full.min.js"></script>
<!-- InputMask
<script src="public/layout/plugins/input-mask/jquery.inputmask.js"></script>
<script src="public/layout/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="public/layout/plugins/input-mask/jquery.inputmask.extensions.js"></script> -->
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="public/layout/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="public/layout/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="public/layout/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="public/layout/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="public/layout/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="public/layout/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="public/layout/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="public/layout/dist/js/demo.js"></script>

<!--datatable-->
<!-- <script src="public/layout/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="public/layout/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="public/layout/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="public/assets/pdfmake.min.js"></script>
<script src="public/layout/plugins/datatables/buttons.html5.min.js"></script> -->

<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="public/layout/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>

<script src="public/assets/datatable.js"></script>

<!--JS-->
<script src="public/assets/common.js"></script>
<script src="public/assets/kiemtra.js"></script>
<script src="public/assets/thu_vien_ajax.js"></script>