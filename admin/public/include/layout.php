<?php 
@session_start() ?>
<!DOCTYPE html>
<html>
<?php include("head.php") ?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
   <?php include("header.php") ?>

   <!-- Left side column. contains the logo and sidebar -->
   <?php include("menu.php") ?>

   <!-- Content Wrapper. Contains page content -->
   <?php include("content.php") ?>

   <!-- Footer -->
   <?php include("footer.php") ?>

   <!-- Control Sidebar -->
   <?php include("control_sidebar.php") ?>

 </div>
 <!-- ./wrapper -->
 
 <?php include("script.php") ?>

</body>

</html>