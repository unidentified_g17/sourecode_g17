<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <?php 
    if(isset($view))
    {
      include($view);
    }
    else
    {
      include("app/controllers/c_dashboard.php");
      $c_dashboard = new C_dashboard();
      $c_dashboard->Hien_thi_dashboard();
    }
    ?>
  </section>
  <!-- /.content -->
</div>