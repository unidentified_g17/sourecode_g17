<?php 
@session_start() ?>
<!DOCTYPE html>
<html>
<?php include("head.php") ?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
   <?php include("header.php") ?>

   <!-- Left side column. contains the logo and sidebar -->
   <?php include("menu.php") ?>

   <!-- View for no-permission -->
   <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
     <h2 class="text-danger text-center">TÀI KHOẢN NÀY KHÔNG CÓ QUYỀN TRUY CẬP VÀO TRANG NÀY!</h2>
     <img class="center-block" src="../images/noo.png"/>
     <br/>
     <a href="index.php" class="btn btn-warning center-block">Quay về trang chủ</a>
   </section>
   <!-- /.content -->
 </div>

 <!-- Footer -->
 <?php include("footer.php") ?>

 <!-- Control Sidebar -->
 <?php include("control_sidebar.php") ?>

</div>
<!-- ./wrapper -->

<?php include("script.php") ?>

</body>

</html>