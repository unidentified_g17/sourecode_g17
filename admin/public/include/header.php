<header class="main-header">

    <!-- Logo -->
    <a href="." class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SKY</b></span>
      <!-- logo for regular state and mobile devices -->
      <!--<span class="logo-lg"><b>Admin</b>LTE</span>-->
      <span class="logo-lg"><img src="../images/logo/skyticket.png" width="160px"/></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Đổi mật khẩu / Đăng xuất / -->
          <li>
          <a href="#" class="btn bg-navy btn-flat margin" style="padding: 5px">Đổi mật khẩu</a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
          <a href="index.php?func=exit" class="btn bg-orange btn-flat margin" style="padding: 5px">Đăng xuất <span class="glyphicon glyphicon-off"></span></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>