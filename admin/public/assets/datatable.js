$(function () {
  $('#table-maybay, #table-sanbay, #table-diadiem, #table-khachhang, #table-nhanvien, #table-chuyenbay, #table-donhang').DataTable({
    "autoFill": true,
    "stateSave": true,
    "paging": true,
    "language": {
      "lengthMenu": "Hiển thị _MENU_ kết quả mỗi trang",
      "zeroRecords": "Không tìm thấy",
      "info": "Trang _PAGE_ / _PAGES_",
      "infoEmpty": "Không có kết quả",
      "infoFiltered": "(tìm kiếm trên _MAX_ kết quả)",
      "search": "Tìm kiếm"
    }, //language

    //html5 button
    "dom": 'Bfrtip',
    "buttons": [
    'copy', 'csv', 'excel', 'pdf', 'print'
 /*{
     "extend": 'pdfHtml5',
     "exportOptions": {
      "columns": [ 0, 1, 2, 5 ]
    }
  }*/
  ]
  }); //datatable
});