// JavaScript Document
// Các hàm kiểm tra
$(function(){
	$('.sua-km').click(function()
	{
		$('#them_ma_khuyen_mai').removeClass('kiemtra');
		$('#them_tien_khuyen_mai').removeClass('kiemtra');
	});
});

function Kiemtradulieu_cbay()
{
	var kt=document.getElementsByClassName("kiemtra");
	for(i=0;i<kt.length;i++)
	{
		if(kt.item(i).value=="")
		{
			swal("Error", kt.item(i).getAttribute("data_error"), "error");
			//window.setTimeout(function (){kt.item(i).focus();}, 0);
			setTimeout(function() { kt.item(i).focus() }, 2000);
			return false;
		}
	}

	var sbdi = document.getElementById("sbdi");
	var sbden = document.getElementById("sbden");
	if(sbdi.value == sbden.value)
	{
		swal("Error", "2 địa điểm không được trùng nhau!", "error");
		return false;
	}

	var c_bay = document.getElementById("ma_chuyen_bay");
	if(c_bay.value.length != 5)
	{
		swal("Error", "Mã chuyến bay phải có 5 ký tự", "error");
		setTimeout(function() { c_bay.focus() }, 1000);
		return false;
	}

	var tg = document.getElementById("reservationtime").value;
	var tgian = tg.split('-');
	var tgdi = tgian[0].trim();
	var tgden = tgian[1].trim();

	if(tgdi === tgden)
	{
		swal("Error", "Xem lại thời gian bay", "error");
		return false;
	}

	return true;
}


function Kiemtradulieu_maybay(){
	//Kiểm tra máy bay
	var kt=document.getElementsByClassName("kiemtra");
	for(i=0;i<kt.length;i++)
	{
		if(kt.item(i).value=="")
		{
			swal("Error", kt.item(i).getAttribute("data_error"), "error");
			//window.setTimeout(function (){kt.item(i).focus();}, 0);
			setTimeout(function() { kt.item(i).focus() }, 2000);
			return false;
		}
	}

	var may_bay = document.getElementById("them_ma_may_bay");
	if(may_bay.value.length != 5)
	{
		swal("Error", "Mã máy bay phải có 5 ký tự", "error");
		setTimeout(function() { may_bay.focus() }, 1000);
		return false;
	}
	return true;
}

function Kiemtradulieu_sanbay(){
	//Kiểm tra máy bay
	var kt=document.getElementsByClassName("kiemtra");
	for(i=0;i<kt.length;i++)
	{
		if(kt.item(i).value=="")
		{
			swal("Error", kt.item(i).getAttribute("data_error"), "error");
			//window.setTimeout(function (){kt.item(i).focus();}, 0);
			setTimeout(function() { kt.item(i).focus() }, 2000);
			return false;
		}
	}

	//Kiểm tra sân bay
	var san_bay = document.getElementById("them_ma_san_bay");
	if(san_bay.value.length != 3)
	{
		swal("Error", "Mã sân bay phải có 3 ký tự", "error");
		setTimeout(function() { san_bay.focus() }, 1000);
		return false;
	}
	return true;
}

function Kiemtradulieu_diadiem(){
	//Kiểm tra máy bay
	var kt=document.getElementsByClassName("kiemtra");
	for(i=0;i<kt.length;i++)
	{
		if(kt.item(i).value=="")
		{
			swal("Error", kt.item(i).getAttribute("data_error"), "error");
			//window.setTimeout(function (){kt.item(i).focus();}, 0);
			setTimeout(function() { kt.item(i).focus() }, 2000);
			return false;
		}
	}

	//Kiểm tra địa điểm
	var dia_diem = document.getElementById("them_ma_dia_diem");
	if(dia_diem.value.length != 3)
	{
		swal("Error", "Mã địa điểm phải có 3 ký tự", "error");
		setTimeout(function() { dia_diem.focus() }, 1000);
		return false;
	}
	return true;
}