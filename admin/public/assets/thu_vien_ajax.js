/*Ajax in here*/
//Cách viết khác: $(function() {} )
/*===============BỎ XÓA========================
$(document).ready(function()
{
	//========XÓA ĐƠN HÀNG
	//dùng attr class
	$('#table-donhang').on('click', '.xoa-don-hang', function(e)
	{
		var id= $(this).attr('ma_don_hang'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-donhang.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});


	//========XÓA ĐỊA ĐIỂM
	//dùng attr class
	$('#table-diadiem').on('click', '.xoa-dia-diem', function(e)
	{
		var id= $(this).attr('ma_dia_diem'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-diadiem.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});

	//========XÓA máy bay
	//dùng attr class
	$('#table-maybay').on('click', '.xoa-may-bay', function(e)
	{
		var id= $(this).attr('ma_may_bay'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-maybay.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});

	//========XÓA sân bay
	//dùng attr class
	$('#table-sanbay').on('click', '.xoa-san-bay', function(e)
	{
		var id= $(this).attr('ma_san_bay'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-sanbay.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});

	//========XÓA CHUYẾN BAY
	//dùng attr class
	$('#table-chuyenbay').on('click', '.xoa-chuyen-bay', function(e)
	{
		var id= $(this).attr('ma_chuyen_bay'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-chuyenbay.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});
}) //end document
============================================*/
$(function(){
		//========XÓA khách hàng
	//dùng attr class
	$('#table-khachhang').on('click', '.xoa-khach-hang', function(e)
	{
		var id= $(this).attr('ma_khach_hang'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-khachhang.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});

	//========XÓA NHÂN VIÊN
	//dùng attr class
	$('#table-nhanvien').on('click', '.xoa-nhan-vien', function(e)
	{
		var id= $(this).attr('ma_nhan_vien'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-nhanvien.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});
});


//Trang cập nhật khuyến mãi
$(function()
{
	$('.sua-km').click(function()
	{
		var id= $(this).attr('ma_khuyen_mai'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		$.ajax(
		{
			type: "POST", //chỗ này dấu PHẨY (,)
			url: "xl_cap_nhat_khuyenmai.php",
			data: data,
			cache: false,
			success: function(dataBack){
				$('#div_cap_nhat_khuyen_mai').show();
				$('#div_cap_nhat_khuyen_mai').html(dataBack);
				$('#them_khuyen_mai').hide();
			},
			error: function(){
				swal2("Có lỗi!");
			}
			});//end ajax
	});

	$('.xoa-km').click(function()
	{
		var id= $(this).attr('ma_khuyen_mai'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-khuyenmai.php",
				data: data,
				cache: false,
				success: function(){
					parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){
					swal2("Xóa thất bại!");
				}
			});//end ajax
		}//end ifif
	});
});

//Đơn hàng
$(function(){
	//Xem chi tiết
	$('.xem-cthd').on('click', function(){
		var id = $(this).attr('ma_donhang');
		var data = { id: id};
		$.ajax({
			url: 'xl_chi_tiet_donhang.php',
			type: 'POST',
			data: data,
			success: function(dataBack){
				$('.modal-dialog').html(dataBack);
			}
		});
	});

	//Cập nhật tình trạng
	$('.btn-capnhatthanhtoan').on('click', function(){
		var id = $(this).attr('ma_donhang');
		var data = { id: id};
		$.ajax({
			url: 'xl_capnhatthanhtoan.php',
			type: 'POST',
			data: data,
			success: function(dataBack){
				alert(dataBack);
				window.location.reload(true);
			}
		});
	});

	//Xem chi tiết backup
	$('.xem-cthd-backup').on('click', function(){
		var id = $(this).attr('ma_donhang');
		var data = { id: id};
		$.ajax({
			url: 'xl_chi_tiet_donhang-backup.php',
			type: 'POST',
			data: data,
			success: function(dataBack){
				$('.modal-dialog').html(dataBack);
			}
		});
	});
})

//Báo cáo
$(function(){
	//Tháng-năm
	$("#xem-bc-thang-nam").on('click',function(){
		var thang = $("#thang").val();
		var nam = $("#nam").val();
		$.ajax({
			type: 'POST',
			url: 'xl_bao_cao_thang_nam.php',
			data: { thang: thang, nam: nam},
			success: function(dataBack){
				$(".xuat-bc-thang-nam").html(dataBack);
			}
		});
	});

	//năm
	$("#xem-bc-nam").on('click',function(){
		var nam = $("#nam").val();
		$.ajax({
			type: 'POST',
			url: 'xl_bao_cao_nam.php',
			data: { nam: nam},
			success: function(dataBack){
				$(".xuat-bc-nam").html(dataBack);
			}
		});
	});
})